#ifndef COLL_H
#define COLL_H

#include <assert.h>

/*
 * A class which represents an object for which the collision is to be detected.
 * It holds the center position, the current velocity, and the 3-dimensional
 * radius of the surrounding ellipsoid, all of which can be changed at runtime.
 */
class CollisionObject
{
protected:
    float _position[3]; // R^3
    float _velocity[3]; // R^3
    float _translation[3]; // R^3
    float _radius[3]; // for ellipsoid space

public:
    friend class CollisionDetect;
    CollisionObject(void)
    {
        reset();
    }
    void reset(void)
    {
        _position[0] = 0.f;
        _position[1] = 0.f;
        _position[2] = 0.f;
        _velocity[0] = 0.f;
        _velocity[1] = 0.f;
        _velocity[2] = 0.f;
        _translation[0] = 0.f;
        _translation[1] = 0.f;
        _translation[2] = 0.f;
        _radius[0] = 1.f;
        _radius[1] = 1.f;
        _radius[2] = 1.f;
    }
    void set_position(float posx, float posy, float posz)
    {
        _position[0] = posx;
        _position[1] = posy;
        _position[2] = posz;
    }
    void set_velocity(float velx, float vely, float velz)
    {
        _velocity[0] = velx;
        _velocity[1] = vely;
        _velocity[2] = velz;
    }
    void set_translation(float dx, float dy, float dz)
    {
        _translation[0] = dx;
        _translation[1] = dy;
        _translation[2] = dz;
    }
    void set_radius(float radx, float rady, float radz)
    {
        assert(radx > 0.f && rady > 0.f && radz > 0.f);
        _radius[0] = radx;
        _radius[1] = rady;
        _radius[2] = radz;
    }
};

class CollisionDetect
{
public:
    /*
     * Reset 'collision' member to false before calling CollisionDetect::detect
     * for a triangle mesh. We won't do this to support successive calls in
     * order to check for the nearest colliding triangle.
     * (You can also check the function's return value, but this won't keep
     * previous hits in mind.)
     */
    struct CollisionInfo
    {
        friend class CollisionDetect;
        bool collision;
        float distance;
        float intersection[3];
        float time;
    private:
        float _intersection_espace[3];
    };
    struct SweepInfo
    {
        friend class CollisionDetect;
        float pos[3];
        float vel[3];
    private:
        float _dest[3];
        float _first_plane[4];
    };
private:
    /*
     * All coordinates in ellipsoid space, in clockwise order.
     * Results are in ellipsoid space as well.
     */
    static bool _detect(CollisionObject &object,
        CollisionInfo &results,
        float trix1, float triy1, float triz1,
        float trix2, float triy2, float triz2,
        float trix3, float triy3, float triz3,
        bool &updated);
public:
    /*
     * Triangle is defined by its 3 points. Triangle points are in
     * anti-clockwise order required by the CG software renderer.
     * Parameters are converted to ellipsoid space internally.
     * Intersection is converted from ellipsoid space.
     */
    static bool detect(CollisionObject &object,
        CollisionInfo &results,
        float trix1, float triy1, float triz1,
        float trix2, float triy2, float triz2,
        float trix3, float triy3, float triz3)
    {
        /* anti-clockwise triangle: 1,2,3 -> clockwise triangle: 1,3,2 */
        trix1 /= object._radius[0];
        trix2 /= object._radius[0];
        trix3 /= object._radius[0];
        triy1 /= object._radius[1];
        triy2 /= object._radius[1];
        triy3 /= object._radius[1];
        triz1 /= object._radius[2];
        triz2 /= object._radius[2];
        triz3 /= object._radius[2];
        bool updated = false;
        bool ret = _detect(object, results, trix1, triy1, triz1,
            trix3, triy3, triz3, trix2, triy2, triz2, updated);
        if (updated) {
            /* response still needs ellipsoid space coordinates */
            results._intersection_espace[0] = results.intersection[0];
            results._intersection_espace[1] = results.intersection[1];
            results._intersection_espace[2] = results.intersection[2];
            results.intersection[0] *= object._radius[0];
            results.intersection[1] *= object._radius[1];
            results.intersection[2] *= object._radius[2];
        }
        return ret;
    }

    /*
     * Triangle is defined by its 3 points. Triangle points are in
     * anti-clockwise order required by the CG software renderer.
     * Optimized for raw data from the CGQuadric class.
     * Parameters are converted to ellipsoid space internally.
     * Intersection is converted from ellipsoid space.
     */
    static inline bool detect(CollisionObject &object,
        CollisionInfo &results, const float *tri)
    {
        return detect(object, results,
            tri[0], tri[1], tri[2],
            tri[3], tri[4], tri[5],
            tri[6], tri[7], tri[8]);
    }

    /*
     * Another collision detection technique. Because the collision object is a
     * sphere, we can add another one to perform a sphere-sphere collision
     * check.
     * Requires the CollisionObjects to have the same radius in all directions.
     */
    static bool detect(CollisionObject &o1, CollisionObject &o2,
        SweepInfo &sweep0, SweepInfo &sweep1);

    /*
     * Sweep an object based on existing collision results.
     * 1. Check the collision as usual using CollisionDetect::detect().
     * 2. Feed the information to sweep_pass1.
     * 3. Check the collision with sweep.pos as new position and sweep.vel as
     *    new velocity.
     * 4. Feed the information to sweep_pass2.
     * 5. Check the collision with sweep.pos as new position and sweep.vel as
     *    new velocity.
     * 6. Feed the information to sweep_pass3.
     * 7. Call sweep_done. sweep.pos is the new result.
     * If collision check after first pass fails, call sweep_done() and read
     * sweep.pos.
     */
    static void sweep_pass1(CollisionObject &object,
        CollisionInfo &first_result, SweepInfo &sweep);
    static void sweep_pass2(CollisionObject &object,
        CollisionInfo &first_result, SweepInfo &sweep);
    static void sweep_pass3(CollisionObject &object,
        CollisionInfo &first_result, SweepInfo &sweep);
    static void sweep_done(CollisionObject &object, SweepInfo &sweep);
};

#endif
