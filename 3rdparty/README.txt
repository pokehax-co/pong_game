Inline source code of GLEW (git 2015-12-28) and GLFW 3.1.2 for mingw-w64 and
Visual Studio builds.

The GLFW source code is slightly patched. It contains a glfw_config.h file
for Linux, and a glfw_config.mingw.h for mingw-w64 (created during build).

Replace the glfw_config.h include in internal.h with following code:

    #if defined(_GLFW_USE_MINGW32_CONFIG_H)
     #include "glfw_config.mingw.h"
    #else
    #if defined(_GLFW_USE_CONFIG_H)
     #include "glfw_config.h"
    #endif
    #endif

Replace the glxext.h include in glx_context.h with the following line:

    #include <GL/glxext.h>

Licensing details:

See COPYING_GLFW.txt for license of GLFW.
See LICENSE_GLEW.txt for license of GLEW.
See LICENSE_glext.txt for license of OpenGL headers.
