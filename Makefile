CXX=g++
CXXFLAGS=-O3 -ffast-math -g -Wall -fopenmp -std=c++98 -pedantic -Ilibcg1framework -I3rdparty/include
LDFLAGS=-Wl,-z,defs -g -fopenmp
LIBS=-static-libstdc++ -static-libgcc -lGL -lX11 -lXrandr -lXcursor -lXinerama -lXxf86vm -lXi -ldl -lrt
AR=ar

OBJS=game.o coll.o map.o renderer.o

.PHONY: all clean
all: game
clean:
	rm -f *.o game game.exe
	$(MAKE) -C libcg1framework clean
	$(MAKE) -C 3rdparty clean
libcg1framework/libcg1framework.a: libcg1framework/*.cc libcg1framework/*.cpp libcg1framework/*.h
	$(MAKE) -C libcg1framework
3rdparty/libGLEW.a:
	$(MAKE) -C 3rdparty
3rdparty/libglfw3.a:
	$(MAKE) -C 3rdparty
game: $(OBJS) libcg1framework/libcg1framework.a 3rdparty/libGLEW.a 3rdparty/libglfw3.a
	$(CXX) $(LDFLAGS) $^ -o $@ $(LIBS)
%.o: %.cc
	$(CXX) $(CXXFLAGS) -c $< -o $@

# deps
coll.o: coll.h
game.o: coll.h game.h renderer.h map.h config.h
renderer.o: game.h renderer.h map.h config.h
map.cc: map.h config.h
