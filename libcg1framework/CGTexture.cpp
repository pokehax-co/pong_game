#include "CGTexture.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "CGMath.h"

//---------------------------------------------------------------------------
// CGTEXTURE2D
//---------------------------------------------------------------------------
CGTexture2D::CGTexture2D()
{
    width=height=0;
    filterMode=CG_NEAREST;
    wrapMode=CG_CLAMP;
    data=NULL;
}
//---------------------------------------------------------------------------
CGTexture2D::~CGTexture2D()
{
    free(data); data=NULL;
}
//---------------------------------------------------------------------------
void CGTexture2D::allocate(int width, int height, const unsigned char* data4ub)
{
    this->width = width;
    this->height = height;
    data = (unsigned char*) realloc(data,width*height*4);
    memcpy(data,data4ub,width*height*4);
}
//---------------------------------------------------------------------------
void CGTexture2D::setFilterMode(int mode)
{
    filterMode = mode;
}
//---------------------------------------------------------------------------
void CGTexture2D::setWrapMode(int mode)
{
    wrapMode = mode;
}
//---------------------------------------------------------------------------
CGVec4 CGTexture2D::fetchTexel(int x, int y) const
{
    CGVec4 sample;

    if (wrapMode == CG_CLAMP) {
        if (x < 0) x = 0;
        if (x >= width) x = width - 1;
        if (y < 0) y = 0;
        if (y >= height) y = height - 1;
    }
    else {
        while (x < 0) x += width;
        x %= width;
        while (y < 0) y += height;
        y %= height;
    }

    unsigned char *p = data + 4 * ((y * width) + x);
    sample.set(p[0] / 255.f, p[1] / 255.f, p[2] / 255.f, p[3] / 255.f);
    return sample;
}
//---------------------------------------------------------------------------
CGVec4 CGTexture2D::sample(const CGVec4 &texcoord) const
{
    if (filterMode == CG_NEAREST) {
        int x = (int)floorf(texcoord[0] * width);
        int y = (int)floorf(texcoord[1] * height);
        return fetchTexel(x, y);
    }
    else {
        /* bilinear filtering */
        float posx = texcoord[0] * width - .5f;
        float posy = texcoord[1] * height - .5f;
        int x0 = (int)floorf(posx);
        int y0 = (int)floorf(posy);
        CGVec4 s1 = fetchTexel(x0, y0);
        CGVec4 s2 = fetchTexel(x0 + 1, y0);
        CGVec4 s3 = fetchTexel(x0, y0 + 1);
        CGVec4 s4 = fetchTexel(x0 + 1, y0 + 1);
        float alpha = posx - x0;
        float beta = posy - y0;
        float alpha1 = 1.f - alpha;
        float beta1 = 1.f - beta;
        return (alpha1 * s1 + alpha * s2) * beta1 +
            (alpha1 * s3 + alpha * s4) * beta;
    }
}
//---------------------------------------------------------------------------
