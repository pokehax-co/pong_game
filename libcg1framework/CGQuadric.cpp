#include <string.h> // for memcpy
#include <stdlib.h> // for malloc
#define _USE_MATH_DEFINES
#include <math.h>
#ifndef M_PI
#define M_PI 3.1415926535897932384626433832795028841971693993751058209749
#endif

#include "CGQuadric.h"
#include "CGMath.h"

//---------------------------------------------------------------------------
//
//---------------------------------------------------------------------------
CGQuadric::CGQuadric()
{
    m_positions = m_normals = m_colors = m_texcoords = NULL;
    m_vertexCount = 0;
    m_standardRGBA[0]=m_standardRGBA[1]=m_standardRGBA[2]=m_standardRGBA[3]=1.0f;
}
//---------------------------------------------------------------------------
CGQuadric::CGQuadric(const CGQuadric &q)
{
    m_vertexCount = q.m_vertexCount;

    m_positions = (float*) malloc(sizeof(float)*3*m_vertexCount);
    memcpy(m_positions,q.m_positions,sizeof(float)*3*m_vertexCount);

    m_normals = (float*) malloc(sizeof(float)*4*m_vertexCount);
    memcpy(m_normals,q.m_normals,sizeof(float)*4*m_vertexCount);

    m_colors = (float*) malloc(sizeof(float)*4*m_vertexCount);
    memcpy(m_colors,q.m_colors,sizeof(float)*4*m_vertexCount);

    m_texcoords = (float*) malloc(sizeof(float)*2*m_vertexCount);
    memcpy(m_texcoords,q.m_texcoords,sizeof(float)*2*m_vertexCount);
}
//---------------------------------------------------------------------------
CGQuadric::~CGQuadric()
{
    free(m_positions);
    free(m_normals);
    free(m_colors);
    free(m_texcoords);
}
//---------------------------------------------------------------------------
// GETTER
//---------------------------------------------------------------------------
int CGQuadric::getVertexCount() const       { return m_vertexCount; }
float* CGQuadric::getPositionArray() const  { return m_positions;   }
float* CGQuadric::getNormalArray() const    { return m_normals;     }
float* CGQuadric::getColorArray() const     { return m_colors;      }
float* CGQuadric::getTexCoordArray() const  { return m_texcoords;   }
//---------------------------------------------------------------------------
void CGQuadric::setStandardColor(float r, float g, float b, float a)
{
    m_standardRGBA[0] = r;
    m_standardRGBA[1] = g;
    m_standardRGBA[2] = b;
    m_standardRGBA[3] = a;
}
void CGQuadric::applyModelView(const CGMatrix4x4 &mtx)
{
    m_modelview = mtx;
}
void CGQuadric::applyExtraModelView(const CGMatrix4x4 &mtx)
{
    m_modelview_xtra = mtx;
}
//---------------------------------------------------------------------------
void CGQuadric::m_addTriangle(float x0, float y0, float z0, float nx0, float ny0, float nz0,
                              float x1, float y1, float z1, float nx1, float ny1, float nz1,
                              float x2, float y2, float z2, float nx2, float ny2, float nz2)
{
    // Increase array storage by 3 vertices.
    m_positions = (float*)realloc(m_positions,sizeof(float)*3*(m_vertexCount+3));
    m_normals   = (float*)realloc(m_normals,sizeof(float)*4*(m_vertexCount+3));
    m_colors    = (float*)realloc(m_colors,sizeof(float)*4*(m_vertexCount+3));
    m_texcoords = (float*)realloc(m_texcoords,sizeof(float)*2*(m_vertexCount+3));

    // Get the next free array position.
    float *p=m_positions+3*m_vertexCount,
          *n=m_normals  +4*m_vertexCount,
          *c=m_colors   +4*m_vertexCount,
          *t=m_texcoords+2*m_vertexCount;

    CGVec4 N, M;

    /* Apply two model-view matrices to the positions */
#define SET_3(p, x, y, z) \
    N.set(x, y, z, 1.f); \
    N = m_modelview_xtra * m_modelview * N; \
    (p)[0] = N[0]; (p)[1] = N[1]; (p)[2] = N[2]

    /* Apply one model-view matrix to the normals (so that they get rotated
       properly). Scaling is irrelevant, because the normals are normalized
       anyway. But translating destroys them, effectively. */
#define SET_N(p, x, y, z, u, v, w) \
    M.set(u, v, w, 1.f); \
    M = m_modelview * M; \
    CGMath::normalize(M); \
    (p)[0] = M[0]; (p)[1] = M[1]; (p)[2] = M[2]; (p)[3] = -(M[0]*N[0]+M[1]*N[1]+M[2]*N[2])

#define SET_4(p, V) \
    (p)[0] = V[0]; (p)[1] = V[1]; (p)[2] = V[2]; (p)[3] = V[3]

#define SET_2(p, X, Y) \
    (p)[0] = X; (p)[1] = Y

    SET_3(p+0,x0,y0,z0); SET_N(n+0,x0,y0,z0,nx0,ny0,nz0); SET_2(t+0,0,0); SET_4(c+0,m_standardRGBA);
    SET_3(p+3,x1,y1,z1); SET_N(n+4,x1,y1,z1,nx1,ny1,nz1); SET_2(t+2,0,0); SET_4(c+4,m_standardRGBA);
    SET_3(p+6,x2,y2,z2); SET_N(n+8,x2,y2,z2,nx2,ny2,nz2); SET_2(t+4,0,0); SET_4(c+8,m_standardRGBA);

    // Increase internal vertex count.
    m_vertexCount+=3;
}
//---------------------------------------------------------------------------
void CGQuadric::m_addQuad(float x0, float y0, float z0, float nx0, float ny0, float nz0,
                          float x1, float y1, float z1, float nx1, float ny1, float nz1,
                          float x2, float y2, float z2, float nx2, float ny2, float nz2,
                          float x3, float y3, float z3, float nx3, float ny3, float nz3)
{
    m_addTriangle(x0,y0,z0,nx0,ny0,nz0,
                  x1,y1,z1,nx1,ny1,nz1,
                  x2,y2,z2,nx2,ny2,nz2);
    m_addTriangle(x0,y0,z0,nx0,ny0,nz0,
                  x2,y2,z2,nx2,ny2,nz2,
                  x3,y3,z3,nx3,ny3,nz3);
}
//---------------------------------------------------------------------------
void CGQuadric::createBox()
{
    /* Create a box without slicing the sides */
    createBox(1, 1, 1);
}
//---------------------------------------------------------------------------

/*
 * note: attempting to normalize normals so that ||normal_vector|| = 1
 * FIXME revise this, colors are not quite right
 */
void CGQuadric::createBox(int slicesX, int slicesY, int slicesZ)
{
    float x, y, z;

    if (slicesX <= 0 || slicesY <= 0 || slicesZ <= 0)
        return;
    float slice_inc_x = 2.f/(float)slicesX;
    float slice_inc_y = 2.f/(float)slicesY;
    float slice_inc_z = 2.f/(float)slicesZ;

    /*
     * Two layers whose normals are parallel to the Z axis (z = 1, z = -1)
     */
#if 1
    z = 1.f;
    for (x = -1.f; x < 1.f; x += slice_inc_x) {
        for (y = -1.f; y < 1.f; y += slice_inc_y) {
            m_addQuad(
                x,               y              , z, 0.f, 0.f, z,
                x + slice_inc_x, y              , z, 0.f, 0.f, z,
                x + slice_inc_x, y + slice_inc_y, z, 0.f, 0.f, z,
                x,               y + slice_inc_y, z, 0.f, 0.f, z
            );
        }
    }
#endif

#if 1
    z = -1.f;
    for (x = -1.f; x < 1.f; x += slice_inc_x) {
        for (y = -1.f; y < 1.f; y += slice_inc_y) {
            m_addQuad(
                x,               y + slice_inc_y, z, 0.f, 0.f, z,
                x + slice_inc_x, y + slice_inc_y, z, 0.f, 0.f, z,
                x + slice_inc_x, y              , z, 0.f, 0.f, z,
                x,               y              , z, 0.f, 0.f, z
            );
        }
    }
#endif

    /*
     * Two layers whose normals are parallel to the X axis (x = 1, x = -1)
     */
#if 1
    x = 1.f;
    for (y = -1.f; y < 1.f; y += slice_inc_y) {
        for (z = -1.f; z < 1.f; z += slice_inc_z) {
            m_addQuad(
                x, y + slice_inc_y, z,               x, 0.f, 0.f,
                x, y + slice_inc_y, z + slice_inc_z, x, 0.f, 0.f,
                x, y              , z + slice_inc_z, x, 0.f, 0.f,
                x, y              , z,               x, 0.f, 0.f
            );
        }
    }
#endif

#if 1
    x = -1.f;
    for (y = -1.f; y < 1.f; y += slice_inc_y) {
        for (z = -1.f; z < 1.f; z += slice_inc_z) {
            m_addQuad(
                x, y              , z,               x, 0.f, 0.f,
                x, y              , z + slice_inc_z, x, 0.f, 0.f,
                x, y + slice_inc_y, z + slice_inc_z, x, 0.f, 0.f,
                x, y + slice_inc_y, z,               x, 0.f, 0.f
            );
        }
    }
#endif

    /*
     * Two layers whose normals are parallel to the Y axis (y = 1, y = -1)
     */
#if 1
    y = 1.f;
    for (x = -1.f; x < 1.f; x += slice_inc_x) {
        for (z = -1.f; z < 1.f; z += slice_inc_z) {
            m_addQuad(
                x,               y, z + slice_inc_z, 0.f, y, 0.f,
                x + slice_inc_x, y, z + slice_inc_z, 0.f, y, 0.f,
                x + slice_inc_x, y, z,               0.f, y, 0.f,
                x,               y, z,               0.f, y, 0.f
            );
        }
    }
#endif

#if 1
    y = -1.f;
    for (x = -1.f; x < 1.f; x += slice_inc_x) {
        for (z = -1.f; z < 1.f; z += slice_inc_z) {
            m_addQuad(
                x + slice_inc_x, y, z,               0.f, y, 0.f,
                x + slice_inc_x, y, z + slice_inc_z, 0.f, y, 0.f,
                x,               y, z + slice_inc_z, 0.f, y, 0.f,
                x,               y, z,               0.f, y, 0.f
            );
        }
    }
#endif
}
//---------------------------------------------------------------------------
void CGQuadric::createUVSphere(int slices, int stacks)
{
    const float pi = 3.14159265359f;
    float phi, theta;

    if (slices <= 2 || stacks <= 1)
        return;
    float phi_inc = 360.f / slices;
    float theta_inc = 180.f / stacks;

    for (theta = -90.f; theta < 90.f; theta += theta_inc) {
        for (phi = 0.f; phi < 360.f; phi += phi_inc) {
            float phi_rad = phi * pi / 180.f;
            float phi_next_rad = (phi + phi_inc) * pi / 180.f;
            float theta_rad = theta * pi / 180.f;
            float theta_next_rad = (theta + theta_inc) * pi / 180.f;

            /*
             * Because it's a regular sphere with radius 1 at all points,
             * its normal vector is equal to its parameter representation,
             * and its norm is 1.
             */

#define NORM(vec) \
    sqrt(vec[0] * vec[0] + vec[1] * vec[1] + vec[2] * vec[2])
#define PARAM(vec) \
    vec[0], vec[1], vec[2], vec[0], vec[1], vec[2]

            float x_vec[3], x_vec_nextrow[3];
            x_vec[0] = cosf(theta_rad) * cosf(phi_rad);
            x_vec[1] = cosf(theta_rad) * sinf(phi_rad);
            x_vec[2] = sinf(theta_rad);
            x_vec_nextrow[0] = cosf(theta_next_rad) * cosf(phi_rad);
            x_vec_nextrow[1] = cosf(theta_next_rad) * sinf(phi_rad);
            x_vec_nextrow[2] = sinf(theta_next_rad);

            float x_vec_next[3], x_vec_nextrow_next[43];
            x_vec_next[0] = cosf(theta_rad) * cosf(phi_next_rad);
            x_vec_next[1] = cosf(theta_rad) * sinf(phi_next_rad);
            x_vec_next[2] = x_vec[2];
            x_vec_nextrow_next[0] = cosf(theta_next_rad) * cosf(phi_next_rad);
            x_vec_nextrow_next[1] = cosf(theta_next_rad) * sinf(phi_next_rad);
            x_vec_nextrow_next[2] = x_vec_nextrow[2];

            /*
             * We're drawing towards the poles, this needs triangles.
             */
            if (theta <= -90.f) {
                m_addTriangle(
                    0.f, 0.f, -1.f, 0.f, 0.f, -1.f,
                    PARAM(x_vec_nextrow_next),
                    PARAM(x_vec_nextrow)
                );
            }
            else if (theta + theta_inc >= 90.f) {
                m_addTriangle(
                    0.f, 0.f, 1.f, 0.f, 0.f, 1.f,
                    PARAM(x_vec),
                    PARAM(x_vec_next)
                );
            }
            else {
                m_addQuad(
                    PARAM(x_vec),
                    PARAM(x_vec_next),
                    PARAM(x_vec_nextrow_next),
                    PARAM(x_vec_nextrow)
                );
            }

#undef PARAM
#undef NORM
        }
    }
}
//---------------------------------------------------------------------------
static void divide(float *input, float m01[3], float m12[3], float m20[3])
{
    float f;
    /*
     * 1. interpret normalized input coordinates as vectors
     * 2. add first + second = m01, second + third = m12, third + first = m20
     * 3. normalize m01, m12, m20
     */
    m01[0] = input[0] + input[3];
    m01[1] = input[1] + input[4];
    m01[2] = input[2] + input[5];
    f = 1.f/sqrtf(m01[0]*m01[0] + m01[1]*m01[1] + m01[2]*m01[2]);
    m01[0] *= f;
    m01[1] *= f;
    m01[2] *= f;

    m12[0] = input[3] + input[6];
    m12[1] = input[4] + input[7];
    m12[2] = input[5] + input[8];
    f = 1.f/sqrtf(m12[0]*m12[0] + m12[1]*m12[1] + m12[2]*m12[2]);
    m12[0] *= f;
    m12[1] *= f;
    m12[2] *= f;

    m20[0] = input[6] + input[0];
    m20[1] = input[7] + input[1];
    m20[2] = input[8] + input[2];
    f = 1.f/sqrtf(m20[0]*m20[0] + m20[1]*m20[1] + m20[2]*m20[2]);
    m20[0] *= f;
    m20[1] *= f;
    m20[2] *= f;
}
void CGQuadric::createIcoSphere(int subdiv)
{
    if (subdiv <= 0)
        return;

    const float t = (1.f + 2.2360679775f /* sqrt(5) */) * .5f;
    const float s = 1.f / sqrtf(1.f + t * t);
    #define v0  t, 1.f, 0.f
    #define v1  -t, 1.f, 0.f
    #define v2  t, -1.f, 0.f
    #define v3  -t, -1.f, 0.f
    #define v4  1.f, 0.f, t
    #define v5  1.f, 0.f, -t
    #define v6  -1.f, 0.f, t
    #define v7  -1.f, 0.f, -t
    #define v8  0.f, t, 1.f
    #define v9  0.f, -t, 1.f
    #define v10 0.f, t, -1.f
    #define v11 0.f, -t, -1.f

    /* It is 2015 and Visual C++'s preprocessor still needs this EXPAND(...)
       thingy... */
    #define ADD_TRIANGLE_(a, b, c) \
        *ptrp++ = s*a; *ptrp++ = s*b; *ptrp++ = s*c
    #define EXPAND(x) x
    #define ADD_TRIANGLE(a, b, c) \
        EXPAND(ADD_TRIANGLE_(a); ADD_TRIANGLE_(b); ADD_TRIANGLE_(c))

    float *ptr = NULL;
    int prev_faces = 0;
    for (int pass = 1; pass <= subdiv; ++pass) {
        /*
         * First pass, init the 20 triangles.
         */
        if (!ptr) {
            ptr = new float[9 * 20];
            float *ptrp = ptr;

            ADD_TRIANGLE(v0, v8, v4);
            ADD_TRIANGLE(v0, v5, v10);
            ADD_TRIANGLE(v2, v4, v9);
            ADD_TRIANGLE(v2, v11, v5);
            ADD_TRIANGLE(v1, v6, v8);
            ADD_TRIANGLE(v1, v10, v7);
            ADD_TRIANGLE(v3, v9, v6);
            ADD_TRIANGLE(v3, v7, v11);
            ADD_TRIANGLE(v0, v10, v8);
            ADD_TRIANGLE(v1, v8, v10);
            ADD_TRIANGLE(v2, v9, v11);
            ADD_TRIANGLE(v11, v9, v3); // this triangle was not anti-clockwise!
            ADD_TRIANGLE(v4, v2, v0);
            ADD_TRIANGLE(v5, v0, v2);
            ADD_TRIANGLE(v6, v1, v3);
            ADD_TRIANGLE(v7, v3, v1);
            ADD_TRIANGLE(v8, v6, v4);
            ADD_TRIANGLE(v9, v4, v6);
            ADD_TRIANGLE(v10, v5, v7);
            ADD_TRIANGLE(v11, v7, v5);
            prev_faces = 20;
        }
        /*
         * Subsequent pass, 1 triangle => 4 triangles (3 vertices => 12
         * vertices), so we need to multiply the number of vertices by 4 with
         * each pass.
         *
         *   A   m20   C
         *    *---*---*
         *     \ / \ /
         *   m01\---/m12
         *       \ /
         *        *
         *        B
         */
        else {
            float *upd = new float[9 * prev_faces * 4];
            float *updp = upd, *ptrp = ptr;

            for (int i = 0; i < prev_faces; ++i) {
                float m01[3], m12[3], m20[3];

                divide(ptrp, m01, m12, m20);

                *updp++ = m20[0]; *updp++ = m20[1]; *updp++ = m20[2];
                *updp++ = *ptrp++; *updp++ = *ptrp++; *updp++ = *ptrp++;
                *updp++ = m01[0]; *updp++ = m01[1]; *updp++ = m01[2];

                *updp++ = m01[0]; *updp++ = m01[1]; *updp++ = m01[2];
                *updp++ = *ptrp++; *updp++ = *ptrp++; *updp++ = *ptrp++;
                *updp++ = m12[0]; *updp++ = m12[1]; *updp++ = m12[2];

                *updp++ = m12[0]; *updp++ = m12[1]; *updp++ = m12[2];
                *updp++ = *ptrp++; *updp++ = *ptrp++; *updp++ = *ptrp++;
                *updp++ = m20[0]; *updp++ = m20[1]; *updp++ = m20[2];

                *updp++ = m20[0]; *updp++ = m20[1]; *updp++ = m20[2];
                *updp++ = m01[0]; *updp++ = m01[1]; *updp++ = m01[2];
                *updp++ = m12[0]; *updp++ = m12[1]; *updp++ = m12[2];
            }
            delete[] ptr;
            ptr = upd;
            prev_faces *= 4;
        }
    }
    for (int i = 0; i < 9 * prev_faces; i += 9)
        m_addTriangle(
            ptr[i], ptr[i+1], ptr[i+2],
            ptr[i], ptr[i+1], ptr[i+2],
            ptr[i+3], ptr[i+4], ptr[i+5],
            ptr[i+3], ptr[i+4], ptr[i+5],
            ptr[i+6], ptr[i+7], ptr[i+8],
            ptr[i+6], ptr[i+7], ptr[i+8]
        );
    delete[] ptr;
}
//---------------------------------------------------------------------------
void CGQuadric::createCylinder(int slices, int stacks)
{
    const float pi = 3.14159265359f;
    float phi, z;

    if (slices <= 2 || stacks <= 0)
        return;
    float phi_inc = 360.f / slices;
    float z_inc = 1.f / stacks;

    for (z = 0.f; z < 1.f; z += z_inc) {
        for (phi = 0.f; phi < 360.f; phi += phi_inc) {
            float phi_rad = phi * pi / 180.f;
            float phi_next_rad = (phi + phi_inc) * pi / 180.f;
            float x = cosf(phi_rad);
            float y = sinf(phi_rad);
            float x_next = cosf(phi_next_rad);
            float y_next = sinf(phi_next_rad);
            float z_next = z + z_inc;

            /*
             * The normal vector is actually the parameter representation of
             * the unit circle, which has a norm of 1.
             */
            m_addQuad(
                x,      y,      z,      x,      y,      0.f,
                x_next, y_next, z,      x_next, y_next, 0.f,
                x_next, y_next, z_next, x_next, y_next, 0.f,
                x,      y,      z_next, x,      y,      0.f
            );
        }
    }
}
//---------------------------------------------------------------------------
void CGQuadric::createDisk(int slices, int loops)
{
    const float pi = 3.14159265359f;
    float phi, phi_scale;

    if (slices <= 1 || loops <= 0)
        return;
    float phi_inc = 360.f / slices;
    float phi_scale_inc = 1.f / loops;

    for (phi_scale = 0.f; phi_scale < 1.f; phi_scale += phi_scale_inc) {
        for (phi = 0.f; phi < 360.f; phi += phi_inc) {
            float phi_rad = phi * pi / 180.f;
            float phi_next_rad = (phi + phi_inc) * pi / 180.f;
            float phi_scale_next = phi_scale + phi_scale_inc;

            float x_vec[2], x_vec_nextrow[2];
            x_vec[0] = phi_scale * cosf(phi_rad);
            x_vec[1] = phi_scale * sinf(phi_rad);
            x_vec_nextrow[0] = phi_scale_next * cosf(phi_rad);
            x_vec_nextrow[1] = phi_scale_next * sinf(phi_rad);

            float x_vec_next[2], x_vec_nextrow_next[2];
            x_vec_next[0] = phi_scale * cosf(phi_next_rad);
            x_vec_next[1] = phi_scale * sinf(phi_next_rad);
            x_vec_nextrow_next[0] = phi_scale_next * cosf(phi_next_rad);
            x_vec_nextrow_next[1] = phi_scale_next * sinf(phi_next_rad);

            /*
             * We're drawing close to (0,0,0), this needs triangles.
             */
            if (phi_scale <= 0.f) {
                m_addTriangle(
                    0.f,                   0.f,                   0.f, 0.f, 0.f, 1.f,
                    x_vec_nextrow[0],      x_vec_nextrow[1],      0.f, 0.f, 0.f, 1.f,
                    x_vec_nextrow_next[0], x_vec_nextrow_next[1], 0.f, 0.f, 0.f, 1.f
                );
            }

            /*
             * We're drawing below the tip, this needs quads.
             */
            else {
                m_addQuad(
                    x_vec[0],              x_vec[1],              0.f, 0.f, 0.f, 1.f,
                    x_vec_nextrow[0],      x_vec_nextrow[1],      0.f, 0.f, 0.f, 1.f,
                    x_vec_nextrow_next[0], x_vec_nextrow_next[1], 0.f, 0.f, 0.f, 1.f,
                    x_vec_next[0],         x_vec_next[1],         0.f, 0.f, 0.f, 1.f
                );
            }
        }
    }
}
//---------------------------------------------------------------------------
void CGQuadric::createCone(int slices, int stacks)
{
    createCone(1.f, slices, stacks);
}
//---------------------------------------------------------------------------
static void cone(float phi_rad, float z, float out[2])
{
    /* v = (r cos φ, r sin φ, z) */
    out[0] = (1.f - z) * cosf(phi_rad);
    out[1] = (1.f - z) * sinf(phi_rad);
}
static void cone_normal(float phi_rad, float z, float out[3])
{
    /* n = (z' cos φ, z' sin φ, -r') */
    out[0] = cosf(phi_rad);
    out[1] = sinf(phi_rad);
    out[2] = 1.f;
}
void CGQuadric::createCone(float maxHeight, int slices, int stacks)
{
    const float pi = 3.14159265359f;
    float phi, z;

    if (maxHeight <= 0.f || maxHeight > 1.f || slices <= 2 || stacks <= 1)
        return;
    float phi_inc = 360.f / slices;
    float z_inc = maxHeight / stacks;

    for (z = maxHeight; z > 0.f; z -= z_inc) {
        for (phi = 0.f; phi < 360.f; phi += phi_inc) {
            float phi_rad = phi * pi / 180.f;
            float phi_next_rad = (phi + phi_inc) * pi / 180.f;
            float z_next = z - z_inc;

            float x_vec[2], x_vec_nextrow[2];
            float x_vec_normal[3], x_vec_nextrow_normal[3];
            cone(phi_rad, z,      x_vec);
            cone(phi_rad, z_next, x_vec_nextrow);
            cone_normal(phi_rad, z,      x_vec_normal);
            cone_normal(phi_rad, z_next, x_vec_nextrow_normal);

            float x_vec_next[2], x_vec_nextrow_next[2];
            float x_vec_next_normal[3], x_vec_nextrow_next_normal[3];
            cone(phi_next_rad, z,      x_vec_next);
            cone(phi_next_rad, z_next, x_vec_nextrow_next);
            cone_normal(phi_next_rad, z,      x_vec_next_normal);
            cone_normal(phi_next_rad, z_next, x_vec_nextrow_next_normal);

#define PARAM(vec, which_z) \
    vec[0], vec[1], which_z, vec##_normal[0], vec##_normal[1], vec##_normal[2]

            /*
             * We're drawing the tip of the cone, this needs triangles.
             * The normal vector for the actual tip is undefined.
             */
            if (z >= 1.f) {
                m_addTriangle(
                    0.f, 0.f, 1.f, 0.f, 0.f, 0.f,
                    PARAM(x_vec_nextrow, z_next),
                    PARAM(x_vec_nextrow_next, z_next)
                );
            }

            /*
             * We're drawing below the tip, this needs quads.
             */
            else {
                m_addQuad(
                    PARAM(x_vec, z),
                    PARAM(x_vec_nextrow, z_next),
                    PARAM(x_vec_nextrow_next, z_next),
                    PARAM(x_vec_next, z)
                );
            }

#undef PARAM
#undef NORM
        }
    }
}
//---------------------------------------------------------------------------
