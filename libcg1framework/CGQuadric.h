#ifndef CG_QUADRIC_H
#define CG_QUADRIC_H

#include "CG.h"
#include "CGMatrix.h"
//---------------------------------------------------------------------------
// CGQuadric-Class
// creates and holds polygonal (triangulated) discretizations
// of objects which can be described through quadratic equations.
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
class CGQuadric
{
public:
    /// Empty constructor.
    CGQuadric();
    /// Copy constructor.
    CGQuadric(const CGQuadric &q);
    /// Destructor.
    ~CGQuadric();
    /// Vertex count getter:
    int getVertexCount() const;
    /// Vertex positions getter:
    float* getPositionArray() const;
    /// Vertex normals getter:
    float* getNormalArray() const;
    /// Vertex colors getter:
    float* getColorArray() const;
    /// Vertex texture coordinates getter:
    float* getTexCoordArray() const;
    /// Set the color (before!) creating the object.
    void setStandardColor(float r, float g, float b, float a=1);
    /// Applies a model-view matrix to both positions and normals.
    /// Make sure that the matrix does not contain a translation!
    void applyModelView(const CGMatrix4x4 &mtx);
    /// Applies an additional model-view matrix to positions only.
    /// For translation matrices only.
    void applyExtraModelView(const CGMatrix4x4 &mtx);

    /// Create a cubical object.
    void createBox();
    void createBox(int slicesX, int slicesY, int slicesZ);
    /// Create a sphere object.
    void createUVSphere(int slices, int stacks);
    void createIcoSphere(int subdiv);
    /// Create a cylinder object.
    void createCylinder(int slices, int stacks);
    /// Create a disk object.
    void createDisk(int slices, int loops);
    /// Create a cone object.
    void createCone(int slices, int stacks);
    void createCone(float maxHeight, int slices, int stacks);

private:
    /// Internal method to add single triangle.
    void m_addTriangle(float x0, float y0, float z0, float nx0, float ny0, float nz0,
                       float x1, float y1, float z1, float nx1, float ny1, float nz1,
                       float x2, float y2, float z2, float nx2, float ny2, float nz2);

    /// Internal method to add single quad.
    void m_addQuad(float x0, float y0, float z0, float nx0, float ny0, float nz0,
                   float x1, float y1, float z1, float nx1, float ny1, float nz1,
                   float x2, float y2, float z2, float nx2, float ny2, float nz2,
                   float x3, float y3, float z3, float nx3, float ny3, float nz3);

    /// Internal pointers to data.
    float *m_positions, *m_normals, *m_colors, *m_texcoords;

    /// Internal vertex count.
    int m_vertexCount;
    float m_standardRGBA[4];

    CGMatrix4x4 m_modelview;
    CGMatrix4x4 m_modelview_xtra;
};
//---------------------------------------------------------------------------

#endif
