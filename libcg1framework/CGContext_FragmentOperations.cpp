#include "CGContext.h"

//---------------------------------------------------------------------------
// CGCONTEXT (FRAGMENT OPERATIONS)
//---------------------------------------------------------------------------
void CGContext::m_cgPushFragment(CGFragmentData& fragment)
{
    // Wait for enough fragments to process.
    m_pipelineFragments[m_pipelineFragmentsCount++] = fragment;
    if(m_pipelineFragmentsCount == CG_MAX_FRAGMENTS_IN_PIPELINE)
        m_cgFlushFragments();
}
//---------------------------------------------------------------------------
void CGContext::m_cgFlushFragments()
{
    // Run fragment pipeline components for each fragment.
    // Uncomment following line for parallel fragment processing (if OpenMP is activated)

    //#pragma omp parallel for
    for(int i=0; i<m_pipelineFragmentsCount; ++i)
        m_cgFragmentPipeline(m_pipelineFragments[i]);

    // All fragments processed, clear pipeline.
    m_pipelineFragmentsCount = 0;
}
//---------------------------------------------------------------------------
void CGContext::m_cgFragmentPipeline(CGFragmentData& fragment)
{
    if(!m_cgFragmentClipping(fragment))
        return;

    m_cgFragmentProgram(fragment);
    if(m_capabilities.depthTest && !m_cgFragmentZTest(fragment))
        return;

    if(m_capabilities.blend)
        m_cgFragmentBlending(fragment);

    m_cgFragmentWriteBuffer(fragment);
}
//---------------------------------------------------------------------------
bool CGContext::m_cgFragmentClipping(const CGFragmentData& fragment)
{
    const CGVec2i &pos = fragment.coordinates;
    if (pos[0] < 0 || pos[1] < 0 || pos[0] >= m_frameBuffer.getWidth() ||
        pos[1] >= m_frameBuffer.getHeight())
    {
        return false;
    }
    float z = fragment.varyings[CG_POSITION_VARYING][2];
    if (z < -1.f || z > 1.f)
        return false;
    return true;
}
//---------------------------------------------------------------------------
void CGContext::m_cgFragmentProgram(CGFragmentData& fragment)
{
    // Run programmable fragment processing.
    CGVec4 color;
    m_fragmentProgram(fragment, color, m_uniforms);
    fragment.varyings[CG_COLOR_VARYING] = color;
}
//---------------------------------------------------------------------------
bool CGContext::m_cgFragmentZTest(const CGFragmentData& fragment)
{
    const float tolerance = 1e-6f;

    int x = fragment.coordinates[0];
    int y = fragment.coordinates[1];
    float z = fragment.varyings[CG_POSITION_VARYING][2];
    //float z = fragment.varyings[CG_POSITION_VARYING][3];
    float z_buffer = m_frameBuffer.depthBuffer.get(x, y);
    //printf("%f <= %f + %f\n", z, z_buffer, tolerance);
    return z <= z_buffer + tolerance;
}
//---------------------------------------------------------------------------
void CGContext::m_cgFragmentBlending(CGFragmentData& fragment)
{
    int x = fragment.coordinates[0];
    int y = fragment.coordinates[1];
    float fraga, rgba[4];
    m_frameBuffer.colorBuffer.get(x, y, rgba);

    fraga = fragment.varyings[CG_COLOR_VARYING][3];
    for (int i = 0; i <= 2; ++i) {
        fragment.varyings[CG_COLOR_VARYING][i] =
            fraga * fragment.varyings[CG_COLOR_VARYING][i] +
            (1.f - fraga) * rgba[i];
    }
}
//---------------------------------------------------------------------------
void CGContext::m_cgFragmentWriteBuffer(const CGFragmentData& fragment)
{
    // Write the current fragment into the framebuffer.
    // color into color buffer
    m_frameBuffer.colorBuffer.set(fragment.coordinates[0],
        fragment.coordinates[1],
        fragment.varyings[CG_COLOR_VARYING].elements);

    //printf("Writing %f\n", 1.f/fragment.varyings[CG_POSITION_VARYING][3]);
    m_frameBuffer.depthBuffer.set(fragment.coordinates[0],
        fragment.coordinates[1],
        fragment.varyings[CG_POSITION_VARYING][2]);
        //fragment.varyings[CG_POSITION_VARYING][3]);
}
//---------------------------------------------------------------------------
