#include "CGContext.h"
#include <stdio.h>

//---------------------------------------------------------------------------
// CGCONTEXT (VERTEX OPERATIONS)
//---------------------------------------------------------------------------
void CGContext::m_cgPullVertex(int vertexIndex)
{
    if (m_pipelineVerticesCount >= CG_MAX_VERTICES_IN_PIPELINE) {
        fprintf(stderr, "vertex per primitive limit reached");
        return;
    }
    // We copy all attributes from the attribute pointers to the vertex entry v.
    // If no pointer is given, the default value is used.
    CGVertexAttributes& v=m_pipelineVertexAttributes[m_pipelineVerticesCount++];

    if(m_pVertexAttributePointer[CG_POSITION_ATTRIBUTE]) {
        const float *pos=m_pVertexAttributePointer[CG_POSITION_ATTRIBUTE]+3*vertexIndex;
        v.attributes[CG_POSITION_ATTRIBUTE].set(pos[0], pos[1], pos[2], 1.0f);
    } else {
        v.attributes[CG_POSITION_ATTRIBUTE].set(0.0f, 0.0f, 0.0f, 1.0f);
    }

    if(m_pVertexAttributePointer[CG_NORMAL_ATTRIBUTE]) {
        const float *nrm=m_pVertexAttributePointer[CG_NORMAL_ATTRIBUTE]+4*vertexIndex;
        v.attributes[CG_NORMAL_ATTRIBUTE].set(nrm[0], nrm[1], nrm[2], nrm[3]);
    } else {
        v.attributes[CG_NORMAL_ATTRIBUTE].set(0.0f, 0.0f, 1.0f, 0.0f);
    }

    if(m_pVertexAttributePointer[CG_COLOR_ATTRIBUTE]) {
        const float *col=m_pVertexAttributePointer[CG_COLOR_ATTRIBUTE]+4*vertexIndex;
        v.attributes[CG_COLOR_ATTRIBUTE].set(col[0], col[1], col[2], col[3]);
    } else {
        v.attributes[CG_COLOR_ATTRIBUTE].set(0.0f, 0.0f, 0.0f, 1.0f);
    }

    if(m_pVertexAttributePointer[CG_TEXCOORD_ATTRIBUTE]) {
        const float *tex=m_pVertexAttributePointer[CG_TEXCOORD_ATTRIBUTE]+2*vertexIndex;
        v.attributes[CG_TEXCOORD_ATTRIBUTE].set(tex[0], tex[1], 0.0f, 1.0f);
    } else {
        v.attributes[CG_TEXCOORD_ATTRIBUTE].set(0.0f, 0.0f, 0.0f, 1.0f);
    }
}
//---------------------------------------------------------------------------
void CGContext::m_cgVertexPipeline()
{
    // Do first part of vertex processing right here.
    m_cgVertexProgram();

    // Process this vertex for the current primitive.
    m_cgPrimitiveProcessing();

    // Note: rest of the 'original' vertex pipeline is finished after
    // the primitive processing because new vertices might have been
    // created after clipping.
}
//---------------------------------------------------------------------------
void CGContext::m_cgVertexProgram()
{
    // Run programmable vertex processing.
    m_vertexProgram(m_pipelineVertexAttributes[m_pipelineVerticesCount-1],
                    m_pipelineVertexVaryings[m_pipelineVerticesCount-1],
                    m_uniforms);
}
//---------------------------------------------------------------------------
void CGContext::m_cgPrimitiveProcessing()
{
    // Primitive assembly stage: collect vertices in m_pipelineVertexVaryings
    // until enough entries for the current primitive are present.
    int verticesNeeded = 0;
    switch(m_primitiveMode) {
        case CG_POINTS:     verticesNeeded = 1; break;
        case CG_LINES:      verticesNeeded = 2; break;
        case CG_TRIANGLES:  verticesNeeded = 3; break;
        // Insert other primitive types here.
    }
    if (m_pipelineVerticesCount < verticesNeeded)
        return;
    // else m_pipelineVerticesCount == verticesNeeded and we can process this primitive

    // This might split our one primitive and create new ones.
    int pipelinePrimitiveCount = m_cgClipPrimitive();

    // Finish the 'original' vertex pipeline for all pipeline vertices.
    for(int i=0; i<m_pipelineVerticesCount; i++) {
        m_cgVertexPerspectiveDivide(i);
        m_cgVertexViewportTransform(i);
    }

    // We might have to rasterize more than one primitive.
    for(int i=0; i<pipelinePrimitiveCount; i++) {

        m_cgRasterize(i);
    }

    // Purge vertex list.
    m_pipelineVerticesCount = 0;
}
//---------------------------------------------------------------------------
void CGContext::m_cgVertexPerspectiveDivide(int pipelineVertex)
{
    // We are interested in the last vertex (as always if no vertex index available).
    CGVertexVaryings& v=m_pipelineVertexVaryings[pipelineVertex];
    CGVec4 &pos = v.varyings[CG_POSITION_VARYING];

    /*
     * "Just divide your coordinates by W.
     * After the modelview/projection transformation, your vertex is in 'clip
     * space'. X, Y and Z are then in the range -W to W (anything outside this
     * range is clipped). By dividing everything by W, your coordinates are now
     * in the -1 to 1 range ('normalized device coordinates')."
     */
    pos[3] = 1.f/pos[3];
    pos[0] *= pos[3];
    pos[1] *= pos[3];
    pos[2] *= pos[3];
}
//---------------------------------------------------------------------------
void CGContext::m_cgVertexViewportTransform(int pipelineVertex)
{
    // We are interested in the last vertex (as always if no vertex index available).
    CGVertexVaryings& v=m_pipelineVertexVaryings[pipelineVertex];
    CGVec4 &pos = v.varyings[CG_POSITION_VARYING];

    pos.elements[0] = .5f * (1.f + pos.elements[0]) * (float)m_viewport[2] + (float)m_viewport[0];
    pos.elements[1] = .5f * (1.f + pos.elements[1]) * (float)m_viewport[3] + (float)m_viewport[1];
}
