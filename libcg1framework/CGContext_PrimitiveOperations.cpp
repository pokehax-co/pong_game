#include "CGContext.h"
#include "CGPrimClipper.h"

static float _det_points(float Ax, float Ay, float Bx, float By, float Px,
    float Py)
{
    return (Ax - Px) * (By - Py) - (Ay - Py) * (Bx - Px);
}

//---------------------------------------------------------------------------
// CGCONTEXT (PRIMITIVE OPERATIONS)
//---------------------------------------------------------------------------
int CGContext::m_cgClipPrimitive()
{
    // take all vertices from m_pipelineVertexVaryings and clip (in clip space)
    // overwrite with new vertices. Could also handle QUADS and POLYGONS here!
    int newPrimCount = 1;
    switch(m_primitiveMode) {
        case CG_POINTS:
            m_pipelineVerticesCount =
                CGPrimClipper::clipPoint(m_pipelineVertexVaryings, m_pipelineVerticesCount);
            newPrimCount = m_pipelineVerticesCount;
            break;
        case CG_LINES:
            m_pipelineVerticesCount =
                CGPrimClipper::clipLine(m_pipelineVertexVaryings, m_pipelineVerticesCount);
            newPrimCount = m_pipelineVerticesCount/2;
            break;
        case CG_TRIANGLES:
            // Clipped vertices need to be re-assembled into triangles
            newPrimCount = CGPrimClipper::clipPoly(m_pipelineVertexVaryings, m_pipelineVerticesCount)-2;
            m_pipelineVerticesCount = 0; //reset the pipeline
            for (int i=0; i<newPrimCount; ++i) { //as triangle fan
                m_pipelineVertexVaryings[m_pipelineVerticesCount++]=CGPrimClipper::outBuf()[0];
                m_pipelineVertexVaryings[m_pipelineVerticesCount++]=CGPrimClipper::outBuf()[i+1];
                m_pipelineVertexVaryings[m_pipelineVerticesCount++]=CGPrimClipper::outBuf()[i+2];
            }
            break;
        // Insert other primitive types here.
        }

    // Returns the new primitive count
    return newPrimCount;
}
//---------------------------------------------------------------------------
bool CGContext::m_cgTriangleSetup(const CGVec4 &vertex0Position,
                                  const CGVec4 &vertex1Position,
                                  const CGVec4 &vertex2Position,
                                  float& area)
{
    area = _det_points(vertex0Position[0], vertex0Position[1],
        vertex1Position[0], vertex1Position[1],
        vertex2Position[0], vertex2Position[1]);
    if (m_capabilities.cullFace && area < 0.f)
        return false;
    return true;
}
//---------------------------------------------------------------------------
// CGCONTEXT RASTERIZERS
//---------------------------------------------------------------------------
void CGContext::m_cgRasterize(int primitiveNumber)
{
    // Rasterize one primitive with the correct rasterizer.
    // Note: without clipping, this is always primitiveNumber=0, using
    // vertices (0) for points, (0,1) for lines and (0,1,2) for triangles.
    // BUT: with clipping, more primitives might be created and vertices
    // (N*i+0, N*i+1, N*i+2) must be used (where N=3 for triangles).

    switch(m_primitiveMode) {
        case CG_POINTS:
            m_cgRasterizePoint(primitiveNumber*1);
            break;
        case CG_LINES:
            if (m_capabilities.useBresenham)
                m_cgRasterizeLineBresenham(primitiveNumber*2);
            else
                m_cgRasterizeStupidLine(primitiveNumber*2);
            break;
        case CG_TRIANGLES:
            if (m_polygonMode == CG_LINE)
                m_cgRasterizeWireTriangle(primitiveNumber*3);
            else
                m_cgRasterizeTriangle(primitiveNumber*3);
            break;
        // Insert other rasterizers here.
    }

    // Process any remaining fragments
    m_cgFlushFragments();
}
//---------------------------------------------------------------------------
void CGContext::m_cgRasterizePoint(int pipelineVertexOffset)
{
    // This primitive produces only one fragment.

    // We are interested only in one vertex data:
    CGVertexVaryings &vertex0 = m_pipelineVertexVaryings[pipelineVertexOffset+0];

    // Initialize the fragment.
    CGFragmentData fragment;
    fragment.set(vertex0);

    // And set coordinates. (SetFragment initializes just the attributes + varyings).
    fragment.coordinates.set((int) vertex0.varyings[CG_POSITION_VARYING][0],
                             (int) vertex0.varyings[CG_POSITION_VARYING][1]);

    // Push the fragment into the pipeline.
    m_cgPushFragment(fragment);
}
//---------------------------------------------------------------------------
void CGContext::m_cgRasterizeStupidLine(int pipelineVertexOffset)
{
    // This primitive produces many fragments from 2 vertices:
    CGVertexVaryings &vertex0 = m_pipelineVertexVaryings[pipelineVertexOffset+0];
    CGVertexVaryings &vertex1 = m_pipelineVertexVaryings[pipelineVertexOffset+1];

    // draw a line from vertex0 to vertex1
    CGVec2i from,to;
    from.set((int)vertex0.varyings[CG_POSITION_VARYING][0],
             (int)vertex0.varyings[CG_POSITION_VARYING][1]);
    to.set((int)vertex1.varyings[CG_POSITION_VARYING][0],
           (int)vertex1.varyings[CG_POSITION_VARYING][1]);

    if (from[0] > to[0]) {
        // swap from and to
        CGVec2i tmp;
        tmp=from;
        from=to;
        to=tmp;
    }

    // Fragment to work on (initialize from vertex, set coordinates, push).
    CGFragmentData fragment;
    fragment.set(vertex0);

    // draw a line from <from> to <to>

    // vertical line
    if (from[0] == to[0]) {
        while (from[1] <= to[1]) {
            fragment.coordinates.set(from[0], from[1]);
            m_cgPushFragment(fragment);
            ++from[1];
        }
    }
    else {
        float incr = (float)(to[1] - from[1]) / (float)(to[0] - from[0]);
        float n = from[1] - incr * from[0];

        while (from[0] <= to[0]) {
            int x = from[0];
            int y = (int)(incr * from[0] + n);
            if (x >= 0 && y >= 0) {
                fragment.coordinates.set(x, y);
                m_cgPushFragment(fragment);
            }
            ++from[0];
        }
    }
    // Uebung01, Aufgabe 4a) (Zusatzaufgabe)
    // ...
}
//---------------------------------------------------------------------------
static void swap_vectors(CGVec2i &a, CGVec2i &b)
{
    CGVec2i temp = a;
    a = b;
    b = temp;
}

void CGContext::_bresenham_helper(CGVertexVaryings &vertex0, CGVertexVaryings &vertex1)
{
    CGVec2i from, to;
    from.set((int)vertex0.varyings[CG_POSITION_VARYING][0],
        (int)vertex0.varyings[CG_POSITION_VARYING][1]);
    to.set((int)vertex1.varyings[CG_POSITION_VARYING][0],
        (int)vertex1.varyings[CG_POSITION_VARYING][1]);

    CGFragmentData fragment;
    fragment.set(vertex0);

    bool swap_xy = abs(to[1] - from[1]) > abs(to[0] - from[0]);
    if (swap_xy) {
        from.set(from[1], from[0]);
        to.set(to[1], to[0]);
    }
    bool swapped_again = from[0] > to[0];
    if (swapped_again)
        swap_vectors(from, to);

    int x = from[0], y = from[1];
    int dx = to[0] - from[0], dy = abs(to[1] - from[1]);
    int d = 2 * dy - dx, ystep = from[1] < to[1] ? 1 : -1;
    int deltaE = 2 * dy, deltaNE = 2 * (dy - dx);

    while (x <= to[0]) {
        float ratio;
        if (swap_xy)
            fragment.coordinates.set(y, x);
        else
            fragment.coordinates.set(x, y);

        if (to[0] != from[0])
            ratio = (float)(x - from[0]) / (float)(to[0] - from[0]);
        else
            ratio = (float)(y - from[1]) / (float)(to[1] - from[1]);
        if (dy > dx)
            ratio = 1.f - ratio;
        if (swapped_again)
            ratio = 1.f - ratio;
        fragment.set(vertex0, vertex1, ratio);
        m_cgPushFragment(fragment);

        if (d < 0)
            d += deltaE;
        else {
            d += deltaNE;
            y += ystep;
        }
        ++x;
    }
}

void CGContext::m_cgRasterizeLineBresenham(int pipelineVertexOffset)
{
    // This primitive produces many fragments from 2 vertices:
    CGVertexVaryings &vertex0 = m_pipelineVertexVaryings[pipelineVertexOffset + 0];
    CGVertexVaryings &vertex1 = m_pipelineVertexVaryings[pipelineVertexOffset + 1];
    _bresenham_helper(vertex0, vertex1);
}
//---------------------------------------------------------------------------
void CGContext::m_cgRasterizeWireTriangle(int pipelineVertexOffset)
{
    float jimmy_neutron;

    // This primitive produces many fragments from 3 vertices:
    CGVertexVaryings &vertex0 = m_pipelineVertexVaryings[pipelineVertexOffset + 0];
    CGVertexVaryings &vertex1 = m_pipelineVertexVaryings[pipelineVertexOffset + 1];
    CGVertexVaryings &vertex2 = m_pipelineVertexVaryings[pipelineVertexOffset + 2];

    if (!m_cgTriangleSetup(vertex0.varyings[CG_POSITION_VARYING],
        vertex1.varyings[CG_POSITION_VARYING], vertex2.varyings[CG_POSITION_VARYING],
        jimmy_neutron))
    {
        return;
    }

    _bresenham_helper(vertex0, vertex1);
    _bresenham_helper(vertex1, vertex2);
    _bresenham_helper(vertex2, vertex0);
}
//---------------------------------------------------------------------------

void CGContext::m_cgRasterizeTriangle(int pipelineVertexOffset)
{
    CGVertexVaryings &vertex0 = m_pipelineVertexVaryings[pipelineVertexOffset + 0];
    CGVertexVaryings &vertex1 = m_pipelineVertexVaryings[pipelineVertexOffset + 1];
    CGVertexVaryings &vertex2 = m_pipelineVertexVaryings[pipelineVertexOffset + 2];

    float total_area;
    if (!m_cgTriangleSetup(vertex0.varyings[CG_POSITION_VARYING],
        vertex1.varyings[CG_POSITION_VARYING], vertex2.varyings[CG_POSITION_VARYING],
        total_area))
    {
        return;
    }

    float Ax = vertex0.varyings[CG_POSITION_VARYING][0];
    float Ay = vertex0.varyings[CG_POSITION_VARYING][1];
    float Bx = vertex1.varyings[CG_POSITION_VARYING][0];
    float By = vertex1.varyings[CG_POSITION_VARYING][1];
    float Cx = vertex2.varyings[CG_POSITION_VARYING][0];
    float Cy = vertex2.varyings[CG_POSITION_VARYING][1];

    CGFragmentData fragment;

    /* clip triangle boundaries */
    int left, right, top, bottom;
    left = (int)(Ax < Bx ? (Ax < Cx ? Ax : Cx) : (Bx < Cx ? Bx : Cx));
    top = (int)(Ay < By ? (Ay < Cy ? Ay : Cy) : (By < Cy ? By : Cy));
    right = (int)(Ax > Bx ? (Ax > Cx ? Ax : Cx) : (Bx > Cx ? Bx : Cx));
    bottom = (int)(Ay > By ? (Ay > Cy ? Ay : Cy) : (By > Cy ? By : Cy));

    /* clip framebuffer */
    int fbxmax = m_frameBuffer.getWidth()-1;
    int fbymax = m_frameBuffer.getHeight()-1;
    left = left < 0 ? 0 : (left < fbxmax ? left : fbxmax);
    top = top < 0 ? 0 : (top < fbymax ? top : fbymax);
    right = right < 0 ? 0 : (right < fbxmax ? right : fbxmax);
    bottom = bottom < 0 ? 0 : (bottom < fbymax ? bottom : fbymax);

    for (int x = left; x <= right; ++x) {
        for (int y = top; y <= bottom; ++y) {
            float Px = .5f + (float)x;
            float Py = .5f + (float)y;

            float alpha = _det_points(Bx, By, Cx, Cy, Px, Py);
            if (alpha < 0.f)
                continue;

            float beta = _det_points(Cx, Cy, Ax, Ay, Px, Py);
            if (beta < 0.f)
                continue;

            float gamma = _det_points(Ax, Ay, Bx, By, Px, Py);
            if (gamma < 0.f)
                continue;

            fragment.coordinates.set(x, y);
            fragment.set(vertex0, vertex1, vertex2,
                alpha / total_area, beta / total_area, gamma / total_area);
            m_cgPushFragment(fragment);
        }
    }
}
//---------------------------------------------------------------------------
