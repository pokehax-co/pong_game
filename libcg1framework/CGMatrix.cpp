#include "CGMatrix.h"
#define _USE_MATH_DEFINES
#include <math.h>
#include <string.h>
#include <stdio.h>

// The code is given in UE8

//---------------------------------------------------------------------------
// CGMATRIX4X4 : Static (factory) functions.
//---------------------------------------------------------------------------
CGMatrix4x4 CGMatrix4x4::getIdentityMatrix()
{
    CGMatrix4x4 m;
    return m;
}
//---------------------------------------------------------------------------
CGMatrix4x4 CGMatrix4x4::getTranslationMatrix(float x, float y, float z)
{
    CGMatrix4x4 m;
    m.m_matrix[3][0] = x; m.m_matrix[3][1] = y; m.m_matrix[3][2] = z;
    return m;
}
//---------------------------------------------------------------------------
CGMatrix4x4 CGMatrix4x4::getRotationMatrixX(float angle)
{
    return CGMatrix4x4::getRotationMatrix(angle, 1.0f, 0.0f, 0.0f);
}
//---------------------------------------------------------------------------
CGMatrix4x4 CGMatrix4x4::getRotationMatrixY(float angle)
{
    return CGMatrix4x4::getRotationMatrix(angle, 0.0f, 1.0f, 0.0f);
}
//---------------------------------------------------------------------------
CGMatrix4x4 CGMatrix4x4::getRotationMatrixZ(float angle)
{
    return CGMatrix4x4::getRotationMatrix(angle, 0.0f, 0.0f, 1.0f);
}
//---------------------------------------------------------------------------
CGMatrix4x4 CGMatrix4x4::getScaleMatrix(float x, float y, float z)
{
    CGMatrix4x4 m;
    m.m_matrix[0][0] = x; m.m_matrix[1][1] = y; m.m_matrix[2][2] = z;
    return m;
}
//---------------------------------------------------------------------------
CGMatrix4x4 CGMatrix4x4::getRotationMatrix(float angle, float x, float y, float z)
{
    const float c = (float)cosf(angle * (float)M_PI / 180.0f);
    const float one_minus_c = 1.0f - c;
    const float s = (float)sinf(angle * (float)M_PI / 180.0f);
    const float v_len = (float)sqrtf(x*x + y*y + z*z);
    float x_one_minus_c;
    float y_one_minus_c;
    float z_one_minus_c;
    CGMatrix4x4 rot;
    if (v_len == 0.0f)
        return rot;
    if (v_len != 1.0f) {
        x /= v_len; y /= v_len; z /= v_len;
    }
    x_one_minus_c = x * one_minus_c;
    y_one_minus_c = y * one_minus_c;
    z_one_minus_c = z * one_minus_c;
    rot.m_matrix[0][0] = x * x_one_minus_c + c;
    rot.m_matrix[0][1] = x * y_one_minus_c + z * s;
    rot.m_matrix[0][2] = x * z_one_minus_c - y * s;
    rot.m_matrix[1][0] = y * x_one_minus_c - z * s;
    rot.m_matrix[1][1] = y * y_one_minus_c + c;
    rot.m_matrix[1][2] = y * z_one_minus_c + x * s;
    rot.m_matrix[2][0] = z * x_one_minus_c + y * s;
    rot.m_matrix[2][1] = z * y_one_minus_c - x * s;
    rot.m_matrix[2][2] = z * z_one_minus_c + c;
    return rot;
}
//---------------------------------------------------------------------------
CGMatrix4x4 CGMatrix4x4::getFrustum(float left, float right, float bottom, float top, float nearVal, float farVal)
{
    CGMatrix4x4 f;
    // ...
    f.m_matrix[0][0] = (2.0f * nearVal) / (right - left);
    f.m_matrix[2][0] = (left + right) / (right - left);
    f.m_matrix[1][1] = (2.0f * nearVal) / (top - bottom);
    f.m_matrix[2][1] = (top + bottom) / (top - bottom);
    f.m_matrix[2][2] = -(farVal + nearVal) / (farVal - nearVal);
    f.m_matrix[3][2] = -(2.0f * farVal * nearVal) / (farVal - nearVal);
    f.m_matrix[2][3] = -1.0f;
    f.m_matrix[3][3] = 0.0f;

    return f;
}

CGMatrix4x4 CGMatrix4x4::getOrtho(float left, float right, float bottom,
    float top, float n, float f)
{
    CGMatrix4x4 out;

    out.m_matrix[0][0] = 2.f/(right - left);
    out.m_matrix[3][0] = -(right + left)/(right - left); // tx
    out.m_matrix[1][1] = 2.f/(top - bottom);
    out.m_matrix[3][1] = -(top + bottom)/(top - bottom); // ty
    out.m_matrix[2][2] = -2.f/(f - n);
    out.m_matrix[3][2] = -(f + n)/(f - n);
    out.m_matrix[3][3] = 1.f;
    return out;
}
//---------------------------------------------------------------------------
// CGMATRIX4X4 : Non-static operants.
//---------------------------------------------------------------------------
CGMatrix4x4::CGMatrix4x4()
{
    identity();
}
//---------------------------------------------------------------------------
void CGMatrix4x4::getFloatsToColMajor(float *floats) const
{
    memcpy(floats, &m_matrix[0][0], sizeof(float)* 16);
}
//---------------------------------------------------------------------------
void CGMatrix4x4::setFloatsFromColMajor(const float *floats)
{
    memcpy(&m_matrix[0][0], floats, sizeof(float)* 16);
}
//---------------------------------------------------------------------------
void CGMatrix4x4::getFloatsToRowMajor(float *floats) const
{
    CGMatrix4x4 m = *this;
    m.transpose();
    m.getFloatsToColMajor(floats);
}
//---------------------------------------------------------------------------
void CGMatrix4x4::setFloatsFromRowMajor(const float *floats)
{
    setFloatsFromColMajor(floats);
    transpose();
}
//---------------------------------------------------------------------------
void CGMatrix4x4::identity()
{
    m_matrix[0][0] = 1.0f; m_matrix[1][0] = 0.0f; m_matrix[2][0] = 0.0f; m_matrix[3][0] = 0.0f;
    m_matrix[0][1] = 0.0f; m_matrix[1][1] = 1.0f; m_matrix[2][1] = 0.0f; m_matrix[3][1] = 0.0f;
    m_matrix[0][2] = 0.0f; m_matrix[1][2] = 0.0f; m_matrix[2][2] = 1.0f; m_matrix[3][2] = 0.0f;
    m_matrix[0][3] = 0.0f; m_matrix[1][3] = 0.0f; m_matrix[2][3] = 0.0f; m_matrix[3][3] = 1.0f;
}
//---------------------------------------------------------------------------
CGMatrix4x4 CGMatrix4x4::operator*(const CGMatrix4x4& b) const
{
    #define M(x,y) m_matrix[x][y]
    CGMatrix4x4 m;
    m.M(0,0) = M(0,0) * b.M(0,0) + M(1,0) * b.M(0,1) + M(2,0) * b.M(0,2) + M(3,0) * b.M(0,3);
    m.M(0,1) = M(0,1) * b.M(0,0) + M(1,1) * b.M(0,1) + M(2,1) * b.M(0,2) + M(3,1) * b.M(0,3);
    m.M(0,2) = M(0,2) * b.M(0,0) + M(1,2) * b.M(0,1) + M(2,2) * b.M(0,2) + M(3,2) * b.M(0,3);
    m.M(0,3) = M(0,3) * b.M(0,0) + M(1,3) * b.M(0,1) + M(2,3) * b.M(0,2) + M(3,3) * b.M(0,3);
    m.M(1,0) = M(0,0) * b.M(1,0) + M(1,0) * b.M(1,1) + M(2,0) * b.M(1,2) + M(3,0) * b.M(1,3);
    m.M(1,1) = M(0,1) * b.M(1,0) + M(1,1) * b.M(1,1) + M(2,1) * b.M(1,2) + M(3,1) * b.M(1,3);
    m.M(1,2) = M(0,2) * b.M(1,0) + M(1,2) * b.M(1,1) + M(2,2) * b.M(1,2) + M(3,2) * b.M(1,3);
    m.M(1,3) = M(0,3) * b.M(1,0) + M(1,3) * b.M(1,1) + M(2,3) * b.M(1,2) + M(3,3) * b.M(1,3);
    m.M(2,0) = M(0,0) * b.M(2,0) + M(1,0) * b.M(2,1) + M(2,0) * b.M(2,2) + M(3,0) * b.M(2,3);
    m.M(2,1) = M(0,1) * b.M(2,0) + M(1,1) * b.M(2,1) + M(2,1) * b.M(2,2) + M(3,1) * b.M(2,3);
    m.M(2,2) = M(0,2) * b.M(2,0) + M(1,2) * b.M(2,1) + M(2,2) * b.M(2,2) + M(3,2) * b.M(2,3);
    m.M(2,3) = M(0,3) * b.M(2,0) + M(1,3) * b.M(2,1) + M(2,3) * b.M(2,2) + M(3,3) * b.M(2,3);
    m.M(3,0) = M(0,0) * b.M(3,0) + M(1,0) * b.M(3,1) + M(2,0) * b.M(3,2) + M(3,0) * b.M(3,3);
    m.M(3,1) = M(0,1) * b.M(3,0) + M(1,1) * b.M(3,1) + M(2,1) * b.M(3,2) + M(3,1) * b.M(3,3);
    m.M(3,2) = M(0,2) * b.M(3,0) + M(1,2) * b.M(3,1) + M(2,2) * b.M(3,2) + M(3,2) * b.M(3,3);
    m.M(3,3) = M(0,3) * b.M(3,0) + M(1,3) * b.M(3,1) + M(2,3) * b.M(3,2) + M(3,3) * b.M(3,3);
    #undef M
    return m;
}
//---------------------------------------------------------------------------
void CGMatrix4x4::debugPrint(const char *prefix) const
{
    if (prefix == NULL)
        prefix = "";
    printf("%s[%f %f %f %f]\n", prefix, m_matrix[0][0], m_matrix[1][0], m_matrix[2][0], m_matrix[3][0]);
    printf("%s[%f %f %f %f]\n", prefix, m_matrix[0][1], m_matrix[1][1], m_matrix[2][1], m_matrix[3][1]);
    printf("%s[%f %f %f %f]\n", prefix, m_matrix[0][2], m_matrix[1][2], m_matrix[2][2], m_matrix[3][2]);
    printf("%s[%f %f %f %f]\n", prefix, m_matrix[0][3], m_matrix[1][3], m_matrix[2][3], m_matrix[3][3]);
}
//---------------------------------------------------------------------------
CGVec4 CGMatrix4x4::operator*(const CGVec4& b) const
{
    CGVec4 c;
    c[0] = m_matrix[0][0] * b[0] + m_matrix[1][0] * b[1] + m_matrix[2][0] * b[2] + m_matrix[3][0] * b[3];
    c[1] = m_matrix[0][1] * b[0] + m_matrix[1][1] * b[1] + m_matrix[2][1] * b[2] + m_matrix[3][1] * b[3];
    c[2] = m_matrix[0][2] * b[0] + m_matrix[1][2] * b[1] + m_matrix[2][2] * b[2] + m_matrix[3][2] * b[3];
    c[3] = m_matrix[0][3] * b[0] + m_matrix[1][3] * b[1] + m_matrix[2][3] * b[2] + m_matrix[3][3] * b[3];
    return c;
}
//---------------------------------------------------------------------------
void CGMatrix4x4::transpose()
{
    int i, j;
    for (i = 0; i<4; i++) {
        for (j = 0; j<i; j++) {
            float tmp;
            tmp = m_matrix[i][j];
            m_matrix[i][j] = m_matrix[j][i];
            m_matrix[j][i] = tmp;
        }
    }
}
//---------------------------------------------------------------------------
void CGMatrix4x4::invert()
{
#define m(a,b) m_matrix[b][a]
#define det3x3(a1, a2, a3, b1, b2, b3, c1, c2, c3) (a1 * (b2 * c3 - b3 * c2) + b1 * (c2 * a3 - a2 * c3) + c1 * (a2 * b3 - a3 * b2))
    const float cof00 = det3x3(m(1, 1), m(1, 2), m(1, 3), m(2, 1), m(2, 2), m(2, 3), m(3, 1), m(3, 2), m(3, 3));
    const float cof01 = -det3x3(m(1, 2), m(1, 3), m(1, 0), m(2, 2), m(2, 3), m(2, 0), m(3, 2), m(3, 3), m(3, 0));
    const float cof02 = det3x3(m(1, 3), m(1, 0), m(1, 1), m(2, 3), m(2, 0), m(2, 1), m(3, 3), m(3, 0), m(3, 1));
    const float cof03 = -det3x3(m(1, 0), m(1, 1), m(1, 2), m(2, 0), m(2, 1), m(2, 2), m(3, 0), m(3, 1), m(3, 2));
    const float inv_det = 1.0f / (m(0, 0) * cof00 + m(0, 1) * cof01 + m(0, 2) * cof02 + m(0, 3) * cof03);
    const float cof10 = -det3x3(m(2, 1), m(2, 2), m(2, 3), m(3, 1), m(3, 2), m(3, 3), m(0, 1), m(0, 2), m(0, 3));
    const float cof11 = det3x3(m(2, 2), m(2, 3), m(2, 0), m(3, 2), m(3, 3), m(3, 0), m(0, 2), m(0, 3), m(0, 0));
    const float cof12 = -det3x3(m(2, 3), m(2, 0), m(2, 1), m(3, 3), m(3, 0), m(3, 1), m(0, 3), m(0, 0), m(0, 1));
    const float cof13 = det3x3(m(2, 0), m(2, 1), m(2, 2), m(3, 0), m(3, 1), m(3, 2), m(0, 0), m(0, 1), m(0, 2));
    const float cof20 = det3x3(m(3, 1), m(3, 2), m(3, 3), m(0, 1), m(0, 2), m(0, 3), m(1, 1), m(1, 2), m(1, 3));
    const float cof21 = -det3x3(m(3, 2), m(3, 3), m(3, 0), m(0, 2), m(0, 3), m(0, 0), m(1, 2), m(1, 3), m(1, 0));
    const float cof22 = det3x3(m(3, 3), m(3, 0), m(3, 1), m(0, 3), m(0, 0), m(0, 1), m(1, 3), m(1, 0), m(1, 1));
    const float cof23 = -det3x3(m(3, 0), m(3, 1), m(3, 2), m(0, 0), m(0, 1), m(0, 2), m(1, 0), m(1, 1), m(1, 2));
    const float cof30 = -det3x3(m(0, 1), m(0, 2), m(0, 3), m(1, 1), m(1, 2), m(1, 3), m(2, 1), m(2, 2), m(2, 3));
    const float cof31 = det3x3(m(0, 2), m(0, 3), m(0, 0), m(1, 2), m(1, 3), m(1, 0), m(2, 2), m(2, 3), m(2, 0));
    const float cof32 = -det3x3(m(0, 3), m(0, 0), m(0, 1), m(1, 3), m(1, 0), m(1, 1), m(2, 3), m(2, 0), m(2, 1));
    const float cof33 = det3x3(m(0, 0), m(0, 1), m(0, 2), m(1, 0), m(1, 1), m(1, 2), m(2, 0), m(2, 1), m(2, 2));
#undef det3x3
#undef m
    m_matrix[0][0] = cof00 * inv_det; m_matrix[1][0] = cof10 * inv_det; m_matrix[2][0] = cof20 * inv_det; m_matrix[3][0] = cof30 * inv_det;
    m_matrix[0][1] = cof01 * inv_det; m_matrix[1][1] = cof11 * inv_det; m_matrix[2][1] = cof21 * inv_det; m_matrix[3][1] = cof31 * inv_det;
    m_matrix[0][2] = cof02 * inv_det; m_matrix[1][2] = cof12 * inv_det; m_matrix[2][2] = cof22 * inv_det; m_matrix[3][2] = cof32 * inv_det;
    m_matrix[0][3] = cof03 * inv_det; m_matrix[1][3] = cof13 * inv_det; m_matrix[2][3] = cof23 * inv_det; m_matrix[3][3] = cof33 * inv_det;
}
//---------------------------------------------------------------------------
void CGMatrix4x4::frustum(float left, float right, float bottom, float top, float nearVal, float farVal)
{
    *this=*this * CGMatrix4x4::getFrustum(left,right,bottom,top,nearVal,farVal);
}
void CGMatrix4x4::ortho(float left, float right, float bottom, float top, float nearVal, float farVal)
{
    *this = *this * CGMatrix4x4::getOrtho(left,right,bottom,top,nearVal,farVal);
}
