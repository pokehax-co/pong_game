#ifndef CG_PROGRAMINTERFACE_H
#define CG_PROGRAMINTERFACE_H

#include "CG.h"
#include "CGMatrix.h"
#include "CGTexture.h"

//---------------------------------------------------------------------------

/// Locations to access the builtin Uniforms
enum CGUniformLocations
{
    CG_ULOC_PROJECTION_MATRIX=0,
    CG_ULOC_MODELVIEW_MATRIX,
    CG_ULOC_NORMAL_MATRIX,
    CG_ULOC_MATERIAL_AMBIENT,
    CG_ULOC_MATERIAL_DIFFUSE,
    CG_ULOC_MATERIAL_SPECULAR,
    CG_ULOC_MATERIAL_SHININESS,
    CG_ULOC_MATERIAL_EMISSION,
    CG_ULOC_LIGHT0_AMBIENT,
    CG_ULOC_LIGHT0_DIFFUSE,
    CG_ULOC_LIGHT0_SPECULAR,
    CG_ULOC_LIGHT0_POSITION,
    CG_ULOC_SAMPLER
};

#define CG_ATTRIBUTE_COUNT 4

/// Access offsets into attribute data arrays.
enum CGAttributes
{
    CG_POSITION_ATTRIBUTE=0,
    CG_NORMAL_ATTRIBUTE,
    CG_COLOR_ATTRIBUTE,
    CG_TEXCOORD_ATTRIBUTE
};

#define CG_VARYING_COUNT 6

/// Access offsets into varying data arrays.
enum CGVaryings
{
    CG_POSITION_VARYING=0,
    CG_NORMAL_VARYING,
    CG_COLOR_VARYING,
    CG_TEXCOORD_VARYING,
    CG_POSITION_EYESPACE_VARYING,
    CG_LIGHT_COLOR_VARYING
};

/// Vertex attribute container (vertex processing inputs).
struct CGVertexAttributes
{
    CGVec4 attributes[CG_ATTRIBUTE_COUNT];  // Attributes are what is pulled from the attribute pointer.
};

/// Vertex varying container (vertex processing outputs).
struct CGVertexVaryings
{
    CGVec4 varyings[CG_VARYING_COUNT];      // Varyings are vertex attributes (and more) after vertex program.
};

/// Fragment data container.
struct CGFragmentData
{
    CGVec2i coordinates;                  // Fragment specific framebuffer coordinate.
    CGVec4  varyings[CG_VARYING_COUNT];   // Varyings as copied from vertex or interpolated.
    void set(const CGVertexVaryings& vertex)
    {
        for (int i=0; i<CG_VARYING_COUNT; i++)
            varyings[i].set(vertex.varyings[i]);
    }
    void set(const CGVertexVaryings& A, const CGVertexVaryings& B, float ratio)
    {
        if (ratio < 0.f || ratio > 1.f)
            return;

        // Speed up special cases: ratio ~ 0, ratio ~ 1
        if (ratio < 0.001f) {
            varyings[CG_POSITION_VARYING] = A.varyings[CG_POSITION_VARYING];
            varyings[CG_NORMAL_VARYING] = A.varyings[CG_NORMAL_VARYING];
            varyings[CG_COLOR_VARYING] = A.varyings[CG_COLOR_VARYING];
            varyings[CG_TEXCOORD_VARYING] = A.varyings[CG_TEXCOORD_VARYING];
            varyings[CG_POSITION_EYESPACE_VARYING] = A.varyings[CG_POSITION_EYESPACE_VARYING];
            varyings[CG_LIGHT_COLOR_VARYING] = A.varyings[CG_LIGHT_COLOR_VARYING];
        }
        else if (ratio > 0.999f) {
            varyings[CG_POSITION_VARYING] = B.varyings[CG_POSITION_VARYING];
            varyings[CG_NORMAL_VARYING] = B.varyings[CG_NORMAL_VARYING];
            varyings[CG_COLOR_VARYING] = B.varyings[CG_COLOR_VARYING];
            varyings[CG_TEXCOORD_VARYING] = B.varyings[CG_TEXCOORD_VARYING];
            varyings[CG_POSITION_EYESPACE_VARYING] = B.varyings[CG_POSITION_EYESPACE_VARYING];
            varyings[CG_LIGHT_COLOR_VARYING] = B.varyings[CG_LIGHT_COLOR_VARYING];
        }
        else {
            float wA = (1.f - ratio) * A.varyings[CG_POSITION_VARYING][3];
            float wB = ratio * B.varyings[CG_POSITION_VARYING][3];
            float div = 1.f/(wA + wB);

            // vertices: 4 used
            varyings[CG_POSITION_VARYING] =
                (1.f - ratio) * A.varyings[CG_POSITION_VARYING] + ratio * B.varyings[CG_POSITION_VARYING];

            // normals: 3 used (but 4th will come!)
            varyings[CG_NORMAL_VARYING] =
                (1.f - ratio) * A.varyings[CG_NORMAL_VARYING] + ratio * B.varyings[CG_NORMAL_VARYING];

            // colors: 4 used
            varyings[CG_COLOR_VARYING] =
                (1.f - ratio) * A.varyings[CG_COLOR_VARYING] + ratio * B.varyings[CG_COLOR_VARYING];

            // tex-coord: 2 used
            varyings[CG_TEXCOORD_VARYING][0] =
                (wA * A.varyings[CG_TEXCOORD_VARYING][0] + wB * B.varyings[CG_TEXCOORD_VARYING][0]) * div;
            varyings[CG_TEXCOORD_VARYING][1] =
                (wA * A.varyings[CG_TEXCOORD_VARYING][1] + wB * B.varyings[CG_TEXCOORD_VARYING][1]) * div;

            // eyespace-position: ? used
            varyings[CG_POSITION_EYESPACE_VARYING] =
                (1.f - ratio) * A.varyings[CG_POSITION_EYESPACE_VARYING] + ratio * B.varyings[CG_POSITION_EYESPACE_VARYING];

            // light colors: 4 used
            varyings[CG_LIGHT_COLOR_VARYING] =
                (1.f - ratio) * A.varyings[CG_LIGHT_COLOR_VARYING] + ratio * B.varyings[CG_LIGHT_COLOR_VARYING];
        }
    }
    void set(const CGVertexVaryings& A, const CGVertexVaryings& B, const CGVertexVaryings& C, float a, float b, float c)
    {
        float wA = a * A.varyings[CG_POSITION_VARYING][3];
        float wB = b * B.varyings[CG_POSITION_VARYING][3];
        float wC = c * C.varyings[CG_POSITION_VARYING][3];
        float div = 1.f/(wA + wB + wC);

        // vertices: 4 used
        varyings[CG_POSITION_VARYING] = a * A.varyings[CG_POSITION_VARYING] +
            b * B.varyings[CG_POSITION_VARYING] + c * C.varyings[CG_POSITION_VARYING];

        // normals: 3 used (but 4th will come!)
        varyings[CG_NORMAL_VARYING] = a * A.varyings[CG_NORMAL_VARYING] +
            b * B.varyings[CG_NORMAL_VARYING] + c * C.varyings[CG_NORMAL_VARYING];

        // colors: 4 used
        varyings[CG_COLOR_VARYING] = a * A.varyings[CG_COLOR_VARYING] +
            b * B.varyings[CG_COLOR_VARYING] + c * C.varyings[CG_COLOR_VARYING];

        // tex-coord: 2 used
        varyings[CG_TEXCOORD_VARYING][0] = div *
            (wA * A.varyings[CG_TEXCOORD_VARYING][0] +
             wB * B.varyings[CG_TEXCOORD_VARYING][0] +
             wC * C.varyings[CG_TEXCOORD_VARYING][0]);
        varyings[CG_TEXCOORD_VARYING][1] = div *
            (wA * A.varyings[CG_TEXCOORD_VARYING][1] +
             wB * B.varyings[CG_TEXCOORD_VARYING][1] +
             wC * C.varyings[CG_TEXCOORD_VARYING][1]);

        // eyespace-position: ? used
        varyings[CG_POSITION_EYESPACE_VARYING] = a * A.varyings[CG_POSITION_EYESPACE_VARYING] +
            b * B.varyings[CG_POSITION_EYESPACE_VARYING] + c * C.varyings[CG_POSITION_EYESPACE_VARYING];

        // light colors: 4 used
        varyings[CG_LIGHT_COLOR_VARYING] = a * A.varyings[CG_LIGHT_COLOR_VARYING] +
            b * B.varyings[CG_LIGHT_COLOR_VARYING] + c * C.varyings[CG_LIGHT_COLOR_VARYING];
    }
};

/// Texture sampler.
struct CGSampler2D
{
    int unit;
    const CGTexture2D *texture;

    /* somehow, this struct is uninitialized */
    CGSampler2D(void) : unit(-1), texture(NULL) {}
};

/// Uniform variables.
struct CGUniformData
{
    CGMatrix4x4 projectionMatrix, modelviewMatrix, normalMatrix;
    CGVec4 materialAmbient, materialDiffuse, materialSpecular, materialEmission;
    float materialShininess;
    CGVec4 light0Ambient, light0Diffuse, light0Specular, light0Position;
    CGSampler2D sampler;
};

/// Shader functions
typedef void (*CGVertexProgram)(const CGVertexAttributes&, CGVertexVaryings&, const CGUniformData&);
typedef void (*CGFragmentProgram)(const CGFragmentData&, CGVec4&, const CGUniformData&);

//---------------------------------------------------------------------------

#endif
