#include "CGFrameBuffer.h"
#include <stdlib.h> // for malloc
#include <string.h> // for memcpy

//---------------------------------------------------------------------------
// CGFRAMEBUFFER
//---------------------------------------------------------------------------
bool CGFrameBuffer::allocate(int width, int height)
{
    if (!colorBuffer.reserve(width, height))
        return false;
    if (!depthBuffer.reserve(width, height))
        return false;
    return true;
}
//---------------------------------------------------------------------------
int CGFrameBuffer::getWidth() const
{
    return colorBuffer.getWidth();
}
//---------------------------------------------------------------------------
int CGFrameBuffer::getHeight() const
{
    return colorBuffer.getHeight();
}
//---------------------------------------------------------------------------
// CGFRAMEBUFFER::CGBUFFER4UB
//---------------------------------------------------------------------------
CGFrameBuffer::CGBuffer4UB::CGBuffer4UB()
{
    pBufferData = NULL;
}
//---------------------------------------------------------------------------
CGFrameBuffer::CGBuffer4UB::~CGBuffer4UB()
{
    free(pBufferData);
}
//---------------------------------------------------------------------------
bool CGFrameBuffer::CGBuffer4UB::reserve(int width, int height)
{
    this->width = width;
    this->height = height;

    // FIXME no overflow check
    pBufferData = (unsigned char*)malloc(width * height * 4);
    if (!pBufferData)
        return false;

    return true;
}
//---------------------------------------------------------------------------
void CGFrameBuffer::CGBuffer4UB::get(int x, int y, float *_rgba) const
{
    unsigned int *p = (unsigned int*)pBufferData;
    unsigned int rgba = p[y * this->width + x];
    _rgba[3] = (float)(rgba & 0xff) / 255.f; rgba >>= 8;
    _rgba[2] = (float)(rgba & 0xff) / 255.f; rgba >>= 8;
    _rgba[1] = (float)(rgba & 0xff) / 255.f; rgba >>= 8;
    _rgba[0] = (float)rgba / 255.f;
}
//---------------------------------------------------------------------------
void CGFrameBuffer::CGBuffer4UB::set(int x, int y, const float *_rgba)
{
    unsigned int *p = (unsigned int*)pBufferData;
    unsigned int rgba = (unsigned char)(_rgba[3] * 255.f);
    rgba = (rgba << 8) | ((unsigned char)(_rgba[2] * 255.f));
    rgba = (rgba << 8) | ((unsigned char)(_rgba[1] * 255.f));
    rgba = (rgba << 8) | ((unsigned char)(_rgba[0] * 255.f));
    p[y * this->width + x] = rgba;
}
//---------------------------------------------------------------------------
void CGFrameBuffer::CGBuffer4UB::clear(const float *_rgba)
{
    unsigned int rgba, count = this->width * this->height;
    unsigned int *p = (unsigned int*)pBufferData;

    rgba = (unsigned char)(_rgba[3] * 255.f);
    rgba = (rgba << 8) | ((unsigned char)(_rgba[2] * 255.f));
    rgba = (rgba << 8) | ((unsigned char)(_rgba[1] * 255.f));
    rgba = (rgba << 8) | ((unsigned char)(_rgba[0] * 255.f));

    while (count >= 16) {
        *p++ = rgba; *p++ = rgba; *p++ = rgba; *p++ = rgba;
        *p++ = rgba; *p++ = rgba; *p++ = rgba; *p++ = rgba;
        *p++ = rgba; *p++ = rgba; *p++ = rgba; *p++ = rgba;
        *p++ = rgba; *p++ = rgba; *p++ = rgba; *p++ = rgba;
        count -= 16;
    }
    while (count--)
        *p++ = rgba;
}
//---------------------------------------------------------------------------
unsigned char* CGFrameBuffer::CGBuffer4UB::getDataPointer() const
{
    return pBufferData;
}
//---------------------------------------------------------------------------
int CGFrameBuffer::CGBuffer4UB::getWidth() const
{
    return width;
}
//---------------------------------------------------------------------------
int CGFrameBuffer::CGBuffer4UB::getHeight() const
{
    return height;
}
//---------------------------------------------------------------------------
// CGFRAMEBUFFER::CGBUFFER1F
//---------------------------------------------------------------------------
CGFrameBuffer::CGBuffer1f::CGBuffer1f()
{
    pBufferData = NULL;
}
//---------------------------------------------------------------------------
CGFrameBuffer::CGBuffer1f::~CGBuffer1f()
{
    free(pBufferData);
}
//---------------------------------------------------------------------------
bool CGFrameBuffer::CGBuffer1f::reserve(int width, int height)
{
    this->width = width;
    this->height = height;

    // FIXME no overflow check
    pBufferData = (float*)malloc(width * height * sizeof(float));
    if (!pBufferData)
        return false;
    return true;
}
//---------------------------------------------------------------------------
float CGFrameBuffer::CGBuffer1f::get(int x, int y) const
{
    return pBufferData[y * this->width + x];
}
//---------------------------------------------------------------------------
void CGFrameBuffer::CGBuffer1f::set(int x, int y, const float z)
{
    pBufferData[y * this->width + x] = z;
}
//---------------------------------------------------------------------------
void CGFrameBuffer::CGBuffer1f::clear()
{
    union {
        float f;
        unsigned int u;
    } x;
    x.f = 1.f;

    unsigned int count = this->width * this->height;
    unsigned int *p = (unsigned int*)pBufferData;

    while (count >= 16) {
        *p++ = x.u; *p++ = x.u; *p++ = x.u; *p++ = x.u;
        *p++ = x.u; *p++ = x.u; *p++ = x.u; *p++ = x.u;
        *p++ = x.u; *p++ = x.u; *p++ = x.u; *p++ = x.u;
        *p++ = x.u; *p++ = x.u; *p++ = x.u; *p++ = x.u;
        count -= 16;
    }
    while (count--)
        *p++ = x.u;
}
//---------------------------------------------------------------------------
float* CGFrameBuffer::CGBuffer1f::getDataPointer() const
{
    return pBufferData;
}
//---------------------------------------------------------------------------
int CGFrameBuffer::CGBuffer1f::getWidth() const
{
    return this->width;
}
//---------------------------------------------------------------------------
int CGFrameBuffer::CGBuffer1f::getHeight() const
{
    return this->height;
}
//---------------------------------------------------------------------------
