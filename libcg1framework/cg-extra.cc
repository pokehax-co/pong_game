#include "cg-extra.h"
#include "CGMath.h"
#include <math.h>

#define _CG_EXTRA_SPECULAR_NOT_AFFECTED_BY_TEXTURE

static inline float max_float(float a, float b)
{
    return a > b ? a : b;
}

CGMatrix4x4 cguLookAt(float eyeX, float eyeY, float eyeZ,
    float centerX, float centerY, float centerZ,
    float upX, float upY, float upZ)
{
    float f[3];
    f[0] = centerX - eyeX;
    f[1] = centerY - eyeY;
    f[2] = centerZ - eyeZ;
    float f_norm = sqrtf(f[0] * f[0] + f[1] * f[1] + f[2] * f[2]);
    f[0] /= f_norm;
    f[1] /= f_norm;
    f[2] /= f_norm;

    float s[3];
    s[0] = f[1] * upZ - upY * f[2];
    s[1] = upX * f[2] - f[0] * upZ;
    s[2] = f[0] * upY - upX * f[1];
    float s_norm = sqrtf(s[0] * s[0] + s[1] * s[1] + s[2] * s[2]);
    s[0] /= s_norm;
    s[1] /= s_norm;
    s[2] /= s_norm;

    float u[3];
    u[0] = s[1] * f[2] - f[1] * s[2];
    u[1] = f[0] * s[2] - s[0] * f[2];
    u[2] = s[0] * f[1] - f[0] * s[1];

    float mtx[16], *mtxp = mtx;
    *mtxp++ = s[0];
    *mtxp++ = u[0];
    *mtxp++ = -f[0];
    *mtxp++ = 0.f;
    *mtxp++ = s[1];
    *mtxp++ = u[1];
    *mtxp++ = -f[1];
    *mtxp++ = 0.f;
    *mtxp++ = s[2];
    *mtxp++ = u[2];
    *mtxp++ = -f[2];
    *mtxp++ = 0.f;
    *mtxp++ = 0.f;
    *mtxp++ = 0.f;
    *mtxp++ = 0.f;
    *mtxp++ = 1.f;

    CGMatrix4x4 V;
    V.setFloatsFromColMajor(mtx);
    V = V * CGMatrix4x4::getTranslationMatrix(-eyeX, -eyeY, -eyeZ);
    return V;
}

CGMatrix4x4 cguPerspective(float fov_y_, float aspect, float zNear, float zFar)
{
    float fov_y = fov_y_ * 3.14159265358979323846f / 180.f;
    float Y = zNear * tanf(fov_y / 2.f);
    float X = Y * aspect;
    return CGMatrix4x4::getFrustum(-X, X, -Y, Y, zNear, zFar);
}

CGMatrix4x4 cguOrtho(float left, float right, float bottom, float top,
    float nearVal, float farVal)
{
    return CGMatrix4x4 ::getOrtho(left, right, bottom, top, nearVal, farVal);
}

void passthrough_vtx_shader_CG1(const CGVertexAttributes& in,
    CGVertexVaryings& out, const CGUniformData& uniforms)
{
    out.varyings[CG_POSITION_VARYING] = in.attributes[CG_POSITION_ATTRIBUTE];
    out.varyings[CG_NORMAL_VARYING]   = in.attributes[CG_NORMAL_ATTRIBUTE];
    out.varyings[CG_COLOR_VARYING]    = in.attributes[CG_COLOR_ATTRIBUTE];
    out.varyings[CG_TEXCOORD_VARYING] = in.attributes[CG_TEXCOORD_ATTRIBUTE];
    out.varyings[CG_LIGHT_COLOR_VARYING] = CGVec4(1.f,1.f,1.f,1.f);
}

void projection_only_vtx_shader_CG1(const CGVertexAttributes& in,
    CGVertexVaryings& out, const CGUniformData& uniforms)
{
    out.varyings[CG_POSITION_VARYING] = uniforms.projectionMatrix *
        in.attributes[CG_POSITION_ATTRIBUTE];
    out.varyings[CG_NORMAL_VARYING]   = in.attributes[CG_NORMAL_ATTRIBUTE];
    out.varyings[CG_COLOR_VARYING]    = in.attributes[CG_COLOR_ATTRIBUTE];
    out.varyings[CG_TEXCOORD_VARYING] = in.attributes[CG_TEXCOORD_ATTRIBUTE];
    out.varyings[CG_LIGHT_COLOR_VARYING] = CGVec4(1.f,1.f,1.f,1.f);
}

void projection_modelview_vtx_shader_CG1(const CGVertexAttributes& in,
    CGVertexVaryings& out, const CGUniformData& uniforms)
{
    out.varyings[CG_POSITION_VARYING] = uniforms.projectionMatrix *
        uniforms.modelviewMatrix * in.attributes[CG_POSITION_ATTRIBUTE];
    out.varyings[CG_NORMAL_VARYING]   = in.attributes[CG_NORMAL_ATTRIBUTE];
    out.varyings[CG_COLOR_VARYING]    = in.attributes[CG_COLOR_ATTRIBUTE];
    out.varyings[CG_TEXCOORD_VARYING] = in.attributes[CG_TEXCOORD_ATTRIBUTE];
    out.varyings[CG_LIGHT_COLOR_VARYING] = CGVec4(1.f,1.f,1.f,1.f);
}

void normal_coloring_vtx_shader_CG1(const CGVertexAttributes& in,
    CGVertexVaryings& out, const CGUniformData& uniforms)
{
    out.varyings[CG_POSITION_VARYING] = uniforms.projectionMatrix *
        uniforms.modelviewMatrix * in.attributes[CG_POSITION_ATTRIBUTE];
    out.varyings[CG_NORMAL_VARYING] = in.attributes[CG_NORMAL_ATTRIBUTE];
    out.varyings[CG_TEXCOORD_VARYING] = in.attributes[CG_TEXCOORD_ATTRIBUTE];
    out.varyings[CG_LIGHT_COLOR_VARYING] = CGVec4(1.f,1.f,1.f,1.f);

    // Set normal as color, transformed into [0,1]-range
    out.varyings[CG_COLOR_VARYING][0] = .5f * out.varyings[CG_NORMAL_VARYING][0] + .5f;
    out.varyings[CG_COLOR_VARYING][1] = .5f * out.varyings[CG_NORMAL_VARYING][1] + .5f;
    out.varyings[CG_COLOR_VARYING][2] = .5f * out.varyings[CG_NORMAL_VARYING][2] + .5f;
    out.varyings[CG_COLOR_VARYING][3] = 1.f;
}

void per_vertex_lighting_vtx_shader_CG1(const CGVertexAttributes& in,
    CGVertexVaryings& out, const CGUniformData& uniforms)
{
    // Get hold of all vertex attributes.
    CGVec4 aPos = in.attributes[CG_POSITION_ATTRIBUTE];
    CGVec4 aNrm = in.attributes[CG_NORMAL_ATTRIBUTE];
    CGVec4 aClr = in.attributes[CG_COLOR_ATTRIBUTE];
    CGVec4 aTex = in.attributes[CG_TEXCOORD_ATTRIBUTE];

    // Get hold of all vertex varyings.
    CGVec4 &vPos = out.varyings[CG_POSITION_VARYING];
    CGVec4 &vNrm = out.varyings[CG_NORMAL_VARYING];
    CGVec4 &vClr = out.varyings[CG_COLOR_VARYING];
    CGVec4 &vTex = out.varyings[CG_TEXCOORD_VARYING];
    CGVec4 &vLgt = out.varyings[CG_LIGHT_COLOR_VARYING];

    // Default program copies all attributes into all varyings used:
    vPos = aPos;
    vNrm = aNrm;
    vClr = aClr;
    vTex = aTex;

    // Transform from Object Space into Eye Space.
    vPos = uniforms.modelviewMatrix * vPos;
    vNrm = uniforms.normalMatrix * vNrm;
    vNrm = CGMath::normalize(vNrm);

    /* do the light magic */
    CGVec4 L = uniforms.light0Position - vPos;
    L = CGMath::normalize(L);

    CGVec4 emis, ambi, diff, spec;
    float dotNL = CGMath::dot(vNrm, L);
    emis = uniforms.materialEmission;
    ambi = uniforms.materialAmbient * uniforms.light0Ambient;
    if (dotNL > 0) {
        diff = uniforms.materialDiffuse * uniforms.light0Diffuse * dotNL;

        CGVec4 E = CGVec4(0.f, 0.f, 0.f, 1.f) - vPos;
        E = CGMath::normalize(E);
        CGVec4 H = L + E;
        H = CGMath::normalize(H);
        spec = uniforms.materialSpecular * uniforms.light0Specular *
            powf(max_float(0.f, CGMath::dot(vNrm, H)),
            uniforms.materialShininess);
    }
    else {
        diff = CGVec4(0.f, 0.f, 0.f, 0.f);
        spec = CGVec4(0.f, 0.f, 0.f, 0.f);
    }

    // sum up the final output color
    CGVec4 clr = ambi + diff + spec + emis;

    // Explicitly set alpha of the color
    clr[3] = uniforms.materialDiffuse[3];
    vLgt = CGMath::clamp(clr, 0.f, 1.f);

    // Transform from Eye Space into Clip Space.
    vPos = uniforms.projectionMatrix * vPos;
}

void passthrough_frag_shader_CG1(const CGFragmentData& in, CGVec4& out,
    const CGUniformData& uniforms)
{
    out = in.varyings[CG_COLOR_VARYING];
}

void texture_frag_shader_CG1(const CGFragmentData& in, CGVec4& out,
    const CGUniformData& uniforms)
{
    if (uniforms.sampler.texture) {
        /*
         * The texture converter creates an image with the alpha channel pre-
         * multiplied.
         * Then we can perform a simple composition. Transparent texture
         * support!
         */
        CGVec4 sample = uniforms.sampler.texture->sample(
            in.varyings[CG_TEXCOORD_VARYING]);

        float a = 1.f - sample[3];
        out = sample + a * in.varyings[CG_COLOR_VARYING];
    } else
        out = in.varyings[CG_COLOR_VARYING];

    /* Don't forget the light ;) */
    out = out * in.varyings[CG_LIGHT_COLOR_VARYING];
}

void per_pixel_lighting_vtx_shader_CG1(const CGVertexAttributes& in,
    CGVertexVaryings& out, const CGUniformData& uniforms)
{
    // Get hold of all vertex attributes.
    CGVec4 aPos = in.attributes[CG_POSITION_ATTRIBUTE];
    CGVec4 aNrm = in.attributes[CG_NORMAL_ATTRIBUTE];
    CGVec4 aClr = in.attributes[CG_COLOR_ATTRIBUTE];
    CGVec4 aTex = in.attributes[CG_TEXCOORD_ATTRIBUTE];

    // Get hold of all vertex varyings.
    CGVec4 &vPos = out.varyings[CG_POSITION_VARYING];
    CGVec4 &vNrm = out.varyings[CG_NORMAL_VARYING];
    CGVec4 &vClr = out.varyings[CG_COLOR_VARYING];
    CGVec4 &vTex = out.varyings[CG_TEXCOORD_VARYING];
    CGVec4 &vPEs = out.varyings[CG_POSITION_EYESPACE_VARYING];
    out.varyings[CG_LIGHT_COLOR_VARYING] = CGVec4(1.f,1.f,1.f,1.f);

    // Default program copies all attributes into all varyings used:
    vPos = aPos; vNrm = aNrm; vClr = aClr; vTex = aTex;

    // Transform from Object Space into Eye Space.
    vPEs = uniforms.modelviewMatrix * aPos;
    vNrm = uniforms.normalMatrix* vNrm;
    vNrm = CGMath::normalize(vNrm);

    // Transform from Eye Space into Clip Space.
    vPos = uniforms.projectionMatrix * vPEs;
}

void per_pixel_lighting_frag_shader_CG1(const CGFragmentData& in,
    CGVec4& out, const CGUniformData& uniforms)
{
    CGVec4 vClr = in.varyings[CG_COLOR_VARYING];
    CGVec4 nrm = in.varyings[CG_NORMAL_VARYING];
    CGVec4 txc = in.varyings[CG_TEXCOORD_VARYING];
    CGVec4 pos = in.varyings[CG_POSITION_EYESPACE_VARYING];

    // renormalize the normal
    nrm = CGMath::normalize(nrm);

    // Compute Blinn-Phong reflection model.
    CGVec4 emis, ambi, diff, spec;
    emis.set(0.0f, 0.0f, 0.0f, 0.0f); ambi.set(0.0f, 0.0f, 0.0f, 0.0f);
    diff.set(0.0f, 0.0f, 0.0f, 0.0f); spec.set(0.0f, 0.0f, 0.0f, 0.0f);

    emis = uniforms.materialEmission;
    ambi = uniforms.materialAmbient * uniforms.light0Ambient;

    // L is vector direction from current point (pos) to the light source (m_uniforms.light0Position)
    CGVec4 L = uniforms.light0Position - pos;
    L = CGMath::normalize(L);

    // calculate dot product of nrm and L
    float NdotL = CGMath::dot(nrm, L);
    if (NdotL > 0) {
            diff = uniforms.materialDiffuse * uniforms.light0Diffuse * NdotL;

            CGVec4 E = CGVec4(0.f, 0.f, 0.f, 1.f) - pos;
            E = CGMath::normalize(E);
            CGVec4 H = L + E;
            H = CGMath::normalize(H);
            spec = uniforms.materialSpecular * uniforms.light0Specular *
                    powf(max_float(0.f, CGMath::dot(nrm, H)),
                    uniforms.materialShininess);
    }

    // sum up the final output color
    CGVec4 clr;
#ifdef _CG_EXTRA_SPECULAR_NOT_AFFECTED_BY_TEXTURE
    clr = ambi + diff + emis;
    clr = CGMath::clamp(clr, 0.f, 1.f);

    /* apply texture first, then specular */
    if (uniforms.sampler.texture)
        clr = clr * uniforms.sampler.texture->sample(txc);
    clr = clr + spec;

    clr[3] = uniforms.materialDiffuse[3];
    clr = CGMath::clamp(clr, 0.f, 1.f);
#else
    clr = ambi + diff + spec + emis;
    // Explicitly set alpha of the color
    clr[3] = uniforms.materialDiffuse[3];
    // clamp color values to range [0,1]
    clr = CGMath::clamp(clr, 0.f, 1.f);

    /* apply texture */
    if (uniforms.sampler.texture)
        clr = clr * uniforms.sampler.texture->sample(txc);
#endif
    out = clr * vClr;
}
