#ifndef CG_EXTRA_H
#define CG_EXTRA_H

#include "CGMatrix.h"
#include "CGProgramInterface.h"

CGMatrix4x4 cguLookAt(float eyeX, float eyeY, float eyeZ,
    float centerX, float centerY, float centerZ,
    float upX, float upY, float upZ);
CGMatrix4x4 cguPerspective(float fov_y_, float aspect, float zNear, float zFar);
CGMatrix4x4 cguOrtho(float left, float right, float bottom, float top,
    float nearVal, float farVal);

/* vertex shaders */
void passthrough_vtx_shader_CG1(const CGVertexAttributes& in,
    CGVertexVaryings& out, const CGUniformData& uniforms);
void projection_only_vtx_shader_CG1(const CGVertexAttributes& in,
    CGVertexVaryings& out, const CGUniformData& uniforms);
void projection_modelview_vtx_shader_CG1(const CGVertexAttributes& in,
    CGVertexVaryings& out, const CGUniformData& uniforms);
void normal_coloring_vtx_shader_CG1(const CGVertexAttributes& in,
    CGVertexVaryings& out, const CGUniformData& uniforms);
void per_vertex_lighting_vtx_shader_CG1(const CGVertexAttributes& in,
    CGVertexVaryings& out, const CGUniformData& uniforms);

/* use with per_pixel_lighting_frag_shader_CG1 */
void per_pixel_lighting_vtx_shader_CG1(const CGVertexAttributes& in,
    CGVertexVaryings& out, const CGUniformData& uniforms);

/* fragment shaders */
void passthrough_frag_shader_CG1(const CGFragmentData& in, CGVec4& out,
    const CGUniformData& uniforms);
void texture_frag_shader_CG1(const CGFragmentData& in, CGVec4& out,
    const CGUniformData& uniforms);

/* use with per_pixel_lighting_vtx_shader_CG1 */
void per_pixel_lighting_frag_shader_CG1(const CGFragmentData& in,
    CGVec4& out, const CGUniformData& uniforms);

#endif
