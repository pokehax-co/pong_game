#ifndef RENDERER_H
#define RENDERER_H

#include <CGContext.h>
#include <CGQuadric.h>
#include "map.h"

/*
 * The TextureHelper class helps with loading textures from a file.
 */
class TextureHelper
{
public:
    TextureHelper(void);
    ~TextureHelper(void);

    bool load(const char *file);
    int texture_id(CGContext *ctx);

    int get_width(void)
    {
        return _width;
    }

    int get_height(void)
    {
        return _height;
    }

private:
    unsigned char *_data;
    int _width, _height;
    int _texture_id;
};

/*
 * The PixelSimulator class computes parameters to simulate a low-resolution
 * pixel screen using 3D primitives.
 * Given the dimensions of the viewport, we calculate the minimum, average, and
 * maximum possible sizes of a simulated pixel, the size of such a pixel for a
 * viewport in the [-1, 1] range, and how large the simulated screen can be
 * (in pixel dimensions).
 */
class PixelSimulator
{
public:
    /*
     * Initialization function. Computes the size of a "virtual" pixel in 3D
     * coordinates, and the maximum dimensions of a "virtual" display (to align
     * objects).
     */
    static void compute_params(unsigned int width, unsigned int height);

    /*
     * Returns the size of a "virtual" pixel in 3D coordinates, along the X,
     * Y, and Z axis. We use pointers because we allow parameters to be set
     * NULL if they are not necessary.
     */
    static void pixel_size(float *span_x, float *span_y, float *span_z);

    /*
     * Returns the size of a "virtual" display in "virtual" pixels. We use
     * pointers because we allow parameters to be set NULL if they are not
     * necessary.
     */
    static void pixel_display(unsigned int *width, unsigned int *height);

private:
    /*
     * Computes the logarithm of an integer in base 2. Operating on integers
     * has the advantage of implicit rounding, which helps compute_params()
     * in avoiding floating point as much as possible.
     */
    static unsigned int _log2(unsigned int x);

    static float _span_x, _span_y, _span_z;
    static unsigned int _sim_width, _sim_height;
};

/* forward declaration */
class Game;

/*
 * The GameRenderer class is responsible for drawing the scene. It can be
 * toggled between 2D and 3D mode. Also, it exports properties of its scene
 * to help the game logic figuring out limits and positions of objects in
 * the scene. To draw objects, it further needs information computed by the
 * game logic. Other than that, the renderer is free to do anything it wants.
 */
class GameRenderer
{
public:
    /*
     * Constructor and destructor. We need the CGContext created by the main()
     * function to render the game, the Game object to access balls and rackets
     * on the field, and the viewport dimensions for all sorts of rendering
     * tasks.
     */
    GameRenderer(CGContext *context, Game *game, unsigned int width,
        unsigned int height);
    ~GameRenderer(void);

    /*
     * Switches 3D mode on or off (2D mode). The Game class tracks the variable
     * passed to this function to achieve a "toggle" effect.
     */
    void set_mode(bool d3);

    /*
     * Renders the current moment of the scene. This includes the map, all
     * objects on the field, the score display, and if active, menus as well.
     */
    void draw_frame(void);

    /*
     * Moves the 3D camera back to its initial position (Phi = 0°, Theta = 0°,
     * Radius = 2 in sphere coordinates). Used when the game is reset using the
     * R key.
     */
    void reset_3d_camera(void);

    /*
     * Retrieves the current position of the 3D camera. Used when moving the
     * camera around.
     */
    void get_3d_camera(float &phi, float &theta, float &distance) const;

    /*
     * Sets the new position of the 3D camera. Used when moving the camera
     * around.
     */
    void set_3d_camera(float phi, float theta, float distance);

    /*
     * Retrieves the map currently presented to the player. There is one map
     * rendered in 2D mode, and another one rendered in 3D mode. From there,
     * the Game class (and the GameRenderer class itself) can retrieve
     * information on all objects to be rendered, some of which is important for
     * the game logic as well.
     */
    const Map* get_active_map(void) const;

    /*
     * These values can be combined to control which aspects of the game are
     * rendered. For example, (STATE_GAME | STATE_MENU) renders the game scene,
     * and the menu is lain over it.
     * set_state_mask() sets this mask, while get_state_mask() retrieves its
     * current combination.
     */
    enum State {
        STATE_MENU = (1U << 0),
        STATE_HELP = (1U << 1),
        STATE_GAME = (1U << 2)
    };
    void set_state_mask(unsigned int state);
    unsigned int get_state_mask(void);

private:
    Game *_game;
    CGContext *_ctx;
    unsigned int _width, _height;
    float _aspect;

    bool _mode3d;
    unsigned int _state;

    TextureHelper _menu_tex;
    TextureHelper _help_tex;

    /* 2D rendering common */
    CGMatrix4x4 _projection_2d;
    const CGMatrix4x4 _mv_identity;

    /* 2D overlay */
    CGMatrix4x4 _mv_digit_left[2];
    CGMatrix4x4 _mv_digit_right[2];

    /* 3D rendering */
    struct {
        float theta;
        float phi;
        float distance;

        CGMatrix4x4 projection;
        CGMatrix4x4 camera;
    } _3d;

    /* active */
    const CGMatrix4x4 *_active_projection;
    const CGMatrix4x4 *_active_camera;

    MapClassic *_map_2d;
    MapUserDefined *_map_3d;

    /*
     * Draws a single object of the 2D or 3D map on the screen.
     */
    void _draw_map_object(const MapObject *o);

    /*
     * Draws the map. The basic map mesh, the two walls at the top and at the
     * bottom, and the "random object" in the middle are drawn by this function.
     */
    void _draw_map(void);

    /*
     * Draws dynamic objects, i.e. objects which are in motion, like walls and
     * rackets.
     */
    void _draw_objects(void);

    /*
     * Draws the score of both players on the screen. This requires an
     * orthographic projection to simulate the 2D effect, hence the method's
     * name.
     */
    void _draw_2d_score(void);

    /*
     * Draws a texture created by the TextureHelper class in the middle of the
     * screen. Used for drawing menu and help. This requires an orthographic
     * projection to simulate the 2D effect, hence the method's name.
     */
    void _draw_2d_texture(TextureHelper &tex);
};

#endif
