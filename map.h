#ifndef MAP_H
#define MAP_H

#include <CG.h>
#include <CGContext.h>
#include <stddef.h>

/*
 * Human-readable names for the indices of the 4D vector returned by
 * Map::playfield_boundaries().
 */
#define MAP_BOUND_XMIN 0
#define MAP_BOUND_YMIN 1
#define MAP_BOUND_XMAX 2
#define MAP_BOUND_YMAX 3

/*
 * This abstract class provides an interface to retrieve visual and collision
 * data from an object.
 */
class MapObject
{
public:
    virtual ~MapObject(void) {}

    /*
     * These functions return (for the CG renderer attributes):
     *   Vertex count
     *   Vertices
     *   Normals
     *   Texture coordinates
     *   Colors
     */
    virtual size_t vertices_count(void) const = 0;
    virtual const float* get_vertices(void) const = 0;
    virtual const float* get_normals(void) const = 0;
    virtual const float* get_texcoords(void) const = 0;
    virtual const float* get_colors(void) const = 0;

    /*
     * These functions are used by the GameRenderer class to retrieve texture
     * information for the vertices of this object.
     * - texture_indices_count() returns the number of indices in the object.
     *   Usually, this is the same as the number of vertices.
     * - get_texture_indices() returns an array of texture indices for each
     *   vertex in the object.
     * - textures_count() returns the number of available textures.
     * - get_textures() returns an array of texture IDs referencing the actual
     *   textures. They are created using the
     *   CGContext::cgGenTexture_cgBindTexture_cgTexImage2D() function, thus,
     *   they can be passed directly to other CGContext functions.
     */
    virtual size_t texture_indices_count(void) const = 0;
    virtual const int* get_texture_indices(void) const = 0;
    virtual size_t textures_count(void) const = 0;
    virtual const int* get_textures(void) const = 0;

    /*
     * The MapObject class allows separation of visual and collision data.
     * (Concept inspired by the N64 Zelda games.)
     * For the Pong game, however, this is not much of concern for us because
     * due to the low number of triangles, there's no real penalty in using the
     * same data for both.
     */
    virtual size_t collision_vertices_count(void) const = 0;
    virtual const float* get_collision_vertices(void) const = 0;
};

/*
 * This abstract class provides information on the map and its objects.
 */
class Map
{
public:
    virtual ~Map(void) {}

    /*
     * This function returns the environment color (RGB) of the map. It is used
     * by the GameRenderer to clear the screen.
     */
    virtual void clear_color(float color[3]) const = 0;

    /*
     * These functions are intended for the Game class, in order to implement
     * basic limitations on objects.
     * - hwall_thickness() returns the height (as seen from 2D mode) of one of
     *   the walls at the top or at the bottom of the screen. The game assumes
     *   that both walls exist and have the same size.
     *   [Used to limit the vertical movement of rackets.]
     * - racket_thickness() returns the width (as seen from 2D mode) of one of
     *   the rackets at the left or at the right of the screen. The game assumes
     *   that both rackets exist and have the same size.
     *   [Used to place the ball at the center of the racket.]
     * - racket_length() returns the height (as seen from 2D mode) of one of the
     *   rackets.
     *   [Used to limit the vertical movement of rackets.]
     * - ball_radius() returns the radius of the ball moving around the screen.
     *   [Used to create balls with this radius, and also for collision
     *   detection between balls.]
     */
    virtual float hwall_thickness(void) const = 0;
    virtual float racket_thickness(void) const = 0;
    virtual float racket_length(void) const = 0;
    virtual float ball_radius(void) const = 0;

    /*
     * This function returns the boundaries of the main play field.
     * The boundaries are packed into a 4-dimensional vector. The values have
     * the following meaning:
     *   Index 0: Minimum X value
     *   Index 1: Minimum Y value
     *   Index 2: Maximum X value
     *   Index 3: Maximum Y value
     * Used by the Game class for ball placement and ball out-of-field checks.
     */
    virtual void playfield_boundaries(CGVec4 &bound) const = 0;

    /*
     * get_map_object() is responsible for returning all objects of the map.
     * Any of the OBJECT_* enumeration values (except FIRST and LAST) can be
     * passed, and the object will return the corresponding MapObject.
     * Used for rendering and collision detection.
     */
    enum Object {
        OBJECT_FIRST = 0,
        OBJECT_RACKET_ENEMY = OBJECT_FIRST,
        OBJECT_RACKET_PLAYER,
        OBJECT_WALL_TOP,
        OBJECT_WALL_BOTTOM,
        OBJECT_RANDOM_OBJECT,
        OBJECT_BALL,
        OBJECT_MAP,
        OBJECT_LAST = OBJECT_MAP
    };
    virtual const MapObject* get_map_object(Object object) const = 0;
};

/*
 * The MapUserDefined class implements the Map class. It retrieves its data from
 * an external map file (map.bin by default). This file is created by a map
 * converter script (map_converter.py in the map/ directory) and contains all
 * map objects intended by the Map class in binary form with small overhead for
 * the CG renderer.
 * This map is rendered by the GameRenderer in 3D mode.
 */
class MapUserDefined : public Map
{
public:
    /*
     * Create a new map from the specified map file. The map is scaled along the
     * X axis by xscale, and along the Y axis by yscale. Textures are created
     * using the specified context. (This means that the same context must be
     * used for the rendering process, if you want to render textures.)
     */
    MapUserDefined(float xscale, float yscale, CGContext *ctx,
        const char *filename);
    ~MapUserDefined(void);

    /* inherited from Map */
    void clear_color(float color[3]) const;
    float hwall_thickness(void) const;
    float racket_thickness(void) const;
    float racket_length(void) const;
    float ball_radius(void) const;

    void playfield_boundaries(CGVec4 &bound) const;
    const MapObject* get_map_object(Object object) const;

private:
    unsigned char *_mapfile;
    size_t _mapfile_size;

    float _xscale, _yscale;
    float _hwall_thickness;
    float _racket_thickness;
    float _racket_length;

    MapObject *_racket_enemy;
    MapObject *_racket_player;
    MapObject *_wall_top;
    MapObject *_wall_bottom;
    MapObject *_random_object;
    MapObject *_ball;
    MapObject *_map;
};

/*
 * The MapClassic class implements the Map class. Using 3D technology, it aims
 * to reproduce the style of a two-dimensional Pong game, like the 1972
 * original. For this, it produces objects based on "virtual" pixels, with the
 * help of the PixelSimulator class (see renderer.h).
 * This map is rendered by the GameRenderer in 3D mode.
 */
class MapClassic : public Map
{
public:
    /*
     * Create a new "classic" map in memory. The map has the dimensions
     * specified by xscale and yscale. [Code expects xscale = display aspect
     * ratio and yscale = 1 most of the time.]
     */
    MapClassic(float xscale, float yscale);
    ~MapClassic(void);

    /* inherited from Map */
    void clear_color(float color[3]) const;
    float hwall_thickness(void) const;
    float racket_thickness(void) const;
    float racket_length(void) const;
    float ball_radius(void) const;

    void playfield_boundaries(CGVec4 &bound) const;
    const MapObject* get_map_object(Object object) const;

private:
    float _xscale, _yscale;
    MapObject *_racket_enemy;
    MapObject *_racket_player;
    MapObject *_wall_top;
    MapObject *_wall_bottom;
    MapObject *_random_object;
    MapObject *_ball;
    MapObject *_map;
};

#endif
