#ifndef GAME_H
#define GAME_H

#include "config.h"

/*
 * Stores the current Y position of a racket.
 */
struct Racket
{
    float y;

    Racket(void) : y(0.f) {}
};

/*
 * Stores information about a ball: whether it is active, its current 2D
 * position, its current velocity [normal vector], and its current speed.
 */
struct Ball
{
    bool active;
    float posx, posy;
    float vecx, vecy;
    float speed;

    Ball(void) : posx(0.f), posy(0.f), vecx(0.f), vecy(0.f), speed(.1f) {}

    void defaults(void)
    {
        posx = 0.f;
        posy = 0.f;
        vecx = 0.f;
        vecy = 0.f;
        speed = .1f;
    }
};

/* forward declaration */
class GameRenderer;

/*
 * The Game class is the heart of the Pong game. It is responsible for key
 * input, a less-than-average intelligence and collision detection, while it
 * instructs the renderer to present the current game state on the screen.
 */
class Game
{
public:
    Game(CGContext *context);
    ~Game(void);

    /* For the CG1Helper program step, we need a function callable like a
       C function. We can do it with static methods in C++ classes, but there
       is no associated object, so we pass it via the parameter. */
    static void program_step_cb(void *param)
    {
        ((Game*)param)->_program_step();
    }

    /*
     * Public getters for moving objects, intended for the renderer.
     */
    const Ball* get_ball(unsigned int ball)
    {
        return &_balls[ball];
    }
    const Racket* get_enemy_racket(void)
    {
        return &_enemy;
    }
    const Racket* get_player_racket(void)
    {
        return &_player;
    }

    /*
     * Public getters for the score, intended for the renderer.
     */
    unsigned int get_enemy_score(void) const
    {
        return _score_enemy;
    }

    unsigned int get_player_score(void) const
    {
        return _score_player;
    }

private:
    CGContext *_context;
    GameRenderer *_renderer;

    bool _3d;
    bool _menu_mode;
    bool _help_mode;
    bool _two_players;
    bool _game_started;
    int _surprise_active_balls;
    unsigned int _frame_counter;
    unsigned int _score_enemy, _score_player;

    Ball _balls[1 + GAME_SURPRISE_BALLS];
    Racket _player, _enemy;

    /*
     * This is the core function of the game, executed by the CG1Helper class
     * at every frame draw. See the class description for more information.
     */
    void _program_step(void);

    /*
     * This function handles key input, not just during actual gameplay, but for
     * the menus as well.
     */
    void _process_user_input(void);

    /*
     * This function implements a horrible intelligence for the single-player
     * mode. See comments in the function itself for details.
     */
    void _process_ai(void);

    /*
     * This function handles collision detection of the specified ball. It is
     * called for every ball on the field. There are various detection and
     * response algorithms; see comments in the function itself for details.
     */
    void _process_physics(unsigned int ball);

    /*
     * Two methods to initialize a ball.
     * _init_ball() with one parameter initializes all balls.
     * _init_ball() with two parameters initializes the specified ball. The
     * left_side parameter, which decides at which racket the ball will be
     * placed, is only honored for ball = 0 (the gameplay ball).
     * See comments in the function itself for details.
     */
    void _init_ball(bool left_side);
    void _init_ball(bool left_side, unsigned int ball);

    /*
     * Resets the game to a mostly clean state. Called when a game is begun via
     * the menu, or when the R key is pressed.
     */
    void _reset(void);
};

#endif
