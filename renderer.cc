#include "renderer.h"
#include "game.h"
#include <stdio.h>
#include <assert.h>
#include <cg-extra.h>
#include <CGImageFile.h>

/******************************************************************************/

/*
 * The TextureHelper class loads a texture from a file and returns a texture ID
 * which can be used during CG renderer calls.
 */

TextureHelper::TextureHelper(void) : _data(NULL), _width(0), _height(0),
    _texture_id(-1)
{}

TextureHelper::~TextureHelper(void)
{
    free((void*)_data);
}

bool TextureHelper::load(const char *file)
{
    if (!file || !*file)
        return false;

    int width, height;
    unsigned char *p = cgImageLoad(file, &width, &height);
    if (!p)
        return false;

    free(_data);
    _data = p;
    _width = width, _height = height;
    return true;
}

int TextureHelper::texture_id(CGContext *ctx)
{
    if (_texture_id < 0) {
        _texture_id = ctx->cgGenTexture_cgBindTexture_cgTexImage2D(
            _width, _height, _data);
    }
    return _texture_id;
}

/******************************************************************************/

/*
 * PixelSimulator class. See renderer.h for more information.
 */

/* documented in renderer.h */
unsigned int PixelSimulator::_log2(unsigned int num)
{
    unsigned int r = 0;
    r += !!(num >>= 1); r += !!(num >>= 1); r += !!(num >>= 1);
    r += !!(num >>= 1); r += !!(num >>= 1); r += !!(num >>= 1);
    r += !!(num >>= 1); r += !!(num >>= 1); r += !!(num >>= 1);
    r += !!(num >>= 1); r += !!(num >>= 1); r += !!(num >>= 1);
    r += !!(num >>= 1); r += !!(num >>= 1); r += !!(num >>= 1);
    r += !!(num >>= 1);
    return r;
}

float PixelSimulator::_span_x = 0.f;
float PixelSimulator::_span_y = 0.f;
float PixelSimulator::_span_z = 0.f;
unsigned int PixelSimulator::_sim_width = 0;
unsigned int PixelSimulator::_sim_height = 0;

/* documented in renderer.h */
void PixelSimulator::pixel_size(float *span_x, float *span_y, float *span_z)
{
    if (span_x) *span_x = _span_x;
    if (span_y) *span_y = _span_y;
    if (span_z) *span_z = _span_z;
}

/* documented in renderer.h */
void PixelSimulator::pixel_display(unsigned int *width, unsigned int *height)
{
    if (width) *width = _sim_width;
    if (height) *height = _sim_height;
}

/* documented in renderer.h */
void PixelSimulator::compute_params(unsigned int width, unsigned int height)
{
    /* Determine the largest common divider of width and height. The result is
       the size of a simulated pixel, rounded down to a power of 2. */
    unsigned int pixel_size;
    do {
        int width_ = width, height_ = height;
        while (height_ > 0) {
            int h = width_ % height_;
            width_ = height_;
            height_ = h;
        }
        pixel_size = width_;
    } while (0);
    pixel_size = _log2(pixel_size);

    /* size = pow(2,width). Minimum is 8 pixels (if we can get that large). */
    unsigned int min_size = pixel_size < 3 ? pixel_size : 3;
    unsigned int max_size = pixel_size;

    /* use the average of minimum and maximum width */
    unsigned int avg_size = (min_size + max_size) >> 1;
    fprintf(stderr, "Pixel simulator: Size of simulated pixel is %ux%u px.\n",
        1U << avg_size, 1U << avg_size);

    /* calculate simulated width and height */
    _sim_width = width >> avg_size;
    _sim_height = height >> avg_size;

    /*
     * Transform pixel sizes to size of field.
     * Basically, the Y span is the (Y size of pixel) : (viewport height) ratio,
     * which is correct. (doubled because the field spans from -1 to 1)
     *
     * For the X span, we don't divide by width, because we want to preserve
     * the field's X span, which is from (-aspect ratio) to (aspect ratio).
     * The aspect ratio is actually width/height, and width = ratio*height.
     *
     * Z span doesn't use negative coordinates, therefore no multiplication with
     * 2. Thus, calculate Z first, X and Y are just multiplied by 2.
     */
    _span_z = (float)(1U << avg_size) / (float)height;
    _span_x = 2.f * _span_z;
    _span_y = 2.f * _span_z;
}

/**************************************/

/*
 * The Digit class pre-compiles an array of vertices to render the digits 0-9 on
 * the simulated pixelated display.
 * Every digit has the same width (3 "virtual" pixels) and height (5 "virtual"
 * pixels), with the coordinate origin at the center of each digit.
 * The digits are colored white, and have a small black drop shadow behind them.
 */
class Digit
{
public:
    /*
     * Computes all information needed by CG renderer calls (vertices, colors,
     * number of vertices).
     */
    static void init(void);

    /*
     * Renders the specified digit using the specified CG context.
     * Note: translation and projection matrices must be set up by the caller.
     */
    static void render(CGContext *ctx, unsigned int digit);

private:
    /*
     * Deletes the information computed by init(). Because all members of the
     * class are static, and there is no construction of an instance of class
     * Digit, we need help from the C standard library (atexit()) to invoke
     * some sort of "destructor".
     */
    static void _destroy(void);

    static float *_vtxs[10];
    static float *_clrs[10];
    static unsigned int _count[10];
};

float *Digit::_vtxs[10] = {NULL};
float *Digit::_clrs[10] = {NULL};
unsigned int Digit::_count[10] = {0};

/* documented in class declaration */
void Digit::init(void)
{
    float pixel_size_x, pixel_size_y;
    PixelSimulator::pixel_size(&pixel_size_x, &pixel_size_y, NULL);

    /*
     * Translation of integer coordinates of digit lines to
     * playfield coordinates.
     */
    float offsetx = -2.f * pixel_size_x;
    float offsety = -2.5f * pixel_size_y;
    float offsetx_q = offsetx + pixel_size_x / 4.f;
    float offsety_q = offsety - pixel_size_y / 4.f;

    for (unsigned int i = 0; i < 10; ++i) {
        /*
         * Pixel coordinates of the digits. Interestingly, they are positive
         * here, but they're translated using the variables above anyway.
         */
        static const char digit_coord[10][5*4] = {
            /* 0 */ { 0,0,0,4, 2,0,2,4, 0,0,2,0, 0,4,2,4 },
            /* 1 */ { 2,0,2,4 },
            /* 2 */ { 0,0,0,2, 2,2,2,4, 0,0,2,0, 0,2,2,2, 0,4,2,4 },
            /* 3 */ { 2,0,2,4, 0,0,2,0, 0,2,2,2, 0,4,2,4 },
            /* 4 */ { 2,0,2,4, 0,2,2,2, 0,2,0,4 },
            /* 5 */ { 0,2,0,4, 2,0,2,2, 0,0,2,0, 0,2,2,2, 0,4,2,4 },
            /* 6 */ { 0,0,0,4, 2,0,2,2, 0,0,2,0, 0,2,2,2, 0,4,2,4 },
            /* 7 */ { 2,0,2,4, 0,4,2,4 },
            /* 8 */ { 0,0,0,4, 2,0,2,4, 0,0,2,0, 0,2,2,2, 0,4,2,4 },
            /* 9 */ { 0,2,0,4, 2,0,2,4, 0,0,2,0, 0,2,2,2, 0,4,2,4 },
        };
        static const char digit_coord_count[10] = { 4,1,5,4,3,5,5,2,5,5 };

        /*
         * What does 3 (or 4) * 6 * 2 mean?
         * 3 (or 4): Number of components. A vertex has 3 components (XYZ),
         * while a color has 4 (RGBA).
         * 6: We produce quads with two triangles. Each triangle has 3 vertices,
         * so we need 6 of them.
         * 2: Not just for the digit itself (which is white), but also for the
         * backdrop (which is black).
         */
        _vtxs[i] = new float[digit_coord_count[i] * 3 * 6 * 2];
        _clrs[i] = new float[digit_coord_count[i] * 4 * 6 * 2];
        _count[i] = 0;

        /* Fill vertices and colors of both "layers" in one pass. */
        float *p = _vtxs[i];
        float *q = _vtxs[i] + digit_coord_count[i] * 3 * 6;
        float *r = _clrs[i];
        float *s = _clrs[i] + digit_coord_count[i] * 4 * 6;

        for (int j = 0; j < digit_coord_count[i]; ++j) {
            float pt[4];

            pt[0] = digit_coord[i][4*j+0] * pixel_size_x;
            pt[1] = digit_coord[i][4*j+1] * pixel_size_y;
            pt[2] = (digit_coord[i][4*j+2] + 1) * pixel_size_x;
            pt[3] = (digit_coord[i][4*j+3] + 1) * pixel_size_y;

            /*
             * To force the digit to render above the backdrop, assign it a
             * higher Z coordinate. The orthogonal projection gets rid of
             * depth anyway.
             */
            *p++ = pt[0] + offsetx; *q++ = pt[0] + offsetx_q;
            *p++ = pt[1] + offsety; *q++ = pt[1] + offsety_q;
            *p++ = 10.f; *q++ = 9.95f;
            *r++ = 1.f; *r++ = 1.f; *r++ = 1.f; *r++ = 1.f;
            *s++ = 0.f; *s++ = 0.f; *s++ = 0.f; *s++ = 1.f;

            *p++ = pt[2] + offsetx; *q++ = pt[2] + offsetx_q;
            *p++ = pt[3] + offsety; *q++ = pt[3] + offsety_q;
            *p++ = 10.f; *q++ = 9.95f;
            *r++ = 1.f; *r++ = 1.f; *r++ = 1.f; *r++ = 1.f;
            *s++ = 0.f; *s++ = 0.f; *s++ = 0.f; *s++ = 1.f;

            *p++ = pt[0] + offsetx; *q++ = pt[0] + offsetx_q;
            *p++ = pt[3] + offsety; *q++ = pt[3] + offsety_q;
            *p++ = 10.f; *q++ = 9.95f;
            *r++ = 1.f; *r++ = 1.f; *r++ = 1.f; *r++ = 1.f;
            *s++ = 0.f; *s++ = 0.f; *s++ = 0.f; *s++ = 1.f;

            *p++ = pt[0] + offsetx; *q++ = pt[0] + offsetx_q;
            *p++ = pt[1] + offsety; *q++ = pt[1] + offsety_q;
            *p++ = 10.f; *q++ = 9.95f;
            *r++ = 1.f; *r++ = 1.f; *r++ = 1.f; *r++ = 1.f;
            *s++ = 0.f; *s++ = 0.f; *s++ = 0.f; *s++ = 1.f;

            *p++ = pt[2] + offsetx; *q++ = pt[2] + offsetx_q;
            *p++ = pt[1] + offsety; *q++ = pt[1] + offsety_q;
            *p++ = 10.f; *q++ = 9.95f;
            *r++ = 1.f; *r++ = 1.f; *r++ = 1.f; *r++ = 1.f;
            *s++ = 0.f; *s++ = 0.f; *s++ = 0.f; *s++ = 1.f;

            *p++ = pt[2] + offsetx; *q++ = pt[2] + offsetx_q;
            *p++ = pt[3] + offsety; *q++ = pt[3] + offsety_q;
            *p++ = 10.f; *q++ = 9.95f;
            *r++ = 1.f; *r++ = 1.f; *r++ = 1.f; *r++ = 1.f;
            *s++ = 0.f; *s++ = 0.f; *s++ = 0.f; *s++ = 1.f;

            _count[i] += 12;
        }
    }

    /* delete digits when we exit application. needs atexit() because all
       methods are static (= no 'this' object, no ctors, no dtors...) */
    atexit(_destroy);
}

/* documented in class declaration */
void Digit::_destroy(void)
{
    for (unsigned int i = 0; i < 10; ++i) {
        delete[] _vtxs[i];
        delete[] _clrs[i];
    }
}

/* documented in class declaration */
void Digit::render(CGContext *ctx, unsigned int digit)
{
    assert(digit < 10);
    ctx->cgVertexAttribPointer(CG_POSITION_ATTRIBUTE, _vtxs[digit]);
    ctx->cgVertexAttribPointer(CG_COLOR_ATTRIBUTE, _clrs[digit]);
    ctx->cgVertexAttribPointer(CG_NORMAL_ATTRIBUTE, NULL);
    ctx->cgVertexAttribPointer(CG_TEXCOORD_ATTRIBUTE, NULL);
    ctx->cgDrawArrays(CG_TRIANGLES, 0, _count[digit]);
}

/******************************************************************************/

/*
 * PixelSimulator class. See renderer.h for more information.
 */

/* documented in renderer.h */
GameRenderer::GameRenderer(CGContext *context, Game *game, unsigned int width,
    unsigned int height) :
    _game(game), _ctx(context),
    _width(width), _height(height), _aspect((float)width / (float)height),
    _mode3d(false), _state(0U)
{
    /* Set up pixel simulator */
    PixelSimulator::compute_params(width, height);
    Digit::init();

    /* load textures */
    if (!_menu_tex.load(GAME_FILE_MENU_TEXTURE)) {
        fprintf(stderr, "cannot load texture %s\n", GAME_FILE_MENU_TEXTURE);
        exit(1);
    }
    if (!_help_tex.load(GAME_FILE_HELP_TEXTURE)) {
        fprintf(stderr, "cannot load texture %s\n", GAME_FILE_HELP_TEXTURE);
        exit(1);
    }

    /* Move 3D camera to original position and set up the projection needed
       for rendering */
    reset_3d_camera();
    _3d.projection = cguPerspective(60.f, _aspect, .001f, 1000.f);

    /* Compute orthogonal projection (= play field dimensions) */
    _projection_2d = cguOrtho(-_aspect, _aspect, -1.f, 1.f, -20.f, 0.f);

    /* Select 2D projection and camera for first launch */
    _active_projection = &_projection_2d;
    _active_camera = &_mv_identity;

    /* Initialize the classic 2D-style map */
    _map_2d = new MapClassic(_aspect, 1.f);

    /* Initialize the new 3D map */
    _map_3d = new MapUserDefined(_aspect, 1.f, context, GAME_FILE_3D_MAP);

    /*
     * Pre-compute the translation matrices to render the score digits.
     */
    float pixel_size_x, pixel_size_y;
    PixelSimulator::pixel_size(&pixel_size_x, &pixel_size_y, NULL);

    float y = 1.f - 4.f * pixel_size_y;
    float x1l = -_aspect + 3.5f * pixel_size_x;
    float x2l = -_aspect + 7.5f * pixel_size_x;
    float x1r = _aspect - 3.5f * pixel_size_x;
    float x2r = _aspect - 7.5f * pixel_size_x;

    _mv_digit_left[0] = CGMatrix4x4::getTranslationMatrix(x1l, y, 0.f);
    _mv_digit_left[1] = CGMatrix4x4::getTranslationMatrix(x2l, y, 0.f);
    _mv_digit_right[0] = CGMatrix4x4::getTranslationMatrix(x1r, y, 0.f);
    _mv_digit_right[1] = CGMatrix4x4::getTranslationMatrix(x2r, y, 0.f);
}

GameRenderer::~GameRenderer(void)
{
    /* Clean up resources */
    delete _map_3d;
    delete _map_2d;
}

/* documented in renderer.h */
void GameRenderer::set_state_mask(unsigned int state)
{
    _state = state;
}

/* documented in renderer.h */
unsigned int GameRenderer::get_state_mask(void)
{
    return _state;
}

/* documented in renderer.h */
void GameRenderer::set_mode(bool d3)
{
    _mode3d = d3;
}

/* documented in renderer.h */
void GameRenderer::draw_frame(void)
{
    /*
     * Clear the screen. If the game itself is not drawn, use the menu's
     * background color, otherwise take the color from the map.
     */
    _ctx->cgClear(CG_COLOR_BUFFER_BIT | CG_DEPTH_BUFFER_BIT);
    if (!(_state & STATE_GAME))
        _ctx->cgClearColor(GAME_EXPERIENCE_BACKGROUND_COLOR, 0.f);
    else {
        float bg[3];
        get_active_map()->clear_color(bg);
        _ctx->cgClearColor(bg[0], bg[1], bg[2], 0.f);
    }

    /* Enable necessary extensions for rendering */
    _ctx->cgEnable(CG_DEPTH_TEST);
    _ctx->cgEnable(CG_CULL_FACE);
    _ctx->cgEnable(CG_USE_BRESENHAM);

    /* Set up viewport so the renderer knows that we occupy the whole screen */
    _ctx->cgViewport(0, 0, _width, _height);

    /*
     * Shader selection. The used shaders are changed several times in a loop,
     * these are used when the map is drawn.
     * In 3D mode, we want to apply per-vertex lighting because the task says
     * so. In 2D mode, this is not necessary, a simple projection-and-modelview
     * transformation is sufficient.
     * The "textured" fragment shader applies the vertex interpolated vertex
     * color and the texture, if one is present, so we can use it for both.
     */
    _ctx->cgUseProgram(_mode3d ? per_vertex_lighting_vtx_shader_CG1 :
        projection_modelview_vtx_shader_CG1, texture_frag_shader_CG1);

    /*
     * Selects projection and camera modelview matrix based on the current
     * mode.
     */
    if (_mode3d) {
        _active_projection = &_3d.projection;
        _active_camera = &_3d.camera;
    } else {
        _active_projection = &_projection_2d;
        _active_camera = &_mv_identity;
    }

    /*
     * Because the per-vertex lighting shader already applies the input vertex
     * color, the light only needs to emit grayscale light.
     * Low ambience with medium diffusion and speculation yields good results.
     */
    static const float ambient[4] =
        { GAME_LIGHT_AMBIENCE, GAME_LIGHT_AMBIENCE, GAME_LIGHT_AMBIENCE, 1.f };
    _ctx->cgUniform4fv(CG_ULOC_LIGHT0_AMBIENT, 1, ambient);

    static const float diffuse[4] = { GAME_LIGHT_DIFFUSION,
        GAME_LIGHT_DIFFUSION, GAME_LIGHT_DIFFUSION, 1.f };
    _ctx->cgUniform4fv(CG_ULOC_LIGHT0_DIFFUSE, 1, diffuse);

    static const float specular[4] = { GAME_LIGHT_SPECULAR, GAME_LIGHT_SPECULAR,
        GAME_LIGHT_SPECULAR, GAME_LIGHT_SPECULAR };
    _ctx->cgUniform4fv(CG_ULOC_LIGHT0_SPECULAR, 1, specular);

    static const float position[4] = { GAME_LIGHT_POSITION, 1.f };
    _ctx->cgUniform4fv(CG_ULOC_LIGHT0_POSITION, 1, position);

    /*
     * For proper light application, the vertices need to behave as if they
     * would appear white, even without a light source present (or too far
     * away).
     */
    static const float WHITE_10[4] = { 1.f, 1.f, 1.f, 1.f };
    _ctx->cgUniform4fv(CG_ULOC_MATERIAL_AMBIENT, 1, WHITE_10);
    _ctx->cgUniform4fv(CG_ULOC_MATERIAL_DIFFUSE, 1, WHITE_10);
    _ctx->cgUniform4fv(CG_ULOC_MATERIAL_SPECULAR, 1, WHITE_10);

    static const float shininess = GAME_LIGHT_SHININESS;
    _ctx->cgUniform1fv(CG_ULOC_MATERIAL_SHININESS, 1, &shininess);

    static const float BLACK_10[4] = { 0.f, 0.f, 0.f, 1.f };
    _ctx->cgUniform4fv(CG_ULOC_MATERIAL_EMISSION, 1, BLACK_10);

    /* Draw the actual game if the state allows. */
    if (_state & STATE_GAME) {
        _draw_map();
        _draw_objects();
        _draw_2d_score();
    }

    /* Draw the menus if the state allows. (Exclusive-or because 1. the help
       texture is larger, 2. drawing both kills any performance we have.) */
    if (_state & STATE_HELP)
        _draw_2d_texture(_help_tex);
    else if (_state & STATE_MENU)
        _draw_2d_texture(_menu_tex);
}

/* documented in renderer.h */
void GameRenderer::_draw_2d_texture(TextureHelper &tex)
{
    /*
     * We're drawing a 2D texture (menu or help). This requires a 2D setup (as
     * if we were in 2D mode or drawing the score). Establish it.
     */
    _ctx->cgUniformMatrix4fv(CG_ULOC_PROJECTION_MATRIX, 1, false,
        _projection_2d.getDataPtr());
    _ctx->cgUniformMatrix4fv(CG_ULOC_MODELVIEW_MATRIX, 1, false,
        _mv_identity.getDataPtr());
    _ctx->cgUseProgram(projection_modelview_vtx_shader_CG1,
        texture_frag_shader_CG1);

    /*
     * For the current texture, determine the coordinates of the 6 vertices
     * needed to present it on the screen. We use the fact that the origin of
     * the projection is at the center of the screen, so that we only need
     * to calculate the ratio between texture and screen dimensions to get
     * proper coordinates.
     */
    float i_width, i_height;
    i_width = -((float)tex.get_width() / (float)_width) * _aspect;
    i_height = -((float)tex.get_height() / (float)_height);

    float vertices[3*6] = {
        -i_width,-i_height,15.f, i_width, i_height,15.f, -i_width,i_height,15.f,
        -i_width,-i_height,15.f, i_width,-i_height,15.f,  i_width,i_height,15.f
    };

    /*
     * The texture needs a white backdrop to be rendered as-is. Also, the
     * texture coordinates at the triangle corners must be correct. They are,
     * however, static and do not need to be recalculated every time.
     */
    static const float colors[4*6] = {
        1.f,1.f,1.f,1.f, 1.f,1.f,1.f,1.f, 1.f,1.f,1.f,1.f,
        1.f,1.f,1.f,1.f, 1.f,1.f,1.f,1.f, 1.f,1.f,1.f,1.f
    };
    static const float texcoords[2*6] = {
        1.f,0.f, 0.f,1.f, 1.f,1.f,
        1.f,0.f, 0.f,0.f, 0.f,1.f
    };

    /* Bind the ID of the texture to texture sampler #0, and select it */
    _ctx->cgActiveTexture_cgBindTexture_cgTexParameter(0,
        tex.texture_id(_ctx), CGTexture2D::CG_NEAREST,
        CGTexture2D::CG_REPEAT);
    _ctx->cgUniform1i(CG_ULOC_SAMPLER, 0);

    /*
     * Draw the triangles. The fragment shader fetches all pixels from the
     * texture and mixes them into the triangle.
     */
    _ctx->cgVertexAttribPointer(CG_POSITION_ATTRIBUTE, vertices);
    _ctx->cgVertexAttribPointer(CG_COLOR_ATTRIBUTE, colors);
    _ctx->cgVertexAttribPointer(CG_TEXCOORD_ATTRIBUTE, texcoords);
    _ctx->cgDrawArrays(CG_TRIANGLES, 0, 6);
    _ctx->cgVertexAttribPointer(CG_POSITION_ATTRIBUTE, NULL);
    _ctx->cgVertexAttribPointer(CG_COLOR_ATTRIBUTE, NULL);
    _ctx->cgVertexAttribPointer(CG_TEXCOORD_ATTRIBUTE, NULL);

    /* Unbind the texture from texture sampler #0 to avoid side effects */
    _ctx->cgActiveTexture_cgBindTexture_cgTexParameter(0, -1,
        CGTexture2D::CG_NEAREST, CGTexture2D::CG_REPEAT);
}

/* documented in renderer.h */
void GameRenderer::_draw_map_object(const MapObject *o)
{
    /* Import all attributes for rendering from the object */
    _ctx->cgVertexAttribPointer(CG_POSITION_ATTRIBUTE,
        o->get_vertices());
    _ctx->cgVertexAttribPointer(CG_COLOR_ATTRIBUTE,
        o->get_colors());
    _ctx->cgVertexAttribPointer(CG_NORMAL_ATTRIBUTE,
        o->get_normals());
    _ctx->cgVertexAttribPointer(CG_TEXCOORD_ATTRIBUTE,
        o->get_texcoords());

    /* Select texture sampler #0 */
    _ctx->cgUniform1i(CG_ULOC_SAMPLER, 0);

    /*
     * Shortcut to simplify processing. The mesh is not textured, so the
     * attributes are enough to render it.
     */
    if (!o->texture_indices_count() || !o->textures_count())
        _ctx->cgDrawArrays(CG_TRIANGLES, 0, (int)o->vertices_count());

    /*
     * The mesh is textured. The MapUserDefined class, which is responsible
     * handling such meshes, produces a list of ranges. Such a range contains
     * the starting vertex, the number of vertices, and the texture to be
     * assigned to these vertices (suitable for the CG framework).
     * Therefore, we swap out textures in the sampler between ranges and only
     * draw these ranges with the same texture.
     */
    else {
        const int *tex = o->get_textures();
        size_t texcount = o->textures_count();
        const int *texidx = o->get_texture_indices();
        size_t texidxcount = o->texture_indices_count();

        for (size_t i = 0; i < texidxcount; ++i) {
            /*
             * Fetch the texture index. If it's positive, grab the ID returned
             * by CG framework's texture loader. A negative value means that
             * these meshes are not textured, so we need to unbind the texture
             * in this case.
             */
            int texture = texidx[3 * i + 2];
            if (texture >= (int)texcount)
                texture = -1; /* out of bounds */
            else if (texture >= 0)
                texture = tex[texture];

            /*
             * Either bind the texture to, or unbind it from the sampler.
             */
            if (texture >= 0) {
                _ctx->cgActiveTexture_cgBindTexture_cgTexParameter(0, texture,
                    CGTexture2D::CG_NEAREST, CGTexture2D::CG_REPEAT);
            }
            else {
                _ctx->cgActiveTexture_cgBindTexture_cgTexParameter(0, -1,
                    CGTexture2D::CG_NEAREST, CGTexture2D::CG_REPEAT);
            }

            /* Draw the current range using the selected texture */
            int start = texidx[3 * i + 0];
            int count = texidx[3 * i + 1];
            _ctx->cgDrawArrays(CG_TRIANGLES, start, count);
        }
    }

    /*
     * Clear the renderer state, so that the CG renderer doesn't try to perform
     * naughty tricks if we were not to set all attributes before a call.
     */
    _ctx->cgActiveTexture_cgBindTexture_cgTexParameter(0, -1,
        CGTexture2D::CG_NEAREST, CGTexture2D::CG_REPEAT);
    _ctx->cgVertexAttribPointer(CG_POSITION_ATTRIBUTE, NULL);
    _ctx->cgVertexAttribPointer(CG_COLOR_ATTRIBUTE, NULL);
    _ctx->cgVertexAttribPointer(CG_NORMAL_ATTRIBUTE, NULL);
    _ctx->cgVertexAttribPointer(CG_TEXCOORD_ATTRIBUTE, NULL);
}

/* documented in renderer.h */
void GameRenderer::_draw_map(void)
{
    const Map *map = get_active_map();

    /*
     * We're drawing the map. Depending on which mode we have,
     * _active_{projection,camera} point either to their 2D or 3D counterparts.
     */
    _ctx->cgUniformMatrix4fv(CG_ULOC_PROJECTION_MATRIX, 1, false,
        _active_projection->getDataPtr());
    _ctx->cgUniformMatrix4fv(CG_ULOC_MODELVIEW_MATRIX, 1, false,
        _active_camera->getDataPtr());

    /*
     * Generic aspects of the map are hardcoded, like the map mesh (always) and
     * objects (as needed). However, using the Map and MapObject abstract
     * classes, underlying details such as sizes, collision, and look-and-feel
     * can be changed.
     */

    /*
     * Draw the map mesh. Speedup hack: skip the floor in 2D mode, we clear the
     * screen black anyway.
     */
    _draw_map_object(map->get_map_object(Map::OBJECT_MAP));

    /*
     * Draw the walls and the "random" object in the middle of the map.
     * Note: The "random" object is actually static, and no other properties
     * are needed by the game (unlike the walls), so technically we could merge
     * it into the map. But then we cannot really claim that we inserted an
     * obstacle into the map (as demo of working collision). Also, the 2D map
     * *needs* a separate map object for it because it operates on CGQuadrics.
     */
    _draw_map_object(map->get_map_object(Map::OBJECT_WALL_TOP));
    _draw_map_object(map->get_map_object(Map::OBJECT_WALL_BOTTOM));
    _draw_map_object(map->get_map_object(Map::OBJECT_RANDOM_OBJECT));
}

/* documented in renderer.h */
/* inherits modelview/projection setup done by _draw_map() */
void GameRenderer::_draw_objects(void)
{
    const Map *map = get_active_map();
    CGMatrix4x4 mv;

    /*
     * The projection is already set by _draw_map(). We need to calculate
     * our own modelview matrices with the camera.
     */

    /*
     * Both rackets are already at the left (or right) edge of the screen.
     * But we want to move them up and down, so their meshes are placed with
     * the X axis going though their vertical center, and then we apply a
     * translation matrix to move them along the Y axis dynamically.
     */
    mv = (*_active_camera) * CGMatrix4x4::getTranslationMatrix(0.f,
        _game->get_enemy_racket()->y, 0.f);
    _ctx->cgUniformMatrix4fv(CG_ULOC_MODELVIEW_MATRIX, 1, false,
        mv.getDataPtr());
    _draw_map_object(map->get_map_object(Map::OBJECT_RACKET_ENEMY));

    mv = (*_active_camera) * CGMatrix4x4::getTranslationMatrix(0.f,
        _game->get_player_racket()->y, 0.f);
    _ctx->cgUniformMatrix4fv(CG_ULOC_MODELVIEW_MATRIX, 1, false,
        mv.getDataPtr());
    _draw_map_object(map->get_map_object(Map::OBJECT_RACKET_PLAYER));

    for (unsigned int i = 0; i < 1 + GAME_SURPRISE_BALLS; ++i) {
        /*
         * We have 1 + surprise balls in total, but the surprise balls don't
         * spawn all from the beginning. Also, normal gameplay doesn't have
         * them, so make sure they're not rendered in this case.
         */
        const Ball *b = _game->get_ball(i);
        if (!b->active)
            continue;

        /*
         * We want the ball to move along the whole scrren, so its mesh is at
         * the coordinate center - OK, moved up along the Z axis a bit so that
         * it touches the ground - and then we apply a translation matrix to
         * move every rendered ball around the screen.
         */
        mv = (*_active_camera) * CGMatrix4x4::getTranslationMatrix(
            b->posx, b->posy, 0.f);
        _ctx->cgUniformMatrix4fv(CG_ULOC_MODELVIEW_MATRIX, 1, false,
            mv.getDataPtr());
        _draw_map_object(map->get_map_object(Map::OBJECT_BALL));
    }
}

/* documented in renderer.h */
void GameRenderer::_draw_2d_score(void)
{
    /*
     * Prepare 2D shaders and projection to render the score on the screen.
     */
    _ctx->cgUseProgram(projection_modelview_vtx_shader_CG1,
        texture_frag_shader_CG1);
    _ctx->cgUniformMatrix4fv(CG_ULOC_PROJECTION_MATRIX, 1, false,
        _projection_2d.getDataPtr());
    _ctx->cgUseProgram(projection_modelview_vtx_shader_CG1,
        texture_frag_shader_CG1);

    /*
     * Render the score of player 2 (on the left). We call it "enemy" because
     * in single-player mode, it is controlled by the AI, and a friend-foe
     * scheme applies well to Pong.
     */
    unsigned int enemy_score = _game->get_enemy_score();
    unsigned int enemy_score_d1 = enemy_score / 10;
    unsigned int enemy_score_d2 = enemy_score % 10;

    _ctx->cgUniformMatrix4fv(CG_ULOC_MODELVIEW_MATRIX, 1, false,
        _mv_digit_left[0].getDataPtr());
    Digit::render(_ctx, enemy_score < 10 ? enemy_score_d2 : enemy_score_d1);
    if (enemy_score >= 10) {
        _ctx->cgUniformMatrix4fv(CG_ULOC_MODELVIEW_MATRIX, 1, false,
            _mv_digit_left[1].getDataPtr());
        Digit::render(_ctx, enemy_score_d2);
    }

    /*
     * Render the score of player 1 (on the right).
     */
    unsigned int player_score = _game->get_player_score();
    unsigned int player_score_d1 = player_score / 10;
    unsigned int player_score_d2 = player_score % 10;

    _ctx->cgUniformMatrix4fv(CG_ULOC_MODELVIEW_MATRIX, 1, false,
        _mv_digit_right[0].getDataPtr());
    Digit::render(_ctx, player_score_d2);
    if (player_score >= 10) {
        _ctx->cgUniformMatrix4fv(CG_ULOC_MODELVIEW_MATRIX, 1, false,
            _mv_digit_right[1].getDataPtr());
        Digit::render(_ctx, player_score_d1);
    }

    /* safety measure */
    _ctx->cgVertexAttribPointer(CG_POSITION_ATTRIBUTE, NULL);
    _ctx->cgVertexAttribPointer(CG_COLOR_ATTRIBUTE, NULL);
}

/* documented in renderer.h */
void GameRenderer::reset_3d_camera(void)
{
    set_3d_camera(0.f, 0.f, 2.f);
}

/* documented in renderer.h */
void GameRenderer::get_3d_camera(float &phi, float &theta, float &distance) const
{
    phi = _3d.phi;
    theta = _3d.theta;
    distance = _3d.distance;
}

/* documented in renderer.h */
void GameRenderer::set_3d_camera(float phi, float theta, float distance)
{
    /*
     * Phi and Theta are stored in degrees internally (for easy modification).
     */
    _3d.phi = phi;
    _3d.theta = theta;
    _3d.distance = distance;

    /*
     * For the calculation of the camera's modelview, however, we need their
     * values in radians.
     */
    phi = 3.14159265358979323846f * phi / 180.f;
    theta = 3.14159265358979323846f * theta / 180.f;

    /*
     * Convert Phi, Theta and the radius on the sphere to X, Y, Z coordinates.
     * This particular formula is from the Mathematik III lecture. I would like
     * to thank Prof. Arnd Meyer for three wonderful semesters of mathematics
     * lectures with him, which were quite the opposite to my last years in
     * grammar school.
     */
    float x = _3d.distance * cosf(theta) * cosf(phi);
    float y = _3d.distance * cosf(theta) * sinf(phi);
    float z = _3d.distance * sinf(theta);
    _3d.camera = cguLookAt(x, y, z, 0.f, 0.f, 0.f, 0.f, 0.f, 1.f);
}

/* documented in renderer.h */
const Map* GameRenderer::get_active_map(void) const
{
    /*
     * Self-explaining. Except that we cannot use the short ?: form because
     * although _map_3d and _map_2d have the same base class, they are in their
     * corresponding class type (different), which cannot be cast to Map*
     * without making the clause ugly.
     */
    if (_mode3d)
        return _map_3d;
    else
        return _map_2d;
}
