#ifndef CONFIG_H
#define CONFIG_H

/*
 * Modify this file to control various aspects of the Pong game. They are
 * grouped in the following categories:
 *   Presentation
 *   Keyboard
 *   Pong balls
 */

/*
 * Because we're not allowed to use accurate platform-specific timing, please
 * enter the frame rate of your display device in the macro below to make sure
 * that the game plays at the proper speed.
 * (Also, your graphics driver MUST perform vertical synchronization when
 * rendering OpenGL graphics.)
 */
#define GAME_FPS 60.f

/*
 * The game is rendered in the following resolution (width * height pixels).
 * It has only been tested at 1024x576 pixels [which is 16:9 standard-definition
 * TV in Europe and elsewhere].
 */
#define GAME_DEFAULT_WIDTH 1024
#define GAME_DEFAULT_HEIGHT 576

/*
 * File names of external files. The following conditions must apply for
 * these files:
 *   help.tga: Targa image, 512x512 pixels, NOT paletted, rows bottom-to-top.
 *   menu.tga: Targa image, 256x384 pixels, NOT paletted, rows bottom-to-top.
 *   map.bin: Map file created using the map/map_converter.py script (see the
 *   game documentation for more details).
 */
#define GAME_FILE_HELP_TEXTURE "help.tga"
#define GAME_FILE_MENU_TEXTURE "menu.tga"
#define GAME_FILE_3D_MAP "map.bin"

/*
 * Attributes of a light source added in 3D mode. (The light itself is grayscale
 * because it is applied on the meshes' textures and vertex colors.)
 */
#define GAME_LIGHT_POSITION 2.f, 2.f, 2.f
#define GAME_LIGHT_SHININESS 32.f
#define GAME_LIGHT_AMBIENCE .2f
#define GAME_LIGHT_DIFFUSION .5f
#define GAME_LIGHT_SPECULAR .5f

/*
 * Colors of various objects in 2D mode.
 */
#define GAME_2D_COLOR_P1_RACKET 1.f, 1.f, 1.f
#define GAME_2D_COLOR_P2_RACKET 1.f, 1.f, 1.f
#define GAME_2D_COLOR_WALL_TOP 1.f, 1.f, 1.f
#define GAME_2D_COLOR_WALL_BOTTOM 1.f, 1.f, 1.f
#define GAME_2D_COLOR_RANDOM_OBJECT .7412f, .698f, .498f
#define GAME_2D_COLOR_BALL .9f, .9f, .9f
#define GAME_2D_COLOR_FLOOR 0.f, 0.f, 0.f
#define GAME_2D_COLOR_DOT_PATTERN 1.f, 1.f, 1.f

/*
 * Background color of the menu and the 3D map. Default is magenta. It was
 * originally a test color to solve early rendering issues, but continues as
 * background color to this day, as part of the game's overall appearance.
 */
#define GAME_EXPERIENCE_BACKGROUND_COLOR 1.f, 0.f, 1.f

/**************************************/

/*
 * These keys should be self-explaining. If you have modified the keys and want
 * to go back, these are the original assignments:
 * Help: F1
 * Menu: Escape
 * Restart the game: R
 * Start one-player game [in menu]: 1
 * Start two-player game [in menu]: 2
 * Start one-player game with extra balls [in menu]: 4
 * Move the right racket [player 1]: Arrow up/down
 * Move the left racket [player 2]: W (up) and S (down)
 * Toggle between 2D and 3D mode: 3
 * Pan the camera [3D mode]: J (left) and L (right)
 * Tilt the camera [3D mode]: I (up) and K (down)
 * Zoom the camera [3D mode]: U (in) and O (out)
 */
#define GAME_KEY_HELP CG_KEY_F1
#define GAME_KEY_RESTART 'r'
#define GAME_KEY_MENU CG_KEY_ESCAPE
#define GAME_KEY_MENU_1P '1'
#define GAME_KEY_MENU_2P '2'
#define GAME_KEY_MENU_1P_SURPRISE '4'
#define GAME_KEY_P1_UP CG_KEY_UP
#define GAME_KEY_P1_DOWN CG_KEY_DOWN
#define GAME_KEY_P2_UP 'w'
#define GAME_KEY_P2_DOWN 's'
#define GAME_KEY_TOGGLE_xD '3'
#define GAME_KEY_PAN_LEFT 'j'
#define GAME_KEY_PAN_RIGHT 'l'
#define GAME_KEY_TILT_UP 'i'
#define GAME_KEY_TILT_DOWN 'k'
#define GAME_KEY_ZOOM_IN 'u'
#define GAME_KEY_ZOOM_OUT 'o'

/**************************************/

/*
 * This is the distance a racket can move within a second. Of couse the value
 * is too large, because the racket is limited by the walls, but it can move
 * along near the whole Y axis in around one second.
 */
#define GAME_RACKET_MOVE_PER_SEC 2.f

/*
 * The initial speed per second at which the ball is moving. (Interpret this as
 * "the ball moves a distance of (initial speed) per second".)
 */
#define GAME_BALL_INITIAL_SPEED 1.0f

/*
 * If the ball collides with an object which is *not* another ball, we multiply
 * the current speed with the value below to speed it up.
 */
#define GAME_BALL_SPEED_MULTIPLIER 1.02f

/*
 * If the ball is too fast, it can fly out of the scene. This is the maximum
 * speed the ball can reach. (Usually, we cannot really control the ball if it
 * is that fast.
 */
#define GAME_BALL_MAX_SPEED_PER_SEC 2.6f

/*
 * Pressing '4' in the main menu launches a special single-player mode, where
 * [after 3 seconds of gameplay] another ball is added per second. The number
 * below controls the number of balls the game may spawn in this mode.
 */
#define GAME_SURPRISE_BALLS 8

#endif
