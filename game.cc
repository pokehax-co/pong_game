#include <CG1Helper.h>
#include <CGMath.h>
#include <time.h>
#include <float.h>

#include "game.h"
#include "renderer.h"
#include "coll.h"

/*
 * Macro to handle anything-per-second values in a per-frame context.
 */
#define SPEED_PER_FRAME(x) \
    ((x) / GAME_FPS)

/*
 * Constructor of the Game object, which receives a CGContext created in the
 * main() function, and initializes internal game state. The renderer is set
 * up in the same step.
 */
Game::Game(CGContext *context) : _context(context),
    _3d(false), _menu_mode(true), _help_mode(false), _two_players(false),
    _game_started(false), _score_enemy(0), _score_player(0)
{
    /* Renderer also requires game state and viewport size */
    _renderer = new GameRenderer(context, this, GAME_DEFAULT_WIDTH,
        GAME_DEFAULT_HEIGHT);
}

/*
 * Deletes the renderer (allocated by new).
 */
Game::~Game(void)
{
    delete _renderer;
}

/* documented in game.h */
void Game::_program_step(void)
{
    /*
     * Depending on the game state, construct a mask for the renderer telling it
     * which parts of the game to draw.
     */
    unsigned int mask = 0U;
    if (_menu_mode) mask |= GameRenderer::STATE_MENU;
    if (_help_mode) mask |= GameRenderer::STATE_HELP;
    if (_game_started) mask |= GameRenderer::STATE_GAME;
    _renderer->set_state_mask(mask);

    /* Handle key input */
    _process_user_input();

    /* Handle the game's AI and collision. Will be paused if help or menu is
       active. */
    if (!_menu_mode && !_help_mode) {

        /* Of course, if two players are playing, the AI would hinder the second
           player from participating. */
        if (!_two_players)
            _process_ai();

        /* Process the collision of each ball. (There was an OpenMP test, but
           removed because I could trigger a deadlock under random
           circumstances.) */
        for (unsigned int i = 0; i < 1 + GAME_SURPRISE_BALLS; ++i)
            _process_physics(i);

        /*
         * Handle the surprise mode here. Every 3 * 60 frames we spawn a new
         * ball, until all GAME_SURPRISE_BALLS are present.
         */
        if (_surprise_active_balls >= 0 && _surprise_active_balls <
            GAME_SURPRISE_BALLS)
        {
            ++_frame_counter;
            if (_frame_counter % (3 * (int)GAME_FPS) == 0) {
                ++_surprise_active_balls;
                _balls[_surprise_active_balls].active = true;
            }
        }
    }

    /* Now we can draw the scene */
    _renderer->draw_frame();
}

/* documented in game.h */
void Game::_process_user_input(void)
{
    const Map *map = _renderer->get_active_map();

    /* Accept F1 (help) at any time. */
    if (CG1Helper::isKeyReleased(GAME_KEY_HELP)) {
        _help_mode = true;
        return;
    }

    /* Handle Escape key. It exits the help and menu screens, and if neither
       are present, it opens the menu. If no game is running, you cannot leave
       the menu until you have started one. */
    if (CG1Helper::isKeyReleased(GAME_KEY_MENU)) {
        if (_help_mode)
            _help_mode = false;
        else if (_menu_mode && _game_started)
            _menu_mode = false;
        else if (!_menu_mode)
            _menu_mode = true;
    }

    /* Help mode. You can only exit. */
    if (_help_mode)
        return;

    /* Menu mode. '1' to start a single-player game, '2' to start two-player,
       '4' to start a single-player game with spawning balls ("surprise"). */
    if (_menu_mode) {
        if (CG1Helper::isKeyReleased(GAME_KEY_MENU_1P)) {
            _two_players = false;
            _menu_mode = false;
            _surprise_active_balls = -1;
            _reset();
        }
        else if (CG1Helper::isKeyReleased(GAME_KEY_MENU_2P)) {
            _two_players = true;
            _menu_mode = false;
            _surprise_active_balls = -1;
            _reset();
        }
        else if (CG1Helper::isKeyReleased(GAME_KEY_MENU_1P_SURPRISE)) {
            _two_players = false;
            _menu_mode = false;
            _surprise_active_balls = 0;
            _reset();
        }
        return;
    }

    /*
     * Everything else is in-game mode.
     */
    float racket_movement_max =
        1.f - map->hwall_thickness() - map->racket_length() / 2.f;

    /* Move the player's racket */
    if (CG1Helper::isKeyPressed(GAME_KEY_P1_UP)) {
        _player.y += SPEED_PER_FRAME(GAME_RACKET_MOVE_PER_SEC);

        if (_player.y > racket_movement_max)
            _player.y = racket_movement_max;
    }
    if (CG1Helper::isKeyPressed(GAME_KEY_P1_DOWN)) {
        _player.y -= SPEED_PER_FRAME(GAME_RACKET_MOVE_PER_SEC);

        if (_player.y < -racket_movement_max)
            _player.y = -racket_movement_max;
    }

    /* Move the enemy's racket (two-player mode only) */
    if (_two_players) {
        if (CG1Helper::isKeyPressed(GAME_KEY_P2_UP)) {
            _enemy.y += SPEED_PER_FRAME(GAME_RACKET_MOVE_PER_SEC);

            if (_enemy.y > racket_movement_max)
                _enemy.y = racket_movement_max;
        }
        if (CG1Helper::isKeyPressed(GAME_KEY_P2_DOWN)) {
            _enemy.y -= SPEED_PER_FRAME(GAME_RACKET_MOVE_PER_SEC);

            if (_enemy.y < -racket_movement_max)
                _enemy.y = -racket_movement_max;
        }
    }

    /* Toggle between 3D and 2D mode */
    if (CG1Helper::isKeyReleased(GAME_KEY_TOGGLE_xD)) {
        _3d ^= true;
        fprintf(stderr, "Switching to %uD mode.\n", _3d + 2);
        _renderer->set_mode(_3d);
    }

    /*
     * Additional keys available to control the camera in 3D mode.
     * J and L = pan
     * I and K = tilt
     * U and O = zoom
     */
    float phi, theta, distance;
    if (CG1Helper::isKeyPressed(GAME_KEY_PAN_LEFT)) {
        _renderer->get_3d_camera(phi, theta, distance);
        phi -= 5.f;
        if (phi < 0.f)
            phi += 360.f;
        _renderer->set_3d_camera(phi, theta, distance);
    }
    if (CG1Helper::isKeyPressed(GAME_KEY_PAN_RIGHT)) {
        _renderer->get_3d_camera(phi, theta, distance);
        phi += 5.f;
        if (phi > 359.f)
            phi -= 360.f;
        _renderer->set_3d_camera(phi, theta, distance);
    }

    if (CG1Helper::isKeyPressed(GAME_KEY_TILT_DOWN)) {
        _renderer->get_3d_camera(phi, theta, distance);
        theta -= 5.f;
        if (theta < -90.f)
            theta = -90.f;
        _renderer->set_3d_camera(phi, theta, distance);
    }
    if (CG1Helper::isKeyPressed(GAME_KEY_TILT_UP)) {
        _renderer->get_3d_camera(phi, theta, distance);
        theta += 5.f;
        if (theta > 90.f)
            theta = 90.f;
        _renderer->set_3d_camera(phi, theta, distance);
    }

    if (CG1Helper::isKeyPressed(GAME_KEY_ZOOM_IN)) {
        _renderer->get_3d_camera(phi, theta, distance);
        distance -= .1f;
        _renderer->set_3d_camera(phi, theta, distance);
    }
    if (CG1Helper::isKeyPressed(GAME_KEY_ZOOM_OUT)) {
        _renderer->get_3d_camera(phi, theta, distance);
        distance += .1f;
        _renderer->set_3d_camera(phi, theta, distance);
    }

    /* Restart the game */
    if (CG1Helper::isKeyReleased(GAME_KEY_RESTART))
        _reset();
}

/*
 * Documented in game.h
 * There are far better intelligence algorithms than this one. Simple google-fu
 * leads to good suggestions what player 2 could do when encountering a ball.
 */
void Game::_process_ai(void)
{
    const Map *map = _renderer->get_active_map();
    Ball &playerball = _balls[0];

    /*
     * Extract information on the field and the objects on it.
     */
    float x, y;
    CGVec4 area_bound;
    float racket_length = map->racket_length() / 2.f;
    float maxval = 1.f - map->hwall_thickness() - racket_length;

    /*
     * We have the ball's position and its velocity. It basically moves in a
     * line which can be represented using the equation
     *   ballposition + x * ballvelocity = left edge
     * We want to solve x (which must be positive). Then we can assume that the
     * ball is moving towards the left edge (where player 2 is located at).
     *
     * But then, it might just hit the wall (if left edge Y < -1 or > +1). Then
     * let it hit the wall without reaction.
     *
     * Finally, player 2 is handicapped in two ways: 1. It cannot stop moving
     * (this is an arbitrary rule imposed by the programmer). 2. There is a
     * 5% chance that the AI ignores the incoming ball, allowing the player to
     * win.
     *
     * Only after passing all these conditions, player 2 will rush to the
     * estimated Y position of the incoming ball.
     */

    /*
     * Solve the equation from above. Not solvable if the ball moves parallel
     * to the Y axis.
     */
#if defined(_MSC_VER) && _MSC_VER < 1900
    /* Old MSVCs don't have C99 compliant math */
    if (_isnan(1.f / playerball.vecx) || !_finite(1.f / playerball.vecx))
#else
    if (isnan(1.f / playerball.vecx) || isinf(1.f / playerball.vecx))
#endif
        goto fallback;

    map->playfield_boundaries(area_bound);
    x = (area_bound[MAP_BOUND_XMIN] - playerball.posx) / playerball.vecx;
    if (x < 0.f)
        goto fallback;

    /*
     * At which y do we land? Is it outside the bounds? Then it will hit the
     * wall. No need to follow.
     */
    y = playerball.posy + x * playerball.vecy;
    if (y < area_bound[MAP_BOUND_YMIN] || y > area_bound[MAP_BOUND_YMAX])
        goto fallback;

    /*
     * Now there's a five percent chance that player 2 might sleep.
     */
    if (rand() % 20 == 13)
        goto fallback;

    /*
     * Rush to the ball. If we're near enough, just move around because the
     * programmer does not allow player 2 to stop moving.
     */
    static bool last_direction = true;
    if (playerball.posy > _enemy.y + racket_length) {
        _enemy.y += SPEED_PER_FRAME(GAME_RACKET_MOVE_PER_SEC);
        last_direction = true;
    } else if (playerball.posy < _enemy.y - racket_length) {
        _enemy.y -= SPEED_PER_FRAME(GAME_RACKET_MOVE_PER_SEC);
        last_direction = false;
    } else {
        _enemy.y += (last_direction ? 1.f : -1.f) *
            SPEED_PER_FRAME(GAME_RACKET_MOVE_PER_SEC);
    }

    /*
     * Clip the Y coordinate of player 2's racket to its allowed range. (We
     * also do this during key input handling, so that's fine.)
     */
    if (_enemy.y > maxval)
        _enemy.y = maxval;
    else if (_enemy.y < -maxval)
        _enemy.y = -maxval;
    return;

fallback:
    /*
     * Fallback algorithm just moves the racket around, like those progress bars
     * with indeterminate progress [https://i.imgur.com/QdHB3Id.gif].
     */
    static bool move_up = true;
    if (move_up) {
        _enemy.y += SPEED_PER_FRAME(GAME_RACKET_MOVE_PER_SEC);
        if (_enemy.y > maxval) {
            _enemy.y = maxval;
            move_up = false;
        }
    } else {
        _enemy.y -= SPEED_PER_FRAME(GAME_RACKET_MOVE_PER_SEC);
        if (_enemy.y < -maxval) {
            _enemy.y = -maxval;
            move_up = true;
        }
    }
}

/* documented in game.h */
void Game::_process_physics(unsigned int ball)
{
    Ball &_ball = _balls[ball];

    /* If the ball is not visible, don't process anything (funny side effects
       if we would do - DO NOT BE FUNNY). */
    if (!_ball.active)
        return;

    /* Grab the map's boundaries */
    const Map *map = _renderer->get_active_map();
    CGVec4 area_bound;
    map->playfield_boundaries(area_bound);

    /*
     * Ball ID 0 is the actual gameplay ball.
     */
    if (ball == 0) {
        if (_ball.posx < area_bound[MAP_BOUND_XMIN] ||
            _ball.posx > area_bound[MAP_BOUND_XMAX])
        {
            /*
             * Weee! The ball flew outside the field. Either player 1 or 2 has
             * won this round.
             * However, we can't display more than two digits. If either of the
             * players has accumulated 100 points, we want to be an asshole and
             * erase the player's progress.
             * (This is not a new concept. Earthbound/Mother 2 on the Super
             * Famicom did this if you pirated the game.)
             */
            bool place_at_enemy;
            if (_ball.posx > 0.f) {
                _score_enemy += 1;
                place_at_enemy = true;
            }
            else {
                _score_player += 1;
                place_at_enemy = false;
            }

            /* initiate asshole mode */
            if (_score_enemy >= 100 || _score_player >= 100) {
                _score_enemy = _score_player = 0;
                _reset();
            }

            /* next round */
            else {
                if (_surprise_active_balls >= 0)
                    _surprise_active_balls = 0;
                _frame_counter = 0;
                _init_ball(place_at_enemy);
            }
            return;
        }
    }

    /*
     * One of the "surprise" balls. Because we don't have walls left and right,
     * they would fall out of the visible screen. This is not good, we want to
     * keep them within the field. So let's perform some manual collision
     * detection and response (with vector reflection).
     */
    else {
        bool collide = false;
        CGVec4 s1;

        /* Determine the normal of an "invisible wall" along the edges */
        if (_ball.posx < area_bound[MAP_BOUND_XMIN]) {
            // normal: 1,0,0
            s1.set(1.f, 0.f, 0.f, 0.f);
            collide = true;
        }
        else if (_ball.posx > area_bound[MAP_BOUND_XMAX]) {
            // normal: -1,0,0
            s1.set(-1.f, 0.f, 0.f, 0.f);
            collide = true;
        }

        /* Perform the reflection of the ball's velocity (new velocity should
           still be normalized) */
        if (collide) {
            CGVec4 vel(_ball.vecx, _ball.vecy, 0.f, 0.f);
            float scale = 2.f * CGMath::dot(vel, s1);
            CGVec4 vel_new = vel - scale * s1;
            _ball.vecx = vel_new[0];
            _ball.vecy = vel_new[1];
        }
    }

    /*
     * Enter the specifications of the ball into the object o.
     * o_translate_y is necessary to detect collisions with the rackets, which
     * are not at a fixed place.
     */
    CollisionObject o;
    float o_translate_y = 0.f;
    o.set_position(_ball.posx, _ball.posy, 0.f);
    o.set_velocity(_ball.speed * _ball.vecx, _ball.speed * _ball.vecy, 0.f);
    o.set_radius(map->ball_radius(), map->ball_radius(), map->ball_radius());

    /*
     * Check collisions between the current ball o and every other (not yet
     * checked) ball o1.
     */
    CollisionObject o1;
    for (unsigned int i = ball + 1; i < 1 + GAME_SURPRISE_BALLS; ++i) {
        if (!_balls[i].active)
            continue;

        /*
         * Enter the specifications of the other ball.
         */
        o1.set_position(_balls[i].posx, _balls[i].posy, 0.f);
        o1.set_velocity(_balls[i].speed * _balls[i].vecx, _balls[i].speed *
            _balls[i].vecy, 0.f);
        o1.set_radius(map->ball_radius(), map->ball_radius(),
            map->ball_radius());

        /*
         * Perform a collision detection between two spheres. See coll.cc for
         * implementation details.
         */
        CollisionDetect::SweepInfo s1, s2;
        if (CollisionDetect::detect(o, o1, s1, s2)) {
            float f;

            /*
             * The function returns the position of the balls at the time of
             * their collision, as well as new (not normalized) velocity
             * vectors which push the balls away from each other.
             */
            _ball.posx = s1.pos[0];
            _ball.posy = s1.pos[1];
            f = sqrtf(s1.vel[0] * s1.vel[0] + s1.vel[1] * s1.vel[1]);
            _ball.vecx = s1.vel[0] / f;
            _ball.vecy = s1.vel[1] / f;

            _balls[i].posx = s2.pos[0];
            _balls[i].posy = s2.pos[1];
            f = sqrtf(s2.vel[0] * s2.vel[0] + s2.vel[1] * s2.vel[1]);
            _balls[i].vecx = s2.vel[0] / f;
            _balls[i].vecy = s2.vel[1] / f;
        }
    }

    /*
     * Reduce the number of objects to be checked, based on the ball's
     * coordinates, so that we check no more than 3 objects.
     * Improves performance. A real-world game engine uses quad trees or so
     * to manage colliding objects.
     */
    Map::Object objs[3];
    objs[0] = Map::OBJECT_RANDOM_OBJECT;
    objs[1] = _ball.posx < 0.f ? Map::OBJECT_RACKET_ENEMY :
        Map::OBJECT_RACKET_PLAYER;
    objs[2] = _ball.posy < 0.f ? Map::OBJECT_WALL_BOTTOM :
        Map::OBJECT_WALL_TOP;

    CollisionDetect::CollisionInfo i;
    unsigned int oth;
    float matching_tri[9] = {0.f};

    /*
     * We use a swept-sphere collision detection to detect collision between
     * the ball (which is a sphere) and a triangle mesh.
     * See coll.cc and coll.h for details.
     */
    for (oth = 0; oth < sizeof(objs)/sizeof(objs[0]); ++oth)
    {
        const MapObject *object =
            _renderer->get_active_map()->get_map_object(objs[oth]);
        size_t vtxcnt = object->collision_vertices_count();
        const float *vtx = object->get_collision_vertices();

        /*
         * Apply different translations to our collision object.
         * The primary reason is that some movable objects have fixed vertices,
         * and are "moved" at the vertex shader stage using translation
         * matrices.
         * Therefore, we want to apply the "inverse" of the object's translation
         * to the ball in order to perform the collision check.
         */
        switch (objs[oth]) {
            case Map::OBJECT_RACKET_ENEMY:
                o_translate_y = _enemy.y;
                break;
            case Map::OBJECT_RACKET_PLAYER:
                o_translate_y = _player.y;
                break;
            default:
                o_translate_y = 0.f;
                break;
        }
        o.set_translation(0.f, -o_translate_y, 0.f);

        /*
         * Now check all triangles of the object. If the current triangle (only)
         * yields a collision (and is closer than a previous one), copy the
         * three vertices which make up that triangle so we can operate on them
         * during the response.
         */
        i.collision = false;
        i.time = 1.f;
        for (size_t j = 0; j < 3*vtxcnt; j += 9) {
            bool old_collision = i.collision;
            float old_time = i.time;

            if (CollisionDetect::detect(o, i, &vtx[j])) {
                if (!old_collision || i.time <= old_time)
                    memcpy(matching_tri, &vtx[j], 9 * sizeof(float));
            }
        }

        /*
         * We need to check every triangle to find the nearest occurrence.
         * But after that, if the ball collides with any of that object's
         * triangles, then we can leave early.
         */
        if (i.collision)
            break;
    }

    /*
     * No collision with other objects. The ball can take its original path
     * without consequences.
     */
    if (!i.collision) {
        _ball.posx = _ball.speed * _ball.vecx + _ball.posx;
        _ball.posy = _ball.speed * _ball.vecy + _ball.posy;
    }
    else {
        /*
         * Sweep the object so that it barely touches the object we have
         * collided.
         * Note: We only perform one pass of the sweep collision response.
         * For a second and third pass, we need to perform the collision
         * detection for a second time (with the ball's new movement) to cover
         * situations in which the ball is stuck between different triangles.
         * Our maps are designed so that it does not happen.
         */
        CollisionDetect::SweepInfo sweep;
        CollisionDetect::sweep_pass1(o, i, sweep);
        CollisionDetect::sweep_done(o, sweep);

        /*
         * Calculate the normal of the triangle the ball has collided with.
         * We will use that as the vector across which we reflect the ball's
         * velocity.
         */
        CGVec4 p1(matching_tri[0], matching_tri[1], matching_tri[2], 0.f);
        CGVec4 p2(matching_tri[3], matching_tri[4], matching_tri[5], 0.f);
        CGVec4 p3(matching_tri[6], matching_tri[7], matching_tri[8], 0.f);
        CGVec4 s1 = p2 - p1;
        CGVec4 s2 = p3 - p1;
        s1 = CGMath::cross(s1, s2);
        s1 = CGMath::normalize(s1);

        /*
         * A creative user can design maps for the 3D mode with triangles whose
         * normals are not parallel to the Z axis. This would cause the velocity
         * to take higher values so that the ball unexpectedly moves faster
         * after the collision.
         * Mitigate this by projecting the triangle's normal vector to the
         * X-Y plane.
         */
        CGVec4 xyplane(0.f, 0.f, 1.f, 0.f);
        float proj_scale = CGMath::dot(s1, xyplane) /
            CGMath::dot(xyplane, xyplane);
        s1 = s1 - proj_scale * xyplane;
        s1 = CGMath::normalize(s1);

        /* holds the new velocity vector of the ball */
        CGVec4 vel_new;

        /*
         * If the ball collides the racket, we want the ball to take an unusual
         * route, depending on the position where the two objects collide.
         * Imagine the following paragraph with the racket's center at Y = 0.
         *
         * What we want is the following: if we hit the racket right at the
         * center, we bounce it off in the same direction as the racket's front-
         * facing normal. If we hit the racket near its borders, though, we want
         * the ball to bounce off at a steep angle (like 60° or something).
         *
         * For this special case, we need to hit either of the rackets at its
         * front face (check the normal: (-1,0,0) for the player's, (1,0,0) for
         * the enemy's). Otherwise, we use the generic reflection method.
         */
        if ((objs[oth] == Map::OBJECT_RACKET_PLAYER ||
            objs[oth] == Map::OBJECT_RACKET_ENEMY) && (s1[0] < -.95f ||
            s1[0] > .95f))
        {
            /* [-pl()/2, pl()/2] => [-pi/3, pi/3] (equiv. [-60°, 60°] */
            float pos2angle_transform = (3.14159265358979323846f * (2.f/3.f)) /
                map->racket_length();
            vel_new[0] = cosf(pos2angle_transform * sweep.pos[1]);
            vel_new[1] = sinf(pos2angle_transform * sweep.pos[1]);
            vel_new[2] = 0.f;
            vel_new[3] = 0.f;
            if (objs[oth] == Map::OBJECT_RACKET_PLAYER)
                vel_new[0] *= -1.f;
        }

        /*
         * Reflect our velocity vector across the triangle's normal s1.
         * The formula is:
         *           v dot l
         *  v - 2 * --------- * l
         *           l dot l
         * where v is the vector be reflected, l is the vector which v is
         * eing reflected across. We can simplify because l is of unit length
         * (||l|| = 1).
         * Afterwards, we need our velocity to be normed as well.
         */
        else {
            CGVec4 vel(_ball.vecx, _ball.vecy, 0.f, 0.f);
            float scale = 2.f * CGMath::dot(vel, s1);
            vel_new = vel - scale * s1;
        }

        /*
         * Apply the calculated information to the ball.
         */
        vel_new = CGMath::normalize(vel_new);
        _ball.posx = sweep.pos[0];
        _ball.posy = sweep.pos[1] + o_translate_y;
        _ball.vecx = vel_new[0];
        _ball.vecy = vel_new[1];

        /*
         * While we do increase the speed, we do so until we reach a certain
         * threshold (2.6 units per second). This is necessary because the
         * collision detection will fail at too high speeds, and balls fly
         * out of the field.
         */
        if (_ball.speed < SPEED_PER_FRAME(GAME_BALL_MAX_SPEED_PER_SEC))
            _ball.speed *= GAME_BALL_SPEED_MULTIPLIER;
    }
}

/* documented in game.h */
void Game::_init_ball(bool left_side)
{
    for (unsigned int i = 0; i < 1 + GAME_SURPRISE_BALLS; ++i)
        _init_ball(left_side, i);
}

/* documented in game.h */
void Game::_init_ball(bool left_side, unsigned int ball)
{
    const Map *map = _renderer->get_active_map();
    Ball &_ball = _balls[ball];

    /* grab play field dimensions */
    CGVec4 field;
    map->playfield_boundaries(field);

    /*
     * We're placing the ball used for gameplay.
     */
    if (ball == 0) {
        /*
         * The gameplay ball is placed next to the winning racket's center.
         */
        _ball.posx = field[MAP_BOUND_XMAX] - map->racket_thickness() -
            map->ball_radius();
        if (left_side)
            _ball.posx = -_ball.posx;
        _ball.posy = left_side ? _enemy.y : _player.y;

        /*
         * The ball will aim at the center of the opposite racket.
         */
        _ball.vecx = 1.f - map->racket_thickness();
        if (!left_side)
            _ball.vecx = -_ball.vecx;
        _ball.vecx -= _ball.posx;
        _ball.vecy = (left_side ? _player.y : _enemy.y) - _ball.posy;
        _ball.active = true;
    }

    /*
     * We're placing the surprise balls. Spawn the ball at either of four
     * points, with the velocity pointing to the middle of the field.
     */
    else {
        unsigned int seed = rand() & 3;
        switch (seed) {
            case 0:
                _ball.posx = -.5f; _ball.posy = .5f;
                _ball.vecx = 1.f; _ball.vecy = -1.f;
                break;
            case 1:
                _ball.posx = .5f; _ball.posy = .5f;
                _ball.vecx = -1.f; _ball.vecy = -1.f;
                break;
            case 2:
                _ball.posx = .5f; _ball.posy = -.5f;
                _ball.vecx = -1.f; _ball.vecy = 1.f;
                break;
            case 3:
                _ball.posx = -.5f; _ball.posy = -.5f;
                _ball.vecx = 1.f; _ball.vecy = 1.f;
                break;
        }
        _ball.active = false;
    }

    /* Make sure the velocity is always a unit */
    float n = sqrtf(_ball.vecx*_ball.vecx + _ball.vecy*_ball.vecy);
    _ball.vecx /= n;
    _ball.vecy /= n;

    /* initial speed, will be increased by some percent on each hit */
    _ball.speed = SPEED_PER_FRAME(GAME_BALL_INITIAL_SPEED);

    /* first start! */
    _game_started = true;
}

/* documented in game.h */
void Game::_reset(void)
{
    /* Reset surprise balls, but not if they're active. */
    if (_surprise_active_balls >= 0)
        _surprise_active_balls = 0;
    _frame_counter = 0;

    /* Reset scores and racket positions. Player 1 starts the game. */
    _score_enemy = _score_player = 0;
    _player.y = _enemy.y = 0.f;
    _init_ball(false);
}

/******************************************************************************/

/*
 * Provide C++ operators new and delete to the MinGW-w64 cross compiler.
 * We got rid of C++ exception, RTTI, standard library already.
 */
#ifdef __MINGW32__
void* operator new(size_t x)
{
    return malloc(x);
}
void* operator new[](size_t x)
{
    return malloc(x);
}
void operator delete(void *p)
{
    free(p);
}
void operator delete[](void *p)
{
    free(p);
}
extern "C" void __cxa_pure_virtual(void)
{
    fprintf(stderr, "pure virtual method called\n");
    abort();
}
#endif

/*
 * First function called after application launch.
 * The srand() call was inherited from CG1Application.cpp, but we use it for
 * random ball spawns now.
 *
 * We let CG1Helper create a context with the specified viewport size for us.
 * Then we feed it to the game object, to initialize Pong.
 * Finally, we hook its program step routine into CG1Helper and start the game.
 */
int main(int argc, char **argv)
{
    srand((unsigned int)time(0));

    CGContext *context;
    if (!CG1Helper::initApplication(context, GAME_DEFAULT_WIDTH,
        GAME_DEFAULT_HEIGHT, 1))
    {
        fprintf(stderr, "Failed to initialize CG1Helper\n");
        return 1;
    }

    Game game(context);
    CG1Helper::setProgramStep(Game::program_step_cb, &game);
    CG1Helper::runApplication();
    return 0;
}
