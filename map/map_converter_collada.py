#!/usr/bin/python2
#
#   Copyright (c) 2015-2016 Thomas Weber
#
#   Permission is hereby granted, free of charge, to any person obtaining
#   a copy of this software and associated documentation files (the
#   "Software"), to deal in the Software without restriction, including
#   without limitation the rights to use, copy, modify, merge, publish,
#   distribute, sublicense, and/or sell copies of the Software, and to
#   permit persons to whom the Software is furnished to do so, subject to
#   the following conditions:
#
#   The above copyright notice and this permission notice shall be included
#   in all copies or substantial portions of the Software.
#
#   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
#   EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
#   MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
#   IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
#   CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
#   TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
#   SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#

#
# This Python script converts several Collada (.dae) meshes into a single
# map.bin file, which is rendered by the Pong game in 3D mode.
# You need ImageMagick in order to convert textures from common image formats
# such as JPEG and PNG.
# You need the pycollada library to read Collada files.
#
# In particular, the script expects seven nodes with the following identifiers
# from the specified Collada file:
# map
# wall_top
# wall_bottom
# paddle_enemy
# paddle_player
# random_object
# ball
#
# The resulting map.bin file is then copied to the same directory as the game
# executable (where you can also find menu.tga and help.tga).
#
# Collada files can be created using Sketchup Pro (not the free version!)
# or Blender. Other modelling tools might work, but I cannot verify
# compatibility with such programs.
# You can use the .dae or the .blend file as a starting point for your own
# designs, so that the sizes stay correct.
#
# When exporting a Blender file into Collada, the following options MUST be
# checked:
#  - Collada Options -> Triangulate
#  - Collada Options -> Use Object Instances
#  - Texture Options -> Include Material Textures
#

import struct, collada, subprocess, os
from sys import exit, argv

def clamp(f, min_value, max_value):
    return max(min(f, max_value), min_value)

################################################################################

if len(argv) < 2:
    print("Usage: %s dae-file" % argv[0])
    exit(0)
filename = argv[1]

# read test collada file
source = collada.Collada(filename)
mapf = None
wall_top = None
wall_bottom = None
paddle_enemy = None
paddle_player = None
random_object = None
ball = None

for x in source.scene.nodes:
    if x.id == "map":
        mapf = x
    elif x.id == "wall_top":
        wall_top = x
    elif x.id == "wall_bottom":
        wall_bottom = x
    elif x.id == "paddle_enemy":
        paddle_enemy = x
    elif x.id == "paddle_player":
        paddle_player = x
    elif x.id == "random_object":
        random_object = x
    elif x.id == "ball":
        ball = x

if mapf is None:
    print("missing node 'map' in " + filename)
    exit(1)
if wall_top is None:
    print("missing node 'wall_top' in " + filename)
    exit(1)
if wall_bottom is None:
    print("missing node 'wall_bottom' in " + filename)
    exit(1)
if paddle_enemy is None:
    print("missing node 'paddle_enemy' in " + filename)
    exit(1)
if paddle_player is None:
    print("missing node 'paddle_player' in " + filename)
    exit(1)
if random_object is None:
    print("missing node 'random_object' in " + filename)
    exit(1)
if ball is None:
    print("missing node 'ball' in " + filename)
    exit(1)

# write binary data
out = open("map.bin", "wb")
offsets = [0, 0, 0, 0, 0, 0, 0]

out.write(struct.pack("<iiiiiii", 0,0,0,0,0,0,0)) # for now

oi = 0
for imp in (mapf, wall_top, wall_bottom, paddle_enemy, paddle_player, random_object, ball):

    #
    # Collect the polygons of the current object
    #
    polys = []
    dae_geometry = imp.objects('geometry')
    for g in dae_geometry:
        for p in g.primitives():
            for poly in p:
                polys.append(poly)

    #
    # Collect the texture IDs of the current object
    #
    texids = []
    for poly in polys:
        e = poly.material.effect
        if type(e.diffuse) == collada.material.Map:
            texid = e.diffuse.sampler.surface.image.id
            if not texid in texids:
                texids.append(texid)

    #
    # header of single object:
    # 32-bit vertices/normals/texcoords/colors count
    # 32-bit offset to vertices
    # 32-bit offset to normals
    # 32-bit offset to texcoords
    # 32-bit offset to colors
    # 32-bit offset to texture indices
    # 32-bit offset to texture data
    #

    # write dummy header (we will come back soon!)
    offsets[oi] = header_offset = out.tell()
    oi += 1
    out.write(struct.pack("<iiiiiii", 0,0,0,0,0,0,0)) # for now

    #
    # write vertices:
    # 3 floats make up one vertex
    #
    # FIXME!
    vertices_count = 0
    vertices_offset = out.tell()
    print("%x" % vertices_offset)
    for poly in polys:
        vertices_count += len(poly.vertices)
        for i in poly.vertices:
            out.write(struct.pack("<fff", i[0], i[1], i[2]))
    print("count %u" % vertices_count)

    #
    # write normals:
    # 4 floats make up one normal (last float ignored)
    #
    normals_offset = out.tell()
    print("%x" % normals_offset)
    for poly in polys:
        if poly.normals is None or len(poly.normals) == 0:
            # insert dummy data
            out.write(len(poly.vertices) * struct.pack("<ffff", 0.0, 0.0, 0.0, 0.0))
        else:
            for i in poly.normals:
                out.write(struct.pack("<ffff", i[0], i[1], i[2], 0.0))

    #
    # write texcoords:
    # 2 floats make up one texcoord
    #
    texcoords_offset = out.tell()
    print("%x" % texcoords_offset)
    for poly in polys:
        if poly.texcoords is None or len(poly.texcoords) == 0:
            # insert dummy data
            out.write(len(poly.vertices) * struct.pack("<ff", 0.0, 0.0))
        else:
            # FIXME what if there are more numpy arrays? Works for triangulated
            # Blender export at least.
            for i in poly.texcoords[0]:
                out.write(struct.pack("<ff", i[0], i[1]))

    #
    # write colors:
    # 4 floats make up one color
    #
    colors_offset = out.tell()
    print("%x" % colors_offset)

    dae_geometry = imp.objects('geometry')
    for g in dae_geometry:
        for p in g.primitives():
            #
            # Extract COLOR inputs
            #
            poly_count = len(p)
            colors_ref = p.original.sources.get('COLOR', None)
            if colors_ref is None or len(colors_ref) == 0:
                # write white
                out.write(poly_count * 3 * struct.pack("<ffff", 1.0, 1.0, 1.0, 1.0))
                continue

            #
            # Extract the color using the index array
            # FIXME what if there are more numpy arrays? Works for triangulated
            # Blender export at least.
            #
            for x in colors_ref:
                color_index_offset = x[0]
                for y in p.original.index:
                    color = x[4].data[y[color_index_offset]]
                    out.write(struct.pack("<ffff", color[0], color[1], color[2], 1.0))

    #
    # write indices:
    # one 32-bit signed integer index for each triangle
    #
    texindex_offset = out.tell()
    print("%x" % texindex_offset)

    # FIXME only tested with polys with texture in diffuse
    for poly in polys:
        e = poly.material.effect
        if type(e.diffuse) != collada.material.Map:
            out.write(struct.pack("<i", -1)) # no texture used
            continue
        texture = e.diffuse.sampler.surface.image
        if not texture.id in texids:
            print("warning: material %s not found" % texture.id)
            out.write(struct.pack("<i", -1)) # no texture used
        else:
            idx = texids.index(texture.id)
            out.write(struct.pack("<i", idx))

    #
    # write texture data:
    # number of textures
    # [per-texture]
    # size of targa image data [0 if material does not have any texture attached]
    # targa image
    #
    texdata_offset = out.tell()
    out.write(struct.pack("<i", len(texids)))

    for x in source.images:
        if not x.id in texids:
            continue

        #
        # Convert to Targa format using ImageMagick.
        # We need to flip the image vertically because the CG renderer assumes
        # bottom-to-top row order.
        #
        # Requires minimum version 6.8.9-2 for RLE compression support.
        # Alpha premultiplication command line from:
        # http://imagemagick.org/discourse-server/viewtopic.php?t=23463
        #
        if subprocess.call(['convert', x.path, '-alpha', 'set',
            '(', '+clone', '-alpha', 'Extract', ')', '-channel', 'RGB',
            '-compose', 'Multiply', '-composite', '-flip',
            '-compress', 'RLE', '__temp__.tga']) != 0:
            print("Conversion of texture " + x.path + " failed.")
            exit(1)

        f = open("__temp__.tga", "rb")
        filesize = os.path.getsize("__temp__.tga")
        out.write(struct.pack("<i", filesize))
        out.write(f.read())

        # round up to multiple of 4 (performance)
        if filesize % 4 == 1:
            out.write(struct.pack("<BH",0,0))
        elif filesize % 4 == 2:
            out.write(struct.pack("<H",0))
        elif filesize % 4 == 3:
            out.write(struct.pack("<B",0))
        f.close()
        #os.remove("__temp__.tga")

    out.seek(header_offset)
    out.write(struct.pack("<iiiiiii", vertices_count, vertices_offset,
        normals_offset, texcoords_offset, colors_offset, texindex_offset,
        texdata_offset))
    print("-----------------------")
    out.seek(0, 2) # back to end

out.seek(0, 0)
out.write(struct.pack("<iiiiiii", offsets[0], offsets[1], offsets[2], offsets[3], offsets[4], offsets[5], offsets[6]))

out.close()
