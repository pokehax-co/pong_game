#!/usr/bin/python2
#
#   Copyright (c) 2015-2016 Thomas Weber
#
#   Permission is hereby granted, free of charge, to any person obtaining
#   a copy of this software and associated documentation files (the
#   "Software"), to deal in the Software without restriction, including
#   without limitation the rights to use, copy, modify, merge, publish,
#   distribute, sublicense, and/or sell copies of the Software, and to
#   permit persons to whom the Software is furnished to do so, subject to
#   the following conditions:
#
#   The above copyright notice and this permission notice shall be included
#   in all copies or substantial portions of the Software.
#
#   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
#   EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
#   MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
#   IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
#   CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
#   TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
#   SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#

#
# This Python script converts several Wavefront .obj meshes into a single
# map.bin file, which is rendered by the Pong game in 3D mode.
# You need ImageMagick in order to convert textures from common image formats
# such as JPEG and PNG.
#
# In particular, the script expects the following files to be present in the
# directory where you are invoking the script from:
# map.obj
# wall_top.obj
# wall_bottom.obj
# paddle_enemy.obj
# paddle_player.obj
# random_object.obj
# ball.obj
#
# The resulting map.bin file is then copied to the same directory as the game
# executable (where you can also find menu.tga and help.tga).
#
# Wavefront .obj files can be created using Sketchup Pro (not the free version!)
# or Blender. Other modelling tools might work, but I cannot verify
# compatibility with such programs.
#
# Make sure that all faces are exported as triangulars! Quads or other forms
# of meshes are NOT supported.
#

import struct, subprocess, os

def clamp(f, min_value, max_value):
    return max(min(f, max_value), min_value)

class ObjMaterial(object):
    def __init__(self, name):
        self.name = name
        self.ambient = (0.0, 0.0, 0.0)
        self.diffuse = (0.0, 0.0, 0.0)
        self.diffuse_texture = None
        self.specular = (0.0, 0.0, 0.0)
        self.specular_exponent = 0.0

class ObjGroup(object):
    def __init__(self, name):
        self.name = name
        self.vertices = []
        self.texcoords = []
        self.normals = []
        self.triangles = [] # bound to active_material
        self.active_material = [] # bound to triangles

class ObjImporter(object):
    def __init__(self):
        self.materials = []
        self.material_lib = None
        self.groups = []
        self.current_material = None

    def _check_material(self):
        if len(self.materials) == 0:
            g = ObjMaterial("unnamed")
            self.materials.append(g)
        return len(self.materials) - 1

    def _check_group(self):
        if len(self.groups) == 0:
            g = ObjGroup("unnamed")
            self.groups.append(g)
        return len(self.groups) - 1

    def read_material(self, filename):
        with open(filename, "r") as f:
            for line in f:
                _line = line.splitlines()[0].strip()
                # comment
                if len(_line) == 0 or _line[0] == '#':
                    continue
                tokens = _line.split(" ")
                # new material
                if tokens[0] == "newmtl":
                    self.materials.append(ObjMaterial(tokens[1]))
                elif tokens[0] == "map_Kd":
                    i = self._check_material()
                    self.materials[i].diffuse_texture = " ".join(tokens[1:])
                elif tokens[0] == "Ka":
                    i = self._check_material()
                    self.materials[i].ambient = (float(tokens[1]),float(tokens[2]),float(tokens[3]))
                elif tokens[0] == "Kd":
                    i = self._check_material()
                    self.materials[i].diffuse = (float(tokens[1]),float(tokens[2]),float(tokens[3]))
                elif tokens[0] == "Ks":
                    i = self._check_material()
                    self.materials[i].specular = (float(tokens[1]),float(tokens[2]),float(tokens[3]))
                elif tokens[0] == "Ns":
                    i = self._check_material()
                    self.materials[i].specular_exponent = float(tokens[1])

    def read_file(self, filename):
        with open(filename, "r") as f:
            for line in f:
                _line = line.splitlines()[0].strip()
                # comment
                if len(_line) == 0 or _line[0] == '#':
                    continue
                tokens = _line.split(" ")
                # material lib
                if tokens[0] == "mtllib":
                    self.material_lib = " ".join(tokens[1:])
                    self.read_material(self.material_lib)
                # material
                if tokens[0] == "usemtl":
                    self.current_material = tokens[1]
                # group
                if tokens[0] == "g":
                    self.groups.append(ObjGroup(tokens[1]))
                # geometric vertex
                elif tokens[0] == "v":
                    i = self._check_group()
                    self.groups[i].vertices.append((float(tokens[1]),float(tokens[2]),float(tokens[3])))
                # geometric texcoord
                elif tokens[0] == "vt":
                    i = self._check_group()
                    self.groups[i].texcoords.append((float(tokens[1]),float(tokens[2])))
                # geometric normal
                elif tokens[0] == "vn":
                    i = self._check_group()
                    self.groups[i].normals.append((float(tokens[1]),float(tokens[2]),float(tokens[3])))
                # face
                elif tokens[0] == "f":
                    i = self._check_group()
                    for j in [1,2,3]:
                        s = tokens[j].split("/")
                        tri = [-1, -1, -1]
                        tri[0] = int(s[0])
                        if len(s) >= 2 and len(s[1]) > 0:
                            tri[1] = int(s[1])
                        if len(s) >= 3 and len(s[2]) > 0:
                            tri[2] = int(s[2])
                        self.groups[i].triangles.append(tuple(tri))
                        self.groups[i].active_material.append(self.current_material)

################################################################################

# read all .obj files
mapf = ObjImporter()
mapf.read_file("map.obj")
wall_top = ObjImporter()
wall_top.read_file("wall_top.obj")
wall_bottom = ObjImporter()
wall_bottom.read_file("wall_bottom.obj")
paddle_enemy = ObjImporter()
paddle_enemy.read_file("paddle_enemy.obj")
paddle_player = ObjImporter()
paddle_player.read_file("paddle_player.obj")
random_object = ObjImporter()
random_object.read_file("random_object.obj")
ball = ObjImporter()
ball.read_file("ball.obj")

# write binary data
out = open("map.bin", "wb")
offsets = [0, 0, 0, 0, 0, 0, 0]

out.write(struct.pack("<iiiiiii", 0,0,0,0,0,0,0)) # for now

oi = 0
for imp in (mapf, wall_top, wall_bottom, paddle_enemy, paddle_player, random_object, ball):

    #
    # header of single object:
    # 32-bit vertices/normals/texcoords/colors count
    # 32-bit offset to vertices
    # 32-bit offset to normals
    # 32-bit offset to texcoords
    # 32-bit offset to colors
    # 32-bit offset to texture indices
    # 32-bit offset to texture data
    #


    # write dummy header (we will come back soon!)
    offsets[oi] = header_offset = out.tell()
    oi += 1
    out.write(struct.pack("<iiiiiii", 0,0,0,0,0,0,0)) # for now

    #
    # write vertices:
    # 3 floats make up one vertex
    #
    vertices_count = 0
    vertices_offset = out.tell()
    print("%x" % vertices_offset)
    for g in imp.groups:
        vertices_count += len(g.triangles)
        for i in g.triangles:
            if i[0] < 0:
                # insert dummy data
                out.write(struct.pack("<fff", 0.0, 0.0, 0.0))
            else:
                out.write(struct.pack("<fff", g.vertices[i[0]-1][0], g.vertices[i[0]-1][1], g.vertices[i[0]-1][2]))
    print("count %u" % vertices_count)

    #
    # write normals:
    # 4 floats make up one normal (last float ignored)
    #
    normals_offset = out.tell()
    print("%x" % normals_offset)
    for g in imp.groups:
        for i in g.triangles:
            if i[2] < 0:
                # insert dummy data
                out.write(struct.pack("<ffff", 0.0, 0.0, 0.0, 0.0))
            else:
                out.write(struct.pack("<ffff", g.normals[i[2]-1][0], g.normals[i[2]-1][1], g.normals[i[2]-1][2], 0.0))

    #
    # write texcoords:
    # 2 floats make up one texcoord
    #
    texcoords_offset = out.tell()
    print("%x" % texcoords_offset)
    for g in imp.groups:
        for i in g.triangles:
            if i[1] < 0:
                # insert dummy data
                out.write(struct.pack("<ff", 0.0, 0.0))
            else:
                out.write(struct.pack("<ff", g.texcoords[i[1]-1][0], g.texcoords[i[1]-1][1]))

    #
    # write colors:
    # 4 floats make up one color
    #
    colors_offset = out.tell()
    print("%x" % colors_offset)
    __cmp = 0
    for g in imp.groups:
        for i in g.active_material:
            __cmp += 1
            if i is None:
                # write black
                out.write(struct.pack("<ffff", 0.0, 0.0, 0.0, 1.0))
            else:
                # find material by name
                mat = None
                for j in imp.materials:
                    if j.name == i:
                        mat = j
                        break
                if mat is None:
                    print("warning: material %s not found" % i)
                    out.write(struct.pack("<ffff", 0.0, 0.0, 0.0, 1.0))
                else:
                    out.write(struct.pack("<ffff",
                        clamp(mat.ambient[0] + mat.diffuse[0] + mat.specular[0], 0.0, 1.0),
                        clamp(mat.ambient[1] + mat.diffuse[1] + mat.specular[1], 0.0, 1.0),
                        clamp(mat.ambient[2] + mat.diffuse[2] + mat.specular[2], 0.0, 1.0),
                        1.0))
    print("cmp %u" % __cmp)

    #
    # write indices:
    # one 32-bit signed integer index for each triangle
    #
    texindex_offset = out.tell()
    print("%x" % texindex_offset)
    for g in imp.groups:
        count = -1
        for i in g.active_material:
            count = count + 1
            if count % 3 != 0:
                continue
            if i is None:
                 out.write(struct.pack("<i", -1)) # no texture used
            else:
                # find material by name
                mat = None
                mati = 0
                for j in imp.materials:
                    if j.name == i:
                        mat = j
                        break
                    mati = mati + 1
                if mat is None:
                    print("warning: material %s not found" % i)
                    out.write(struct.pack("<i", -1)) # no texture used
                else:
                    out.write(struct.pack("<i", mati))

    #
    # write texture data:
    # number of textures
    # [per-texture]
    # size of targa image data [0 if material does not have any texture attached]
    # targa image
    #
    texdata_offset = out.tell()
    out.write(struct.pack("<i", len(imp.materials)))
    for m in imp.materials:
        if m.diffuse_texture is None:
            out.write(struct.pack("<i", 0))
            continue

        #
        # Convert to Targa format using ImageMagick.
        # We need to flip the image vertically because the CG renderer assumes
        # bottom-to-top row order.
        # Also, export the image as Windows bitmap first, because IM asserts
        # with selected files when directly exporting Targa:
        #
        # Requires minimum version 6.8.9-2 for RLE compression support.
        # Alpha premultiplication command line from:
        # http://imagemagick.org/discourse-server/viewtopic.php?t=23463
        #
        if subprocess.call(['convert', m.diffuse_texture, '-alpha', 'set',
            '(', '+clone', '-alpha', 'Extract', ')', '-channel', 'RGB',
            '-compose', 'Multiply', '-composite', '-flip', '__temp__.bmp']) != 0:
            print("Conversion of texture " + m.diffuse_texture + " failed.")
            exit(1)

        if subprocess.call(['convert', '__temp__.bmp', '-compress', 'RLE', '__temp__.tga']) != 0:
            print("Conversion of texture " + m.diffuse_texture + " failed.")
            exit(1)

        f = open("__temp__.tga", "rb")
        filesize = os.path.getsize("__temp__.tga")
        out.write(struct.pack("<i", filesize))
        out.write(f.read())

        # round up to multiple of 4 (performance)
        if filesize % 4 == 1:
            out.write(struct.pack("<BH",0,0))
        elif filesize % 4 == 2:
            out.write(struct.pack("<H",0))
        elif filesize % 4 == 3:
            out.write(struct.pack("<B",0))
        f.close()
        os.remove("__temp__.tga")
        os.remove("__temp__.bmp")

    out.seek(header_offset)
    out.write(struct.pack("<iiiiiii", vertices_count, vertices_offset,
        normals_offset, texcoords_offset, colors_offset, texindex_offset,
        texdata_offset))
    print("-----------------------")
    out.seek(0, 2) # back to end

out.seek(0, 0)
out.write(struct.pack("<iiiiiii", offsets[0], offsets[1], offsets[2], offsets[3], offsets[4], offsets[5], offsets[6]))

out.close()
