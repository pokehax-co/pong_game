#include "coll.h"
#include <math.h>
#include <float.h>

/*
 * The collision detection algorithm in this file is based on Kasper Fauerby's
 * "Improved Collision detection and Response" paper:
 * http://peroxide.dk/papers/collision/collision.pdf
 *
 * Verified implementation against a JavaScript implementation of the algorithm
 * by Brandon Jones:
 * https://gist.github.com/toji/2802287
 *
 * The collision response algorithm ("sweep") uses an improved version by
 * Jeff Linahan ("Improving the Numerical Robustness of Sphere Swept Collision
 * Detection");
 * http://arxiv.org/pdf/1211.0059
 *
 * There's also a simple sphere-sphere collision test at the bottom of this
 * file.
 *
 * XXX: somehow test pass2 and pass3. One pass is enough for us, because
 * usually there are no two planes in which our object can get stuck between at
 * the same time.
 */

/*
 * Dot product of two vectors a and b
 */
static inline float _dot_product(float a1, float a2, float a3, float b1,
    float b2, float b3)
{
    return a1*b1 + a2*b2 + a3*b3;
}
#define DOT_PRODUCT(a, b) \
    _dot_product((a)[0], (a)[1], (a)[2], (b)[0], (b)[1], (b)[2])

/*
 * Cross product of two vectors a and b
 */
static inline void _cross_product(float a1, float a2, float a3, float b1,
    float b2, float b3, float out[3])
{
    out[0] = a2 * b3 - a3 * b2;
    out[1] = a3 * b1 - a1 * b3;
    out[2] = a1 * b2 - a2 * b1;
}
#define CROSS_PRODUCT(a, b, c) \
    _cross_product((a)[0], (a)[1], (a)[2], (b)[0], (b)[1], (b)[2], c)

/**************************************/

/*
 * Calculates the normal vector and distance d of a triangle.
 */
static void _triangle_plane(float x1, float y1, float z1, float x2, float y2,
    float z2, float x3, float y3, float z3, float out[4])
{
    float U[3] = { x2 - x1, y2 - y1, z2 - z1 };
    float V[3] = { x3 - x1, y3 - y1, z3 - z1 };
    out[0] = U[1] * V[2] - U[2] * V[1];
    out[1] = U[2] * V[0] - U[0] * V[2];
    out[2] = U[0] * V[1] - U[1] * V[0];
    float d = 1.f/sqrtf(out[0] * out[0] + out[1] * out[1] + out[2] * out[2]);
    out[0] *= d;
    out[1] *= d;
    out[2] *= d;
    out[3] = -(out[0] * x1 + out[1] * y1 + out[2] * z1);
}
static inline void _triangle_plane(const float *a, const float *b,
    const float *c, float out[4])
{
    _triangle_plane(a[0], a[1], a[2], b[0], b[1], b[2], c[0], c[1], c[2], out);
}

/*
 * http://www.blackpawn.com/texts/pointinpoly/
 * Checks whether a point p lies in the triangle represented by a, b, c.
 */
static bool _point_in_triangle(const float *p, const float *a, const float *b,
    const float *c)
{
    float v2[3] = {p[0]-a[0], p[1]-a[1], p[2]-a[2]};
    float v1[3] = {b[0]-a[0], b[1]-a[1], b[2]-a[2]};
    float v0[3] = {c[0]-a[0], c[1]-a[1], c[2]-a[2]};
    float dot00 = DOT_PRODUCT(v0, v0);
    float dot01 = DOT_PRODUCT(v0, v1);
    float dot02 = DOT_PRODUCT(v0, v2);
    float dot11 = DOT_PRODUCT(v1, v1);
    float dot12 = DOT_PRODUCT(v1, v2);
    float inv_denom = 1.f/(dot00 * dot11 - dot01 * dot01);
    float u = (dot11 * dot02 - dot01 * dot12) * inv_denom;
    if (u < 0.f) return false;
    float v = (dot00 * dot12 - dot01 * dot02) * inv_denom;
    if (v < 0.f) return false;
    return u + v <= 1.f;
}

/*
 * Solves an equation of the form ax^2 + bx + c = 0.
 * If x is below a specified threshold, this function succeeds.
 * It fails if x exceeds a specified threshold, or if the equation has no
 * solution.
 */
static bool _solve(float a, float b, float c, float threshold, float &result)
{
    float radicant = b*b - 4.f*a*c;
    if (radicant < 0.f)
        return false;
    radicant = sqrtf(radicant);
    a = 1.f/(2.f*a);

    /* make x1 < x2 */
    float x1 = (-b + radicant) * a;
    float x2 = (-b - radicant) * a;
    if (x1 > x2) { float _ = x1; x1 = x2; x2 = _; }

    /* the algorithm only wants positive ones */
    if (x1 > 0 && x1 < threshold) {
        result = x1;
        return true;
    }
    if (x2 > 0 && x2 < threshold) {
        result = x2;
        return true;
    }
    return false;
}

bool CollisionDetect::_detect(CollisionObject &object,
        CollisionDetect::CollisionInfo &results,
        float trix1, float triy1, float triz1,
        float trix2, float triy2, float triz2,
        float trix3, float triy3, float triz3,
        bool &updated)
{
    float tri1[3] = {trix1, triy1, triz1};
    float tri2[3] = {trix2, triy2, triz2};
    float tri3[3] = {trix3, triy3, triz3};
    float vec_norm_arg;

    /*
     * Convert position and velocity of the sphere to ellipsoid space, so that
     * the sphere has unit radius.
     */
    float position_e[3], velocity_e[3], velocity_n_e[3];
    position_e[0] = (object._position[0] + object._translation[0]) /
        object._radius[0];
    position_e[1] = (object._position[1] + object._translation[1]) /
        object._radius[1];
    position_e[2] = (object._position[2] + object._translation[2]) /
        object._radius[2];
    velocity_e[0] = object._velocity[0] / object._radius[0];
    velocity_e[1] = object._velocity[1] / object._radius[1];
    velocity_e[2] = object._velocity[2] / object._radius[2];

    /*
     * Normalize the velocity
     */
    vec_norm_arg = 1.f/sqrtf(
        velocity_e[0] * velocity_e[0]+
        velocity_e[1] * velocity_e[1]+
        velocity_e[2] * velocity_e[2]
    );
    velocity_n_e[0] = velocity_e[0] * vec_norm_arg;
    velocity_n_e[1] = velocity_e[1] * vec_norm_arg;
    velocity_n_e[2] = velocity_e[2] * vec_norm_arg;

    /*
     * Calculate triangle's plane, contains normalized normal.
     * (Note: if you have an anti-clockwise triangle which is rendered, and is
     * then passed as clockwise, then the normal vector points in the opposite
     * direction.)
     */
    float tri_plane[4];
    _triangle_plane(&tri1[0], &tri2[0], &tri3[0], tri_plane);

    /*
     * Does the velocity point in the same direction as the triangle's normal?
     * If not, it is unlikely that the sphere collides.
     * (We could also say that we hit the triangle at the other direction.
     * However, we don't because triangles are usually drawn on one side
     * only, when their vertices are counter-clockwise from the camera's
     * perspective.)
     */
    if (DOT_PRODUCT(tri_plane, velocity_n_e) <= 0.f) {
        /* no collision */
        return false;
    }

    /* Distance between base point and plane */
    float distance = tri_plane[3] + DOT_PRODUCT(tri_plane, position_e);

    /* normal (dot) velocity */
    float n_dot_v = DOT_PRODUCT(tri_plane, velocity_e);

    /*
     * The above dot-product yields 0 if the vectors are perpendicular
     * = velocity is parallel to the plane. Therefore, the point is either
     * outside or inside the plane. If the distance between point and plane
     * is at least 1 (in ellipsoid space), then there is no point on the
     * sphere's surface which can collide the triangle. Otherwise, it intersects
     * the triangle already from the beginning and continues until the end.
     */
    float t1, t2 = 1.f;
    bool embedded = false;
#if defined(_MSC_VER) && _MSC_VER < 1900
    /* Old MSVCs don't have C99 compliant math */
    if (_isnan(1.f/n_dot_v) || !_finite(1.f/n_dot_v))
#else
    if (isnan(1.f/n_dot_v) || isinf(1.f/n_dot_v))
#endif
    {
        if (fabsf(distance) >= 1.f) {
            /* no collision */
            return false;
        } else {
            embedded = true;
            t1 = 0.f;
            //t2 = 1.f;
        }
    }

    /*
     * Calculate the intersection intervals t1 and t2. t1 must happen before t2.
     * Also, t1 must start at <= 1 and t2 must end at >= 0 for a successful
     * collision. Then clamp the range to [0, 1].
     */
    else {
        t1 = (-1.f - distance) / n_dot_v;
        t2 = (+1.f - distance) / n_dot_v;
        if (t1 > t2) { float _ = t1; t1 = t2; t2 = _; }
        if (t1 > 1.f || t2 < 0.f) {
            /* no collision */
            return false;
        }
        if (t1 > 1.f) t1 = 1.f;
        //if (t2 > 1.f) t2 = 1.f;
        if (t1 < 0.f) t1 = 0.f;
        //if (t2 < 0.f) t2 = 0.f;
    }

    // XXX: Early abort for subsequent calls which check for smaller distances
    // to determine the nearest object which has been hit.
    // if (i1 >= results.time) return false;
    // Also, we start at collision_time = 1.f only for the first run, and then
    // take results.time into consideration.

    float collision_pt[3];
    float collision_time = 1.f;
    bool collision_found = false;

    /*
     * If not embedded all the time, there is a point where the sphere collides
     * with the triangle. This point, however, is not its center, but a point
     * on the surface of the sphere which touches the triangle before any other.
     */
    if (!embedded) {
        float plane_intersec[3];
        plane_intersec[0] = position_e[0] - tri_plane[0] + t1 * velocity_e[0];
        plane_intersec[1] = position_e[1] - tri_plane[1] + t1 * velocity_e[1];
        plane_intersec[2] = position_e[2] - tri_plane[2] + t1 * velocity_e[2];

        if (_point_in_triangle(&plane_intersec[0], tri1, tri2, tri3)) {
            collision_found = true;
            collision_time = t1;
            collision_pt[0] = plane_intersec[0];
            collision_pt[1] = plane_intersec[1];
            collision_pt[2] = plane_intersec[2];
            goto skip_swipe;
        }
    }

    /*
     * Case 2: Now we need to sweep the sphere against the triangle's outer
     * boundary, also known as its edges and points.
     * (Also hide a g++ initialization crossing.)
     */
    {
    float new_time, a, b, c;
    float base_minus[3];

    float velocity_length_sq = DOT_PRODUCT(velocity_e, velocity_e);
    a = velocity_length_sq;

    /*
     * For every edge and point, solve an equation of the form ax^2 + bx + c = 0
     * and if we get an x below the current time, use this point/edge.
     */
#define CHECK_POINT(pt) \
    base_minus[0] = position_e[0] - pt[0]; \
    base_minus[1] = position_e[1] - pt[1]; \
    base_minus[2] = position_e[2] - pt[2]; \
    b = 2.f * DOT_PRODUCT(velocity_e, base_minus); \
    c = DOT_PRODUCT(base_minus, base_minus) - 1.f; \
    if (_solve(a, b, c, collision_time, new_time)) { \
        collision_time = new_time; \
        collision_found = true; \
        collision_pt[0] = pt[0]; \
        collision_pt[1] = pt[1]; \
        collision_pt[2] = pt[2]; \
        goto skip_swipe; \
    }

    CHECK_POINT(tri1)
    CHECK_POINT(tri2)
    CHECK_POINT(tri3)

    float edge[3], base_to_vertex[3];
    float edge_length_sq, edge_dot_velocity, edge_dot_b2v;
    float velocity_dot_b2v, b2v_length_sq;

#define CHECK_EDGE(ptstart, ptend) \
    edge[0] = ptend[0]-ptstart[0]; \
    edge[1] = ptend[1]-ptstart[1]; \
    edge[2] = ptend[2]-ptstart[2]; \
    base_to_vertex[0] = ptstart[0]-position_e[0]; \
    base_to_vertex[1] = ptstart[1]-position_e[1]; \
    base_to_vertex[2] = ptstart[2]-position_e[2]; \
    edge_length_sq = DOT_PRODUCT(edge, edge); \
    edge_dot_velocity = DOT_PRODUCT(edge, velocity_e); \
    edge_dot_b2v = DOT_PRODUCT(edge, base_to_vertex); \
    velocity_dot_b2v = DOT_PRODUCT(velocity_e, base_to_vertex); \
    b2v_length_sq = 1.f-DOT_PRODUCT(base_to_vertex, base_to_vertex); \
    a = edge_length_sq*(-velocity_length_sq) + \
        edge_dot_velocity*edge_dot_velocity; \
    b = edge_length_sq*2.f*velocity_dot_b2v - \
        2.f*edge_dot_velocity*edge_dot_b2v; \
    c = edge_length_sq*b2v_length_sq + edge_dot_b2v*edge_dot_b2v; \
    /* test edge as if it were of infinite length */ \
    if (_solve(a, b, c, collision_time, new_time)) { \
        /* within line segment? */ \
        float f = (edge_dot_velocity * new_time - edge_dot_b2v) / \
            edge_length_sq; \
        if (f >= 0.f && f <= 1.f) { \
            collision_time = new_time; \
            collision_found = true; \
            collision_pt[0] = ptstart[0] + f * edge[0]; \
            collision_pt[1] = ptstart[1] + f * edge[1]; \
            collision_pt[2] = ptstart[2] + f * edge[2]; \
            goto skip_swipe; \
        } \
    }

    CHECK_EDGE(tri1, tri2)
    CHECK_EDGE(tri2, tri3)
    CHECK_EDGE(tri3, tri1)
    }

skip_swipe:
    if (collision_found) {
        /*
         * First hit or closest hit?
         */
        distance = collision_time * sqrtf(
            velocity_e[0]*velocity_e[0]+
            velocity_e[1]*velocity_e[1]+
            velocity_e[2]*velocity_e[2]
        );
        if (!results.collision || distance < results.distance) {
            results.collision = true;
            results.distance = distance;
            results.time = collision_time;
            results.intersection[0] = collision_pt[0];
            results.intersection[1] = collision_pt[1];
            results.intersection[2] = collision_pt[2];
            updated = true;
        }
    }
    return collision_found;
}

/* can be exactly represented */
static const float VERY_CLOSE_DIST = 1.f/256.f;

void CollisionDetect::sweep_pass1(
    CollisionObject &object,
    CollisionDetect::CollisionInfo &result,
    CollisionDetect::SweepInfo &sweep)
{
    sweep.pos[0] = (object._position[0] + object._translation[0]) /
        object._radius[0];
    sweep.pos[1] = (object._position[1] + object._translation[1]) /
        object._radius[1];
    sweep.pos[2] = (object._position[2] + object._translation[2]) /
        object._radius[2];
    sweep.vel[0] = object._velocity[0] / object._radius[0];
    sweep.vel[1] = object._velocity[1] / object._radius[1];
    sweep.vel[2] = object._velocity[2] / object._radius[2];
    sweep._dest[0] = sweep.pos[0] + sweep.vel[0];
    sweep._dest[1] = sweep.pos[1] + sweep.vel[1];
    sweep._dest[2] = sweep.pos[2] + sweep.vel[2];

    /* CollisionDetect::sweep_pass3() has common stuff for all passes */
    sweep_pass3(object, result, sweep);

    float long_radius = 1.f + VERY_CLOSE_DIST;

    /*
     * Determine touch point. Using touch point and intersection point,
     * construct a sliding plane.
     */
    float touch_point[3];
    touch_point[0] = sweep.pos[0] + result.time * sweep.vel[0];
    touch_point[1] = sweep.pos[1] + result.time * sweep.vel[1];
    touch_point[2] = sweep.pos[2] + result.time * sweep.vel[2];
    sweep._first_plane[0] = touch_point[0] - result._intersection_espace[0];
    sweep._first_plane[1] = touch_point[1] - result._intersection_espace[1];
    sweep._first_plane[2] = touch_point[2] - result._intersection_espace[2];

    float n = 1.f/sqrtf(DOT_PRODUCT(sweep._first_plane, sweep._first_plane));
    sweep._first_plane[0] *= n;
    sweep._first_plane[1] *= n;
    sweep._first_plane[2] *= n;
    sweep._first_plane[3] = -DOT_PRODUCT(sweep._first_plane,
        result._intersection_espace);

    float plane_distance = DOT_PRODUCT(sweep._first_plane, sweep._dest)
        + sweep._first_plane[3] - long_radius;
    sweep._dest[0] -= plane_distance * sweep._first_plane[0];
    sweep._dest[1] -= plane_distance * sweep._first_plane[1];
    sweep._dest[2] -= plane_distance * sweep._first_plane[2];
    sweep.vel[0] = sweep._dest[0] - sweep.pos[0];
    sweep.vel[1] = sweep._dest[1] - sweep.pos[1];
    sweep.vel[2] = sweep._dest[2] - sweep.pos[2];
}

void CollisionDetect::sweep_pass2(
    CollisionObject &object,
    CollisionDetect::CollisionInfo &result,
    CollisionDetect::SweepInfo &sweep)
{
    float n;

    /* CollisionDetect::sweep_pass3() has common stuff for all passes */
    sweep_pass3(object, result, sweep);

    /*
     * Determine touch point. Using touch point and intersection point,
     * construct a sliding plane.
     */
    float touch_point[3], second_plane[4];
    touch_point[0] = sweep.pos[0] + result.time * sweep.vel[0];
    touch_point[1] = sweep.pos[1] + result.time * sweep.vel[1];
    touch_point[2] = sweep.pos[2] + result.time * sweep.vel[2];
    second_plane[0] = touch_point[0] - result._intersection_espace[0];
    second_plane[1] = touch_point[1] - result._intersection_espace[1];
    second_plane[2] = touch_point[2] - result._intersection_espace[2];

    n = 1.f/sqrtf(DOT_PRODUCT(second_plane, second_plane));
    second_plane[0] *= n;
    second_plane[1] *= n;
    second_plane[2] *= n;
    second_plane[3] = -DOT_PRODUCT(second_plane, result._intersection_espace);

    /* norm(n_firstplane x n_secondplane) */
    float crease[3];
    CROSS_PRODUCT(sweep._first_plane, second_plane, crease);

    n = 1.f/sqrtf(DOT_PRODUCT(crease, crease));
    crease[0] *= n;
    crease[1] *= n;
    crease[2] *= n;

    touch_point[0] = sweep._dest[0] - sweep.pos[0];
    touch_point[1] = sweep._dest[1] - sweep.pos[1];
    touch_point[2] = sweep._dest[2] - sweep.pos[2];
    float dis = DOT_PRODUCT(touch_point, crease);

    sweep.vel[0] = dis * crease[0];
    sweep.vel[1] = dis * crease[1];
    sweep.vel[2] = dis * crease[2];
    sweep._dest[0] = sweep.pos[0] + sweep.vel[0];
    sweep._dest[1] = sweep.pos[1] + sweep.vel[1];
    sweep._dest[2] = sweep.pos[2] + sweep.vel[2];
}

void CollisionDetect::sweep_pass3(
    CollisionObject &object,
    CollisionDetect::CollisionInfo &result,
    CollisionDetect::SweepInfo &sweep)
{
    // collision stats already called, assumed true

    float n = sqrtf(DOT_PRODUCT(sweep.vel, sweep.vel));
    float dist = result.time * n;

    float short_dist = dist - VERY_CLOSE_DIST;
    if (short_dist < 0.f) short_dist = 0.f;

    float vel_norm[3];
    vel_norm[0] = sweep.vel[0] / n;
    vel_norm[1] = sweep.vel[1] / n;
    vel_norm[2] = sweep.vel[2] / n;

    sweep.pos[0] += vel_norm[0] * short_dist; // pos = near point
    sweep.pos[1] += vel_norm[1] * short_dist; // pos = near point
    sweep.pos[2] += vel_norm[2] * short_dist; // pos = near point
}

void CollisionDetect::sweep_done(CollisionObject &object, SweepInfo &sweep)
{
    sweep.pos[0] = sweep._dest[0];
    sweep.pos[1] = sweep._dest[1];
    sweep.pos[2] = sweep._dest[2];

    /* convert back to world space */
    sweep.pos[0] *= object._radius[0];
    sweep.pos[1] *= object._radius[1];
    sweep.pos[2] *= object._radius[2];
    sweep.vel[0] *= object._radius[0];
    sweep.vel[1] *= object._radius[1];
    sweep.vel[2] *= object._radius[2];
}

/*
 * This sphere-sphere collision detection does not have anything to do with the
 * swept-sphere algorithm above!
 *
 * Original algorithm can be found on
 * https://studiofreya.com/3d-math-and-physics/
 *   simple-sphere-sphere-collision-detection-and-collision-response/
 */
bool CollisionDetect::detect(CollisionObject &o1, CollisionObject &o2,
    SweepInfo &sweep0, SweepInfo &sweep1)
{
    float d[3];
    d[0] = o1._position[0] - o2._position[0];
    d[1] = o1._position[1] - o2._position[1];
    d[2] = o1._position[2] - o2._position[2];
    float distance = sqrtf(DOT_PRODUCT(d, d));
    float sum_radius = o1._radius[0] + o2._radius[0];

    if (distance > sum_radius)
        return false;

    float x[3] = { d[0], d[1], d[2] };
    float _norm = sqrtf(x[0] * x[0] + x[1] * x[1] + x[2] * x[2]);
    x[0] /= _norm; x[1] /= _norm; x[2] /= _norm;

    float v1[3] = { o1._velocity[0], o1._velocity[1], o1._velocity[2] };
    float x1 = DOT_PRODUCT(x, v1);
    float v1x[3] = { x[0] * x1, x[1] * x1, x[2] * x1 };
    float v1y[3] = { v1[0] - v1x[0], v1[1] - v1x[1], v1[2] - v1x[2] };
    float m1 = 1.f;

    x[0] *= -1.f; x[1] *= -1.f; x[2] *= -1.f;
    float v2[3] = { o2._velocity[0], o2._velocity[1], o2._velocity[2] };
    float x2 = DOT_PRODUCT(x, v2);
    float v2x[3] = { x[0] * x2, x[1] * x2, x[2] * x2 };
    float v2y[3] = { v2[0] - v2x[0], v2[1] - v2x[1], v2[2] - v2x[2] };
    float m2 = 1.f;

    sweep0.pos[0] = o1._position[0];
    sweep0.pos[1] = o1._position[1];
    sweep0.pos[2] = o1._position[2];
    sweep1.pos[0] = o2._position[0];
    sweep1.pos[1] = o2._position[1];
    sweep1.pos[2] = o2._position[2];

    float _1 = (m1 - m2) / (m1 + m2);
    float _2 = (2.f * m2) / (m1 + m2);
    sweep0.vel[0] = v1x[0] * _1 + v2x[0] * _2 + v1y[0];
    sweep0.vel[1] = v1x[1] * _1 + v2x[1] * _2 + v1y[1];
    sweep0.vel[2] = v1x[2] * _1 + v2x[2] * _2 + v1y[2];

    float _3 = (2.f * m1) / (m1 + m2);
    float _4 = (m2 - m1) / (m1 + m2);
    sweep1.vel[0] = v1x[0] * _3 + v2x[0] * _4 + v2y[0];
    sweep1.vel[1] = v1x[1] * _3 + v2x[1] * _4 + v2y[1];
    sweep1.vel[2] = v1x[2] * _3 + v2x[2] * _4 + v2y[2];

    return true;
}
