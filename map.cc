#include "config.h"
#include "map.h"
#include "renderer.h"
#include <CGQuadric.h>
#include <CGImageFile.h>
#include <stdio.h>
#include <assert.h>

/*
 * Macros to extract arguments from macros with comma-separated elements.
 * We use such macros for colors in config.h.
 * You will find these macros in every source file that needs them, because
 * there is no common header for all components without making them dependent
 * of each other.
 */
#define EXTRACT_ARG1_p3_(a,b,c) a
#define EXTRACT_ARG2_p3_(a,b,c) b
#define EXTRACT_ARG3_p3_(a,b,c) c
#define EXPAND(a) a
#define EXTRACT_ARG1_p3(a) EXPAND(EXTRACT_ARG1_p3_(a))
#define EXTRACT_ARG2_p3(a) EXPAND(EXTRACT_ARG2_p3_(a))
#define EXTRACT_ARG3_p3(a) EXPAND(EXTRACT_ARG3_p3_(a))

/******************************************************************************/

/*
 * This object provides a CGQuadric object with the interface of a MapObject.
 * Note that you need derive from this class and implement your own constructor
 * which creates the actual CGQuadric.
 */
class _QuadricObject : public MapObject
{
public:
    /* documented in map.h */
    size_t vertices_count(void) const
    {
        return o.getVertexCount();
    }
    /* documented in map.h */
    const float* get_vertices(void) const
    {
        return o.getPositionArray();
    }
    /* documented in map.h */
    const float* get_normals(void) const
    {
        return o.getNormalArray();
    }
    /* documented in map.h */
    const float* get_texcoords(void) const
    {
        return o.getTexCoordArray();
    }
    /* documented in map.h */
    const float* get_colors(void) const
    {
        return o.getColorArray();
    }

    /*
     * Texturing not really intended by the CGQuadric class (apart from
     * coordinates). Only used for the 2D "classic map", which does not require
     * textures. So not advertising textures is OK.
     * Documented in map.h
     */
    size_t texture_indices_count(void) const { return 0; }
    const int* get_texture_indices(void) const { return NULL; }
    size_t textures_count(void) const { return 0; }
    const int* get_textures(void) const { return NULL; }

    /* documented in map.h */
    size_t collision_vertices_count(void) const
    {
        return o.getVertexCount();
    }
    /* documented in map.h */
    const float* get_collision_vertices(void) const
    {
        return o.getPositionArray();
    }

protected:
    CGQuadric o;
};

/*
 * This object contains the racket of player 2 in the "classic" map.
 */
class _RacketEnemyClassic : public _QuadricObject
{
public:
    _RacketEnemyClassic(Map *parent)
    {
        float pixel_size_x, pixel_size_z;
        PixelSimulator::pixel_size(&pixel_size_x, NULL, &pixel_size_z);
        CGVec4 field;
        parent->playfield_boundaries(field);

        /* create the object */
        o.setStandardColor(GAME_2D_COLOR_P2_RACKET);
        o.createBox();

        /* push it to the left edge */
        float *v = o.getPositionArray();
        unsigned int cnt = o.getVertexCount();
        float xx = pixel_size_x / 2.f;
        while (cnt--) {
            v[0] *= xx;
            v[0] -= field[MAP_BOUND_XMAX] - xx;
            v[1] *= parent->racket_length() / 2.f;
            v[2] += 1.f;
            v[2] *= pixel_size_z;
            v += 3;
        }
    }
};

/*
 * This object contains the racket of player 1 in the "classic" map.
 */
class _RacketPlayerClassic : public _QuadricObject
{
public:
    _RacketPlayerClassic(Map *parent)
    {
        float pixel_size_x, pixel_size_z;
        PixelSimulator::pixel_size(&pixel_size_x, NULL, &pixel_size_z);
        CGVec4 field;
        parent->playfield_boundaries(field);

        /* create the object */
        o.setStandardColor(GAME_2D_COLOR_P1_RACKET);
        o.createBox();

        /* push it to the right edge */
        float *v = o.getPositionArray();
        unsigned int cnt = o.getVertexCount();
        float xx = pixel_size_x / 2.f;
        while (cnt--) {
            v[0] *= xx;
            v[0] += field[MAP_BOUND_XMAX] - xx;
            v[1] *= parent->racket_length() / 2.f;
            v[2] += 1.f;
            v[2] *= pixel_size_z;
            v += 3;
        }
    }
};

/*
 * This object contains the wall at the top edge in the "classic" map.
 */
class _TopWallClassic : public _QuadricObject
{
public:
    _TopWallClassic(Map *parent, float xscale, float yscale)
    {
        float pixel_size_y, pixel_size_z;
        PixelSimulator::pixel_size(NULL, &pixel_size_y, &pixel_size_z);
        float aspect_ratio = xscale / yscale;

        /* create the object */
        o.setStandardColor(GAME_2D_COLOR_WALL_TOP);
        o.createBox();

        /* push it to the left edge */
        float *v = o.getPositionArray();
        unsigned int cnt = o.getVertexCount();
        float yy = pixel_size_y / 2.f;
        while (cnt--) {
            v[0] *= aspect_ratio;
            v[1] *= yy;
            v[1] += 1.f - yy;
            v[2] += 1.f;
            v[2] *= pixel_size_z;
            v += 3;
        }
    }
};

/*
 * This object contains the wall at the bottom edge in the "classic" map.
 */
class _BottomWallClassic : public _QuadricObject
{
public:
    _BottomWallClassic(Map *parent, float xscale, float yscale)
    {
        float pixel_size_y, pixel_size_z;
        PixelSimulator::pixel_size(NULL, &pixel_size_y, &pixel_size_z);
        float aspect_ratio = xscale / yscale;

        /* create the object */
        o.setStandardColor(GAME_2D_COLOR_WALL_BOTTOM);
        o.createBox();

        /* push it to the left edge */
        float *v = o.getPositionArray();
        unsigned int cnt = o.getVertexCount();
        float yy = pixel_size_y / 2.f;
        while (cnt--) {
            v[0] *= aspect_ratio;
            v[1] *= yy;
            v[1] -= 1.f - yy;
            v[2] += 1.f;
            v[2] *= pixel_size_z;
            v += 3;
        }
    }
};

/*
 * This object contains the obstacle ("random object") in the center of the
 * "classic" map.
 */
class _RandomObjectClassic : public _QuadricObject
{
public:
    _RandomObjectClassic(Map *parent, float xscale, float yscale)
    {
        float aspect_ratio = xscale / yscale;

        /*
         * The "random object" is a box. We rotate it slightly and give it a
         * parcel-like color.
         */
        o.setStandardColor(GAME_2D_COLOR_RANDOM_OBJECT);
        o.applyModelView(
            CGMatrix4x4::getScaleMatrix(.3f * aspect_ratio, .5f, .2f) *
            CGMatrix4x4::getRotationMatrixZ(-30.f)
        );
        o.applyExtraModelView(
            CGMatrix4x4::getTranslationMatrix(0.f, 0.f, .2f)
        );
        o.createBox();
    }
};

/*
 * This object contains the ball (not just the main ball, but also surprise
 * balls) in the "classic" map.
 */
class _BallClassic : public _QuadricObject
{
public:
    _BallClassic(Map *parent)
    {
        float pixel_size_x, pixel_size_y, pixel_size_z;
        PixelSimulator::pixel_size(&pixel_size_x, &pixel_size_y, &pixel_size_z);
        pixel_size_x /= 2.f;
        pixel_size_y /= 2.f;

        o.setStandardColor(GAME_2D_COLOR_BALL);
        o.createIcoSphere(2);

        /* make ball as small as one pixel */
        float *v = o.getPositionArray();
        unsigned int cnt = o.getVertexCount();
        while (cnt--) {
            v[0] *= pixel_size_x;
            v[1] *= pixel_size_y;
            v[2] += 1.f; // move ball above ground
            v[2] *= pixel_size_z;
            v += 3;
        }
    }
};

/*
 * This object contains the remaining details of the "classic" map, mainly the
 * dotted line along the horizontal center. [Previously contained a black floor
 * as well, but we use a black environment color to "render" it faster though
 * the CG renderer's clear-color function.
 */
class _MapClassic : public MapObject
{
public:
    /* documented in map.h */
    _MapClassic(Map *parent, float xscale, float yscale)
    {
#define PUSH_3(a,x,y,z) *a++ = (x); *a++ = (y); *a++ = (z);
#define PUSH_4(a,x,y,z,w) *a++ = (x); *a++ = (y); *a++ = (z); *a++ = (w);
#define PUSH_c(a,t,w) EXPAND(PUSH_3(a, t)); *a++ = (w);

        /*
         * Grab information from the PixelSimulator and the top-level map so
         * we can place the dotted line.
         */
        float pixel_size_x, pixel_size_y;
        PixelSimulator::pixel_size(&pixel_size_x, &pixel_size_y, NULL);
        pixel_size_x /= 2.f;
        pixel_size_y /= 2.f;

        unsigned int pixel_height;
        PixelSimulator::pixel_display(NULL, &pixel_height);
        assert(pixel_height > 0);

        /*
         * We construct the main mesh in the constructor. It's fairly easy:
         * just a black floor, and the white dotted line in its center.
         */
        unsigned int dotcount = (pixel_height - 1) / 2;
        size_t dot_pattern_vtxs = dotcount * 6;
        _mesh_vertices_count = dot_pattern_vtxs;

        /* need space for: vertices, colors, normals */
        _mesh_vertices = new float[_mesh_vertices_count * (3 + 4 + 4)];
        float *vertp = _mesh_vertices;
        float *normp = _mesh_vertices + _mesh_vertices_count * 3;
        float *colorp = _mesh_vertices + _mesh_vertices_count * (3 + 4);

        /*
         * Previously, there was code to generate a black floor in this place.
         * Why did we remove this?
         * 1. The performance of CGContext::m_cgRasterizeTriangle is horrible.
         *    Because we fill the whole screen, it creates ~600000 fragments
         *    at 1024x576 pixels!
         * 2. We do not use this map for 3D mode. In 2D mode, the camera is
         *    not movable, so we can hide the disadvantage of an incomplete map.
         * 3. We can control the clear color of the screen! Let the framebuffer
         *    be wiped with a black color, and voilà, there's our black floow.
         */

        /* dot pattern */
        float doty = -1.f + 2.5f * (2.f * pixel_size_y);
        while (dotcount-- > 0) {
            PUSH_3(vertp,  pixel_size_x, doty + pixel_size_y, 0.001f);
            PUSH_3(vertp, -pixel_size_x, doty + pixel_size_y, 0.001f);
            PUSH_3(vertp, -pixel_size_x, doty - pixel_size_y, 0.001f);
            PUSH_3(vertp, -pixel_size_x, doty - pixel_size_y, 0.001f);
            PUSH_3(vertp,  pixel_size_x, doty - pixel_size_y, 0.001f);
            PUSH_3(vertp,  pixel_size_x, doty + pixel_size_y, 0.001f);
            PUSH_4(normp, 0.f, 0.f, 1.f, 0.f);
            PUSH_4(normp, 0.f, 0.f, 1.f, 0.f);
            PUSH_4(normp, 0.f, 0.f, 1.f, 0.f);
            PUSH_4(normp, 0.f, 0.f, 1.f, 0.f);
            PUSH_4(normp, 0.f, 0.f, 1.f, 0.f);
            PUSH_4(normp, 0.f, 0.f, 1.f, 0.f);
            PUSH_c(colorp, GAME_2D_COLOR_DOT_PATTERN, 1.f);
            PUSH_c(colorp, GAME_2D_COLOR_DOT_PATTERN, 1.f);
            PUSH_c(colorp, GAME_2D_COLOR_DOT_PATTERN, 1.f);
            PUSH_c(colorp, GAME_2D_COLOR_DOT_PATTERN, 1.f);
            PUSH_c(colorp, GAME_2D_COLOR_DOT_PATTERN, 1.f);
            PUSH_c(colorp, GAME_2D_COLOR_DOT_PATTERN, 1.f);
            doty += (pixel_size_y * 2.f) * 2.f;
        }
    }
    ~_MapClassic(void)
    {
        delete[] _mesh_vertices;
    }

    /* documented in map.h */
    size_t vertices_count(void) const { return _mesh_vertices_count; }
    const float* get_vertices(void) const { return _mesh_vertices; }
    const float* get_normals(void) const
        { return _mesh_vertices + _mesh_vertices_count * 3; }
    const float* get_texcoords(void) const { return NULL; }
    const float* get_colors(void) const
        { return _mesh_vertices + _mesh_vertices_count * (3 + 4); }

    /* documented in map.h */
    size_t texture_indices_count(void) const { return 0; }
    const int* get_texture_indices(void) const { return NULL; }
    size_t textures_count(void) const { return 0; }
    const int* get_textures(void) const { return NULL; }

    /* documented in map.h */
    size_t collision_vertices_count(void) const { return _mesh_vertices_count; }
    const float* get_collision_vertices(void) const { return _mesh_vertices; }

private:
    size_t _mesh_vertices_count;
    float *_mesh_vertices;
};

/**************************************/

/*
 * Documented in map.h. Although we could initialize the object in the
 * initializer list, Visual Studio 2010 has a problem with the 'this' pointer.
 */
MapClassic::MapClassic(float xscale, float yscale) :
    _xscale(xscale), _yscale(yscale)
{
    _racket_enemy = new _RacketEnemyClassic(this);
    _racket_player = new _RacketPlayerClassic(this);
    _wall_top = new _TopWallClassic(this, xscale, yscale);
    _wall_bottom = new _BottomWallClassic(this, xscale, yscale);
    _random_object = new _RandomObjectClassic(this, xscale, yscale);
    _ball = new _BallClassic(this);
    _map = new _MapClassic(this, xscale, yscale);
}

/*
 * Deletes all created objects.
 */
MapClassic::~MapClassic(void)
{
    delete _map;
    delete _racket_enemy;
    delete _racket_player;
    delete _wall_top;
    delete _wall_bottom;
    delete _random_object;
    delete _ball;
}


/* documented in map.h */
void MapClassic::clear_color(float color[3]) const
{
    /* Return the "2D floor color". The color macro contains 3 comma-separated
       values, which need to be extracted. */
    color[0] = EXTRACT_ARG1_p3(GAME_2D_COLOR_FLOOR);
    color[1] = EXTRACT_ARG2_p3(GAME_2D_COLOR_FLOOR);
    color[2] = EXTRACT_ARG3_p3(GAME_2D_COLOR_FLOOR);
}

/* documented in map.h */
float MapClassic::hwall_thickness(void) const
{
    float pixel_size_y;
    PixelSimulator::pixel_size(NULL, &pixel_size_y, NULL);
    return pixel_size_y;
}

/* documented in map.h */
float MapClassic::racket_thickness(void) const
{
    float pixel_size_x;
    PixelSimulator::pixel_size(&pixel_size_x, NULL, NULL);
    return pixel_size_x;
}

/* documented in map.h */
float MapClassic::racket_length(void) const
{
    float pixel_size_y;
    PixelSimulator::pixel_size(NULL, &pixel_size_y, NULL);
    unsigned int pixel_height;
    PixelSimulator::pixel_display(NULL, &pixel_height);

    /* 15/100th of (height - 2) pixels */
    return .15f * pixel_size_y * (pixel_height - 2);
}

/* documented in map.h */
float MapClassic::ball_radius(void) const
{
    float pixel_size_x;
    PixelSimulator::pixel_size(&pixel_size_x, NULL, NULL);
    return pixel_size_x / 2.f;
}

/* documented in map.h */
void MapClassic::playfield_boundaries(CGVec4 &bound) const
{
    bound[0] = -(_xscale / _yscale);
    bound[1] = -1.f;
    bound[2] = -bound[0];
    bound[3] = 1.f;
}

/* documented in map.h */
const MapObject* MapClassic::get_map_object(Map::Object object) const
{
    switch (object) {
        case OBJECT_RACKET_ENEMY:
            return _racket_enemy;
        case OBJECT_RACKET_PLAYER:
            return _racket_player;
        case OBJECT_WALL_TOP:
            return _wall_top;
        case OBJECT_WALL_BOTTOM:
            return _wall_bottom;
        case OBJECT_RANDOM_OBJECT:
            return _random_object;
        case OBJECT_BALL:
            return _ball;
        case OBJECT_MAP:
            return _map;
        default:
            break;
    }
    return NULL;
}

/******************************************************************************/

/*
 * This class fetches all information from a single object contained in the
 * map file loaded by the MapUserDefined class.
 */
class _UserDefinedObject : public MapObject
{
public:
    /* documented in map.h */
    _UserDefinedObject(CGContext *ctx, const unsigned char *indata,
        unsigned int object_index);
    ~_UserDefinedObject(void);

    /* documented in map.h */
    size_t vertices_count(void) const { return _vertices_count; }
    const float* get_vertices(void) const { return _vertices; }
    const float* get_normals(void) const { return _normals; }
    const float* get_texcoords(void) const {return _texcoords; }
    const float* get_colors(void) const { return _colors; }

    /* documented in map.h */
    size_t texture_indices_count(void) const { return _texture_range_count; }
    const int* get_texture_indices(void) const { return _texture_ranges; }
    size_t textures_count(void) const { return _texture_count; }
    const int* get_textures(void) const { return _textures; }

    /* documented in map.h */
    size_t collision_vertices_count(void) const { return _vertices_count; }
    const float* get_collision_vertices(void) const { return _vertices; }
protected:
    size_t _vertices_count;
    const float *_vertices;
    const float *_normals;
    const float *_colors;
    const float *_texcoords;
    const unsigned int *_texture_data_header;
    size_t _texture_count;
    int *_textures;
    size_t _texture_range_count;
    int *_texture_ranges;
};

/*
 * Maximum number of objects in the map file, and indices to the objects.
 */
#define UD_MAP_MAX_OBJS 7
#define UD_MAP_OBJ_MAP 0
#define UD_MAP_OBJ_WALL_TOP 1
#define UD_MAP_OBJ_WALL_BOTTOM 2
#define UD_MAP_OBJ_P2_RACKET 3
#define UD_MAP_OBJ_P1_RACKET 4
#define UD_MAP_OBJ_RANDOM_OBJECT 5
#define UD_MAP_OBJ_BALL 6

/*
 * For a single object, indices of the various parts:
 * - Vertex count
 * - Vertices
 * - Normals
 * - Texture coordinates
 * - Colors
 * - Texture indices (of the vertices)
 * - Textures
 */
#define UD_OBJECT_VTX_CNT 0
#define UD_OBJECT_VTXS 1
#define UD_OBJECT_NRMS 2
#define UD_OBJECT_TEXCOORD 3
#define UD_OBJECT_CLRS 4
#define UD_OBJECT_TEXIDX 5
#define UD_OBJECT_TEXDATA 6
#define UD_OBJECT_MAX_DATA 7

/*
 * The ball has special treatment. Because Sketchup creates so many faces that
 * they can bring out renderer down to 0 FPS, we create the ball using the
 * CGQuadric class, but not without determining (from the map file):
 * - Radius of the ball (largest X coordinate)
 * - Color of the ball (takes the first vertex's color)
 */
class _BallUserDefined : public _QuadricObject
{
public:
    float ball_radius;

    _BallUserDefined(const unsigned char *indata, unsigned int object_index)
    {
        /*
         * Jump to object header
         */
        const unsigned int *object_header = (const unsigned int*)(
            (ptrdiff_t)indata + ((const unsigned int*)indata)[object_index]);
        size_t vertex_count = object_header[UD_OBJECT_VTX_CNT];

        /*
         * Pick the first color
         */
        if (vertex_count > 0) {
            const float *colors = (const float*)((ptrdiff_t)indata +
                object_header[UD_OBJECT_CLRS]);
            o.setStandardColor(colors[0], colors[1], colors[2]);
        }
        else {
            o.setStandardColor(.9f, .9f, .9f);
        }

        /*
         * Grep all vertices for the largest X coordinate, this is our radius.
         */
        const float *vertices = (const float*)((ptrdiff_t)indata +
            object_header[UD_OBJECT_VTXS]);
        float maxx = vertices[0];
        while (vertex_count-- > 0) {
            if (vertices[0] > maxx)
                maxx = vertices[0];
            vertices += 3;
        }
        ball_radius = maxx;

        /*
         * Create the ball using CGQuadric. We do this because Sketchup creates
         * too many faces, and we already have reduced performance by 67%
         * without textures. Then scale the ball using the found max-X.
         */
        o.createIcoSphere(2);

        /* Scale ball down to the radius we determined above */
        float *v = o.getPositionArray();
        unsigned int cnt = o.getVertexCount();
        while (cnt--) {
            v[0] *= ball_radius;
            v[1] *= ball_radius;
            v[2] += 1.f; // move ball above ground
            v[2] *= ball_radius;
            v += 3;
        }
    }
};

/*
 * Reads the data from an user-defined object passed via the map file 'indata'
 * at index 'object_index'.
 *
 * I would like to use this function as documentation for the actual file format
 * of the map file.
 *
 * There is no magic value. Wrong data and the game WILL crash. I am not
 * responsible for any damages.
 * All offsets within the file start from the beginning, which simplifies
 * pointer arithmetic (look into this function how ugly it is in C++).
 * All values are either 32-bit unsigned, 32-bit signed or 32-bit float,
 * LITTLE ENDIAN (hello MIPS routers).
 *
 * The map file begins with UD_MAP_MAX_OBJS offsets, which point to the
 * individual map objects. Their meaning is given by the UD_MAP_OBJ_* macros.
 *
 * Each object begins with the vertex count (32-bit unsigned integer), followed
 * by another set of UD_OBJECT_MAX_DATA offsets, which point to the various data
 * of the object. Their meaning is given by the UD_OBJECT_* macros.
 *
 * The format of the data depends on the type:
 *   Vertices: 3x 32-bit floats (X, Y, Z position) *per vertex*.
 *   Normals: 4x 32-bit floats (X, Y, Z direction, one unused) *per vertex*.
 *   Texture coordinates: 2x 32-bit floats (S, T) *per vertex*.
 *   Colors: 4x 32-bit floats (R, G, B, A components) *per vertex*.
 *   Texture indices: One 32-bit signed integer *per triangle*. -1 if no texture
 *     is bound to a triangle. One triangle consists of 3 vertices.
 *   Textures:
 *     A 32-bit unsigned integer indicating the number of textures.
 *     Texture data:
 *       Size of the texture in bytes
 *       Texture image. Must be in Targa format (converter script handles this).
 *       If size is not a multiple of 4, there are padding bytes so that the
 *       next texture/object begins at a 4-byte boundary (performance).
 */
_UserDefinedObject::_UserDefinedObject(CGContext *ctx,
    const unsigned char *indata, unsigned int object_index)
{
    /*
     * Grab offsets to the data
     */
    const unsigned int *object_header = (const unsigned int*)(
        (ptrdiff_t)indata + ((const unsigned int*)indata)[object_index]);
    _vertices_count = object_header[UD_OBJECT_VTX_CNT];
    _vertices = (const float*)((ptrdiff_t)indata +
        object_header[UD_OBJECT_VTXS]);
    _normals = (const float*)((ptrdiff_t)indata +
        object_header[UD_OBJECT_NRMS]);
    _texcoords = (const float*)((ptrdiff_t)indata +
        object_header[UD_OBJECT_TEXCOORD]);
    _colors = (const float*)((ptrdiff_t)indata +
        object_header[UD_OBJECT_CLRS]);
    const int *texture_index = (const int*)((ptrdiff_t)indata +
        object_header[UD_OBJECT_TEXIDX]);
    _texture_data_header = (const unsigned int*)((ptrdiff_t)indata +
        object_header[UD_OBJECT_TEXDATA]);

    /*
     * Iterate through the embedded textures, and create a CG texture for each.
     */
    if (_texture_data_header[0] > 0) {
        _texture_count = _texture_data_header[0];
        _textures = new int[_texture_count];

        const unsigned int *p = _texture_data_header + 1;
        for (unsigned int i = 0; i < _texture_count; ++i) {
            /*
             * For performance reasons, pad the size to multiples of 4 bytes.
             */
            unsigned int texture_size = (*p + 3) & ~3;
            if (!texture_size)
                _textures[i] = -1;
            else {
                int w, h;
                unsigned char *pic = cgImageLoadTGAMemory(
                    (const unsigned char*)&p[1], texture_size, &w, &h, 0);
                if (!pic) {
                    fprintf(stderr, "cannot load embedded texture #%u of "
                        "object %u\n", i, object_index);
                    _textures[i] = -1;
                }
                else {
                    _textures[i] = ctx->cgGenTexture_cgBindTexture_cgTexImage2D(
                        w, h, pic);
                    free(pic);
                }
            }
            /* next texture */
            p = (const unsigned int*)((ptrdiff_t)p + sizeof(unsigned int) +
                texture_size);
        }
    }
    else {
        _texture_count = 0;
        _textures = NULL;
    }

    /*
     * Iterate through the texture indices. Make a list with ranges for which a
     * specified texture is used.
     */
    _texture_range_count = 0;
    _texture_ranges = NULL;
    int last_texture = -32;
    for (size_t i = 0; i < _vertices_count / 3; ++i) {
        if (texture_index[i] != last_texture) {
            int *_;
            _ = (int*)realloc((void*)_texture_ranges,
                3 * (_texture_range_count + 1) * sizeof(int));
            if (!_) {
                fprintf(stderr, "out of memory\n");
                free(_texture_ranges);
                exit(1);
            }
            _texture_ranges = _;

            /* 0: starting vertex, 1: vertices count, 2: texture index */
            _texture_ranges[3 * _texture_range_count + 0] = 3 * (int)i;
            _texture_ranges[3 * _texture_range_count + 1] = 3;
            _texture_ranges[3 * _texture_range_count + 2] = texture_index[i];
            ++_texture_range_count;
            last_texture = texture_index[i];
        }
        else {
            assert(_texture_ranges && "reported by clang static analyzer");
            _texture_ranges[3 * (_texture_range_count - 1) + 1] += 3;
        }
    }
}

/*
 * Deletes all data associated with a map object.
 */
_UserDefinedObject::~_UserDefinedObject(void)
{
    free(_texture_ranges);
    delete[] _textures;
}

/* documented in map.h */
MapUserDefined::MapUserDefined(float xscale, float yscale, CGContext *ctx,
    const char *filename)
{
    _xscale = xscale;
    _yscale = yscale;

    /*
     * Load the map file into memory. We need the memory to be present during
     * the whole run time.
     */
    FILE *f = fopen(filename, "rb");
    if (!f) {
        fprintf(stderr, "cannot load map.bin");
        exit(1);
    }
    fseek(f, 0, SEEK_END);
    _mapfile_size = ftell(f);
    fseek(f, 0, SEEK_SET);

    _mapfile = new unsigned char[_mapfile_size];
    if (fread(_mapfile, _mapfile_size, 1, f) < 1) {
        fprintf(stderr, "cannot load map.bin");
        delete[] _mapfile;
        fclose(f);
        exit(1);
    }

    fclose(f);

    /*
     * For each object, scale X and Y by xscale and yscale, respectively.
     * Normalize normals. Do not scale balls, they are treated specially.
     */
    for (unsigned int object = 0; object < UD_MAP_MAX_OBJS; ++object) {

        unsigned int *object_header = (unsigned int*)(
            (ptrdiff_t)_mapfile + ((unsigned int*)_mapfile)[object]);
        size_t vertex_count = object_header[UD_OBJECT_VTX_CNT];

        if (object != UD_MAP_OBJ_BALL) {
            float *vertices = (float*)((ptrdiff_t)_mapfile +
                object_header[UD_OBJECT_VTXS]);
            while (vertex_count-- > 0) {
                *vertices++ *= xscale;
                *vertices++ *= yscale;
                vertices++;
            }
        }

        vertex_count = object_header[UD_OBJECT_VTX_CNT];
        float *normals = (float*)((ptrdiff_t)_mapfile +
            object_header[UD_OBJECT_NRMS]);
        while (vertex_count-- > 0) {
            float n = sqrtf(normals[0] * normals[0] + normals[1] * normals[1] +
                normals[2] * normals[2]);
            *normals++ /= n;
            *normals++ /= n;
            *normals++ /= n;
            normals++;
        }
    }

    /*
     * Create the map objects
     */
    _map = new _UserDefinedObject(ctx, _mapfile,
        UD_MAP_OBJ_MAP);
    _wall_top = new _UserDefinedObject(ctx, _mapfile,
        UD_MAP_OBJ_WALL_TOP);
    _wall_bottom = new _UserDefinedObject(ctx, _mapfile,
        UD_MAP_OBJ_WALL_BOTTOM);
    _racket_enemy = new _UserDefinedObject(ctx, _mapfile,
        UD_MAP_OBJ_P2_RACKET);
    _racket_player = new _UserDefinedObject(ctx, _mapfile,
        UD_MAP_OBJ_P1_RACKET);
    _random_object = new _UserDefinedObject(ctx, _mapfile,
        UD_MAP_OBJ_RANDOM_OBJECT);
    _ball = new _BallUserDefined(_mapfile, UD_MAP_OBJ_BALL);

    /*
     * Iterate over the vertices of the horizontal walls. Note down minimum
     * values on the Y axis, and calculate the distance to the top edge
     * (y = 1).
     */
    {
        unsigned int *object_header = (unsigned int*)(
            (ptrdiff_t)_mapfile + ((unsigned int*)_mapfile)
            [UD_MAP_OBJ_WALL_TOP]);
        float miny;

        size_t vertex_count = object_header[UD_OBJECT_VTX_CNT];
        float *vertices = (float*)((ptrdiff_t)_mapfile +
            object_header[UD_OBJECT_VTXS]);

        miny = vertices[1];
        while (vertex_count-- > 0) {
            if (vertices[1] < miny)
                miny = vertices[1];
            vertices += 3;
        }
        _hwall_thickness = fabsf(1.f - miny);
    }

    /*
     * Iterate over the vertices of the racket. Note down minimum values on the
     * X and Y axis. Calculate the length as the difference between minimum
     * and maximum Y values. Calculate the thickness as the distance between
     * the maximum X value and the left edge (x = -aspect).
     */
    {
        unsigned int *object_header = (unsigned int*)(
            (ptrdiff_t)_mapfile + ((unsigned int*)_mapfile)
            [UD_MAP_OBJ_P2_RACKET]);
        float maxx;
        float maxy, miny;

        size_t vertex_count = object_header[UD_OBJECT_VTX_CNT];
        float *vertices = (float*)((ptrdiff_t)_mapfile +
            object_header[UD_OBJECT_VTXS]);
        float aspect = xscale / yscale;

        maxx = vertices[0];
        maxy = miny = vertices[1];
        while (vertex_count-- > 0) {
            if (vertices[0] > maxx)
                maxx = vertices[0];
            if (vertices[1] > maxy)
                maxy = vertices[1];
            if (vertices[1] < miny)
                miny = vertices[1];
            vertices += 3;
        }
        _racket_thickness = fabsf(-aspect - maxx);
        _racket_length = fabsf(maxy - miny);
    }
}

/*
 * Deletes all created objects.
 */
MapUserDefined::~MapUserDefined(void)
{
    delete _map;
    delete _racket_enemy;
    delete _racket_player;
    delete _wall_top;
    delete _wall_bottom;
    delete _random_object;
    delete _ball;
    delete[] _mapfile;
}

/* documented in map.h */
void MapUserDefined::clear_color(float color[3]) const
{
    /* Return the "Game experience color". The color macro contains 3
       comma-separated values, which need to be extracted. */
    color[0] = EXTRACT_ARG1_p3(GAME_EXPERIENCE_BACKGROUND_COLOR);
    color[1] = EXTRACT_ARG2_p3(GAME_EXPERIENCE_BACKGROUND_COLOR);
    color[2] = EXTRACT_ARG3_p3(GAME_EXPERIENCE_BACKGROUND_COLOR);
}

/* documented in map.h */
float MapUserDefined::hwall_thickness(void) const
{
    return _hwall_thickness;
}

/* documented in map.h */
float MapUserDefined::racket_thickness(void) const
{
    return _racket_thickness;
}

/* documented in map.h */
float MapUserDefined::racket_length(void) const
{
    return _racket_length;
}

/* documented in map.h */
float MapUserDefined::ball_radius(void) const
{
    return static_cast<_BallUserDefined*>(_ball)->ball_radius;
}

/* documented in map.h */
void MapUserDefined::playfield_boundaries(CGVec4 &bound) const
{
    bound[0] = -_xscale;
    bound[1] = -_yscale;
    bound[2] = _xscale;
    bound[3] = _yscale;
}

/* documented in map.h */
const MapObject* MapUserDefined::get_map_object(Map::Object object) const
{
    switch (object) {
        case OBJECT_RACKET_ENEMY:
            return _racket_enemy;
        case OBJECT_RACKET_PLAYER:
            return _racket_player;
        case OBJECT_WALL_TOP:
            return _wall_top;
        case OBJECT_WALL_BOTTOM:
            return _wall_bottom;
        case OBJECT_RANDOM_OBJECT:
            return _random_object;
        case OBJECT_BALL:
            return _ball;
        case OBJECT_MAP:
            return _map;
        default:
            break;
    }
    return NULL;
}
